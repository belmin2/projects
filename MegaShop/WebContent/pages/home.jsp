<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*" %>
<%@ page import="java.util.*" %>
<%
	Object id = session.getAttribute("UserId");
	Object name = session.getAttribute("name");
	Object admin = session.getAttribute("Admin");
	String nameString="";
	if(id == null || name == null)
	{
		response.sendRedirect("../index.jsp");
	}
	else
	{
		nameString = name.toString();
	}
%>
<!DOCTYPE html>
<html>
 <head>
        <title>Mega Shop</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link rel="apple-touch-icon" sizes="180x180" href="../icon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../icon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../icon/favicon-16x16.png">
        <link rel="manifest" href="../icon/site.webmanifest">
        <script>
            function funkcija(number)
            {
                if(number == 1)
                {
                    document.getElementById("reklama1").style.display="none";
                    document.getElementById("reklama2").style.display="block";
                    document.getElementById("reklama3").style.display="none";
                }
                else if(number == 2)
                {
                    document.getElementById("reklama1").style.display="none";
                    document.getElementById("reklama3").style.display="block";
                    document.getElementById("reklama2").style.display="none";
                }
                else
                {
                    document.getElementById("reklama3").style.display="none";
                    document.getElementById("reklama1").style.display="block";
                    document.getElementById("reklama2").style.display="none";
                }
            }
        </script>
    </head>
    <body>
        <div id="header">
            <img src="../images/megashoplogonew.png" alt="logo" style="width:250px;height: 100px">
            <div id="flag">
                <ul>
                	<li>
                        <img src="../images/user.png" style="width: 50px;height:50px" alt="user">
                        <p style="float:right;padding-left:10px">
                        <%out.print(nameString);%>
                        </p>
                    </li>
                    <li>
                        <img src="../images/srb4.png" style="width: 50px;height:50px" alt="srb">
                    </li>
                    <li style="padding-top:32px;">
                       064/12356789<br>
                       034/7882390
                    </li>
                    <li id="phonenumber" style="padding-right: 5px;">
                       <img src="../images/phone.png" style="width: 50px;height:50px;" alt="phone">
                    </li>
                </ul>     
            </div>
        </div>
        <div id="menu">
            <ul>
            	<li><a href="home.jsp">Pocetna</a></li>
                <li><a href="${pageContext.request.contextPath}/ProductServlet">Proizvodi</a></li>
                <li><a href="${pageContext.request.contextPath}/UserServlet">Nalog</a></li>
                <li><a href="${pageContext.request.contextPath}/ContactServlet">Kontakt</a></li>
                <li><a href="logout.jsp">Odjavi se</a></li>
            	<%
            	if(admin != null)
            	{
            		%><li><a href="admin.jsp">Admin</a></li><%
            	}
            	%>
            </ul>
        </div>
        <div id="slideshow" class="thing">
            <div id="reklama1" onclick="funkcija(1)" style="background-image: url('../images/rtx3090.png'); background-size: 100% 400px; height:400px">
                <div class="text">
                    <div class="textinner">
                        <h1>NVidia RTX 3090 Ti</h1>
                        <p>Zelite da pokrecete nove naslove iz gejming industrije ko od sale?
                        Onda vam mi preporucujemo najnoviju NVidijinu RTX 3090 Ti.
                        Za vise detalja o proizvodu,kliknite <a href="products.jsp">ovde.</a>
                        </p><br>
                        <span class="dot1"></span>
                        <span class="dot2"></span>
                        <span class="dot2"></span>
                    </div>
                </div>
            </div>
            <div id="reklama2" onclick="funkcija(2)" style="display:none;background-image: url('../images/lgoledevo.jpg'); background-size: 100% 400px; height:400px">
                <div class="text">
                    <div class="textinner">
                        <h1>LG OLED EVO C2 4K</h1>
                        <p>Zelite najbolji kvalitet slike dok gledate filmove i serije?.Onda vam mi
                            mozemo pomoci,nasa preporuka je novi LG OLED EVO sa 4K rezolucijom.
                            Za vise detalja o proizvodu,kliknite <a href="products.jsp">ovde.</a>
                        </p><br>
                        <span class="dot2"></span>
                        <span class="dot1"></span>
                        <span class="dot2"></span>
                    </div>
                </div>
            </div>
            <div id="reklama3" onclick="funkcija(3)" style="display:none;background-image: url('../images/samsung22g.jpg'); background-size: 100% 400px; height:400px">
                <div class="text">
                    <div class="textinner">
                        <h1>Samsung Galaxy S22</h1>
                        <p>Zelite slike najboljeg kvaliteta?Telefon koji pomera granice mogucnosti?
                            Onda je pravi izbor za vas Samsung Galaxy S22.
                            Za vise detalja o proizvodu,kliknite <a href="products.jsp">ovde.</a>
                        </p><br>
                        <span class="dot2"></span>
                        <span class="dot2"></span>
                        <span class="dot1"></span>
                    </div>
                </div>
            </div>
        </div>
        <div id="lokacija" >
            <div id="transp" style="background-image: url('../images/kgloc.png'); background-size: inherit; background-position: center bottom; height:400px">
                <div id="map1">
                    <img src="../images/mapiconw.png" alt="mapiconw" style="width: 100px;height:100px;padding-top:75px;padding-left: 37%">
                    <div id="tekstlokacija">
                    <h1>Gde nas mozete pronaci?</h1>
                    <p>Zelite da pogledate proizvode uzivo,onda nas posetite u vasem gradu.
                        Mi imamo filijale cak u 30 razlicith gradova u Srbiji.
                        Za detaljniji pregled nasih lokacija i kako nas mozete pronaci
                        kliknite <a href="contact.jsp">ovde.</a></p>
                </div>
                </div>
            </div>
        </div>
        <div id="footer">
            <div id="mreze">
                <ul>
                    <li>
                        <img src="../images/twiter.png" alt="twiter" style="height: 50px;width:50px">
                    </li>
                    <li>
                        <img src="../images/instagram.jpg" alt="instagram" style="height: 50px;width:50px">
                    </li>
                    <li>
                        <img src="../images/facebook.png" alt="facebook" style="height: 50px;width:50px">
                    </li>
                </ul>
            </div>
            <br><br>
            MegaShop &#169 2022<br>
            megashop@gmail.com<br>
        </div>
    </body>
</html>