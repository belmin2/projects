<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*" %>
<%@ page import="java.util.*" %>
<%
	Object id = session.getAttribute("UserId");
	Object name = session.getAttribute("name");
	Object admin = session.getAttribute("Admin");
	String nameString="";
	if(id == null || name == null)
	{
		response.sendRedirect("../index.jsp");
	}
	else
	{
		nameString = name.toString();
	}
	List<Product> products = (List<Product>)request.getAttribute("products");
	String bought = (String)request.getAttribute("bought");
	List<Category> categories = (List<Category>)request.getAttribute("categories");
%>
<!DOCTYPE html>
<html>
	<head>
		<title>Mega Shop</title>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <link href="css/style.css" rel="stylesheet" type="text/css"/>
	    <link rel="apple-touch-icon" sizes="180x180" href="icon/apple-touch-icon.png">
	    <link rel="icon" type="image/png" sizes="32x32" href="icon/favicon-32x32.png">
	    <link rel="icon" type="image/png" sizes="16x16" href="icon/favicon-16x16.png">
	    <link rel="manifest" href="icon/site.webmanifest">
	    <script src="javascript/products.js" type="text/javascript"></script>
	    <script>
	    	var Msg = "<%out.print(bought);%>";
	    	console.log(Msg);
	    	if(Msg == "Yes")
	    	{
	    		function alertBought()
	    		{
	    			alert("Uspesno ste kupili proizvode.");
	    		}
	    	}
	    </script>
	</head>
	<body onload="callAjax(),alertBought()">
		<div id="header">
            <img src="images/megashoplogonew.png" alt="logo" style="width:250px;height: 100px">
            <div id="flag">
                <ul>
                	<li>
                        <img src="images/user.png" style="width: 50px;height:50px" alt="user">
                        <p style="float:right;padding-left:10px">
                        <%out.print(nameString);%>
                        </p>
                    </li>
                    <li>
                        <img src="images/srb4.png" style="width: 50px;height:50px" alt="srb">
                    </li>
                    <li style="padding-top:32px;">
                       064/12356789<br>
                       034/7882390
                    </li>
                    <li id="phonenumber" style="padding-right: 5px;">
                       <img src="images/phone.png" style="width: 50px;height:50px;" alt="phone">
                    </li>
                </ul>     
            </div>
    	</div>
    	<div id="menu">
            <ul>
            	<li><a href="pages/home.jsp">Pocetna</a></li>
                <li><a href="${pageContext.request.contextPath}/ProductServlet">Proizvodi</a></li>
                <li><a href="${pageContext.request.contextPath}/UserServlet">Nalog</a></li>
                <li><a href="${pageContext.request.contextPath}/ContactServlet">Kontakt</a></li>
                <li><a href="logout.jsp">Odjavi se</a></li>
            	<%
            	if(admin != null)
            	{
            		%><li><a href="admin.jsp">Admin</a></li><%
            	}
            	%>
            	<li style="float:right">
                    <button id="korpabuttonmeni" onclick="cartOpen()">
                        <img src="images/cart.png" alt="cart" style="width:41px;height:100%;">
                    </button>
                </li>
            </ul>
        </div>
        <div id="cartdiv">
             <div id="cart">
                    <div id="korpa"><h3>Korpa</h3></div>
                    <form method="post" action="${pageContext.request.contextPath}/BuyProductServlet">
                    <input type="hidden" name="userid" value="<%out.print(id.toString());%>">
                    <table id="tabelakorpa">
                    </table>
                    <div id="suma"><p id="ukupno" style="color:white;">Ukupno</p><button id="kupibutton">Kupi</button></div>
                    </form>
            </div>
        </div>
        <main>
            <aside class="sticky">
                <div id="rad">
                    <br><br>
                    Naziv:<br><br>
                    <input type="text" oninput="nameFiltering()" name="naziv" id="naziv" style="background-color:#121314;color:lightblue;border:1px solid lightblue;width:80%;height:30px"><br><br>
                    Cena:<br><br>
                    od:&nbsp;<input type="text" id="cenaod" oninput="checkPriceFrom()" style="background-color:#121314;color:lightblue;border:1px solid lightblue;width:30%;height:30px">&nbsp;do:&nbsp;
                    <input type="text" id="cenado" oninput="checkPriceTo()" style="background-color:#121314;color:lightblue;border:1px solid lightblue;width:30%;height:30px"><br><br>
                    Kategorija:<br><br>
                    <div class="select" style="margin-left:7%">
                    <select id="standard-select" onchange="categoryFilter()">
                      <option selected value="0">Sve</option>
                      <%
                      int br=0;
                      for(Category c : categories)
                      {
                    	br++;
                    	%>
                    	<option value="<%out.print(br);%>"><%out.print(c.getNaziv());%></option>
                    	<%  
                   	  }
                   	  %>
                    </select>
                    <span class="focus"></span>
                    </div><br><br>Boja:<br><br>
                    <input type="text" oninput="colorFiltering()" name="boja" id="boja" style="background-color:#121314;color:lightblue;border:1px solid lightblue;width:80%;height:30px">
                </div>
            </aside>
            <section class="product">
                <div class="grid" id="gridproduct">
                </div>
            </section>
        </main>
    	<div id="footer">
            <div id="mreze">
                <ul>
                    <li>
                        <img src="images/twiter.png" alt="twiter" style="height: 50px;width:50px">
                    </li>
                    <li>
                        <img src="images/instagram.jpg" alt="instagram" style="height: 50px;width:50px">
                    </li>
                    <li>
                        <img src="images/facebook.png" alt="facebook" style="height: 50px;width:50px">
                    </li>
                </ul>
            </div>
            <br><br>
            MegaShop &#169 2022<br>
            megashop@gmail.com<br>
        </div>
	</body>
</html>