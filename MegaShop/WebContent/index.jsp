<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%
	String errorLogin = request.getParameter("errorLogin");
%>
<html>
	<head>
        <title>Mega Shop</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <link rel="apple-touch-icon" sizes="180x180" href="icon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="icon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="icon/favicon-16x16.png">
        <link rel="manifest" href="icon/site.webmanifest">
    </head>
    <body>
        <div id="header">
            <img src="images/megashoplogonew.png" alt="logo" style="width:250px;height: 100px">
            <div id="flag">
                <ul>
                    <li>
                        <img src="images/srb4.png" style="width: 50px;height:50px" alt="srb">
                    </li>
                    <li style="padding-top:32px;">
                       064/12356789<br>
                       034/7882390
                    </li>
                    <li id="phonenumber" style="padding-right: 5px;">
                       <img src="images/phone.png" style="width: 50px;height:50px;" alt="phone">
                    </li>
                </ul>     
            </div>
        </div>
        <div id="menu">
            <ul>
                <li><a href="index.jsp">Prijava</a></li>
                <li><a href="pages/register.jsp">Registracija</a></li>
            </ul>
        </div>
        <div id="login"><br>
            <div id="loginbox"><br>
            <form method="get" action="${pageContext.request.contextPath}/AuthServlet" id="loginForm" >
                <h2>Prijava</h2>
                Unesite Email i lozinku<br><br>
                <input id="user" type="text" name="email" placeholder="Email"><br>
                <input id="pass" type="password" name="sifra" placeholder="Sifra">
                <%
                if(errorLogin!=null)
                {
	                if(errorLogin.equals("true"))
					{%>
						<p id="errorLogin" style="color:red">Greska! Pogresan email ili sifra</p>
					<%
					}
	            }%>
                <br>
                <button id="dugmeprijava">Prijavi se</button><br><br>
                Nemate nalog? <a href="register.html">Registrujte se</a> 
            </form><br>
            </div>
        <br></div>
        <div id="footer">
            <div id="mreze">
                <ul>
                    <li>
                        <img src="images/twiter.png" alt="twiter" style="height: 50px;width:50px">
                    </li>
                    <li>
                        <img src="images/instagram.jpg" alt="instagram" style="height: 50px;width:50px">
                    </li>
                    <li>
                        <img src="images/facebook.png" alt="facebook" style="height: 50px;width:50px">
                    </li>
                </ul>
            </div>
            <br><br>
            MegaShop &#169 2022<br>
            megashop@gmail.com<br>
        </div>
    </body>
</html>