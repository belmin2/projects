package db;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.BoughtProducts;

public class BoughtProductsRepository {
	private EntityManager entityManager;
	private EntityManagerFactory entityManagerFactory;

	public BoughtProductsRepository() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("persistenceUnit");
		this.entityManager = entityManagerFactory.createEntityManager();
	}
	
	public long getBuyId() {
		Query find_buy_id = entityManager.createNamedQuery("find buy id");
		long l =(long) find_buy_id.getSingleResult(); 
		return l;
	}
	public List<BoughtProducts> getAllBoughtProductsByIdUser(int id_korisnika)
	{
		Query find_bought_products_by_id_user = entityManager.createNamedQuery("find boughtproducts by id user");
		find_bought_products_by_id_user.setParameter("id_korisnika", id_korisnika);
		return find_bought_products_by_id_user.getResultList();
	}
	public int getMaxBuyId() {
		Query find_max_buy_id = entityManager.createNamedQuery("find max buy id");
		int l =(int)find_max_buy_id.getSingleResult(); 
		return l;
	}
	public BoughtProducts saveBuy(BoughtProducts boughtProduct)
	{
		entityManager.getTransaction().begin();
		entityManager.merge(boughtProduct);
		entityManager.getTransaction().commit();
		return boughtProduct;
	}
}
