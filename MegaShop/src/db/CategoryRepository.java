package db;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Category;
import model.Product;

public class CategoryRepository {
	private EntityManager entityManager;
	private EntityManagerFactory entityManagerFactory;

	public CategoryRepository() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("persistenceUnit");
		this.entityManager = entityManagerFactory.createEntityManager();
	}
	public List<Category> getAllCategories()
	{
		Query get_all_categories = entityManager.createNamedQuery("get all categories");
		return get_all_categories.getResultList();
	}
}
