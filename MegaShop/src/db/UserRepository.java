package db;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.User;

public class UserRepository {
	private EntityManager entityManager;
	private EntityManagerFactory entityManagerFactory;
	
	public UserRepository() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("persistenceUnit");
		this.entityManager = entityManagerFactory.createEntityManager();
	}
	
	public User addUser(User u)
	{
		entityManager.getTransaction().begin();
		entityManager.merge(u);
		entityManager.getTransaction().commit();
		return u;
	}
	public User findUserById(int id)
	{
		return entityManager.find(User.class,id);
	}
	public User findUserByEmail(String email) {
		Query find_user_by_email = entityManager.createNamedQuery("find user by email");
		find_user_by_email.setParameter("email", email);
		User usr;
		try {
			usr = (User) find_user_by_email.getSingleResult();
		} catch (NoResultException e) {
			usr = null;
		}
		return usr;
	}
}
