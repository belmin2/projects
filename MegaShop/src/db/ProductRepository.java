package db;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Product;

public class ProductRepository {
	private EntityManager entityManager;
	private EntityManagerFactory entityManagerFactory;
	
	public ProductRepository() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("persistenceUnit");
		this.entityManager = entityManagerFactory.createEntityManager();
	}
	
	
	
	public List<Product> getAllProducts()
	{
		Query get_all_products = entityManager.createNamedQuery("get all products");
		return get_all_products.getResultList();
	}
	public Product getProductById(int id)
	{
		return entityManager.find(Product.class, id);
	}
}
