package db;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Location;

public class LocationRepository {
	private EntityManager entityManager;
	private EntityManagerFactory entityManagerFactory;
	
	public LocationRepository() {
		this.entityManagerFactory = Persistence.createEntityManagerFactory("persistenceUnit");
		this.entityManager = entityManagerFactory.createEntityManager();
	}
	
	public List<Location> getLocationByTip(String tip)
	{
		Query get_location_tip = entityManager.createNamedQuery("get locations by name");
		get_location_tip.setParameter("tip", tip);
		return get_location_tip.getResultList();
	}
	public List<Location> getAllLocations()
	{
		Query get_location_all = entityManager.createNamedQuery("get all locations");
		return get_location_all.getResultList();
	}
}
