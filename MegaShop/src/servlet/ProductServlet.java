package servlet;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import model.Category;
import model.IProduct;
import model.Location;
import model.Product;

@WebServlet("/ProductServlet")
public class ProductServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	IProduct productService;
	public ProductServlet() {
		super();
		try {
			productService = (IProduct)Naming.lookup("//localhost:1103/productServer");
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		List<Product> products = this.productService.getAllProducts();
		Object bought = request.getParameter("bought");
		System.out.println(bought);
		String boughtProduct;
		if(bought != null)
		{
			boughtProduct = "Yes";
		}
		else
		{
			boughtProduct="No";
		}
		request.setAttribute("products", products);
		List<Category> categories = this.productService.getAllCategories();
		request.setAttribute("products", products);
		request.setAttribute("bought", boughtProduct);
		request.setAttribute("categories", categories);
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/pages/products.jsp");
		rd.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Product> products = this.productService.getAllProducts();
		JSONArray array = new JSONArray();
        
		for(Product p : products)
		{
			JSONObject product = new JSONObject();
			product.put("id", p.getId());
			product.put("cena", p.getCena());
			product.put("naziv", p.getNaziv());
			product.put("opis", p.getOpis());
			product.put("kolicina", p.getKolicina());
			product.put("boja", p.getBoja());
			product.put("idKategorije", p.getId_kategorije());
			product.put("slika", p.getSlika());
			array.put(product);
		}
		response.setContentType("application/json");
        response.getWriter().write(array.toString());
	}
}
