package servlet;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.BoughtProducts;
import model.Category;
import model.IAuth;
import model.Product;
import model.ProductsCart;
import model.User;

@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	IAuth authService;
	public UserServlet() {
		super();
		try {
			this.authService = (IAuth) Naming.lookup("//localhost:1103/authServer");
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) 
	{
	    Map<Object, Boolean> map = new ConcurrentHashMap<>();
	    return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		int userId = (int) session.getAttribute("UserId");
		User user=this.authService.getUserById(userId);
		request.setAttribute("user", user);
		List<Product> products = this.authService.getAllProducts();
		List<BoughtProducts> boughtProducts =this.authService.getAllBoughtProductsById(userId);
		List<BoughtProducts> boughtProductHelp = boughtProducts.stream().filter(distinctByKey(bp-> bp.getId_kupovine())).toList();
		List<Integer> listBuys = new LinkedList<Integer>();
		for(BoughtProducts b : boughtProductHelp)
		{
			listBuys.add(b.getId_kupovine());
		}
		List<ProductsCart> productsCart = new LinkedList<ProductsCart>();
		for(BoughtProducts b : boughtProducts)
		{
			for(Product p: products)
			{
				if(b.getId_proizvoda() == p.getId())
				{
					ProductsCart productCart = new ProductsCart(b.getId_kupovine(),p.getNaziv(),b.getKolicina(),p.getCena());
					productsCart.add(productCart);
				}
			}
		}
		int Sum;
		Map<Integer,Integer> mapBuys = new HashMap<>();
		for(Integer i : listBuys)
		{
			Sum=0;
			for(ProductsCart pc : productsCart)
			{
				if(i == pc.getId_kupovine())
				{
					Sum+=pc.getCena()*pc.getKolicina();
				}
			}
			mapBuys.put(i, Sum);
		}
		request.setAttribute("listbuys", mapBuys);
		request.setAttribute("products", productsCart);
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/pages/account.jsp");
		rd.forward(request, response);
	}
}
