package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name = "find buy id", query = "SELECT count(bp.id) from BoughtProducts bp")
@NamedQuery(name = "find max buy id", query = "SELECT max(bp.id_kupovine) from BoughtProducts bp")
@NamedQuery(name = "find boughtproducts by id user", query="SELECT bp from BoughtProducts bp WHERE bp.id_korisnika = :id_korisnika")
public class BoughtProducts implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private int id_kupovine;
	private int id_korisnika;
	private int id_proizvoda;
	private Date datum_kupovine;
	private int kolicina;
	public BoughtProducts() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public int getId_kupovine() {
		return id_kupovine;
	}

	public void setId_kupovine(int id_kupovine) {
		this.id_kupovine = id_kupovine;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId_korisnika() {
		return id_korisnika;
	}
	public void setId_korisnika(int id_korisnika) {
		this.id_korisnika = id_korisnika;
	}
	public int getId_proizvoda() {
		return id_proizvoda;
	}
	public void setId_proizvoda(int id_proizvoda) {
		this.id_proizvoda = id_proizvoda;
	}
	public Date getDatum_kupovine() {
		return datum_kupovine;
	}
	public void setDatum_kupovine(java.util.Date datum) {
		this.datum_kupovine = datum;
	}
	public int getKolicina() {
		return kolicina;
	}
	public void setKolicina(int kolicina) {
		this.kolicina = kolicina;
	}

	@Override
	public String toString() {
		return "BoughtProducts [id=" + id + ", id_kupovine=" + id_kupovine + ", id_korisnika=" + id_korisnika
				+ ", id_proizvoda=" + id_proizvoda + ", datum_kupovine=" + datum_kupovine + ", kolicina=" + kolicina
				+ "]";
	}
	
	
	
	
}
