package model;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface IProduct extends Remote{
	List<Product> getAllProducts() throws RemoteException;
	List<Category> getAllCategories() throws RemoteException;
	long getIdOfBuy() throws RemoteException;
	int getMaxIdOfBuy() throws RemoteException;
	boolean saveBuy(BoughtProducts boughtProduct) throws RemoteException;
}
