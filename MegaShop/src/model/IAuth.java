package model;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface IAuth extends Remote {
	boolean register(User u) throws RemoteException;
	User login(String username,String password) throws RemoteException;
	User getUserById(int id) throws RemoteException;
	List<Product> getAllProducts() throws RemoteException;
	List<BoughtProducts> getAllBoughtProductsById(int idUser) throws RemoteException;
	int getMaxIdOfBuy() throws RemoteException;
}
