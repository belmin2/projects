USE [prodavnica_database]
GO
/****** Object:  Table [dbo].[kategorije]    Script Date: 8/7/2020 9:40:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[kategorije](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[naziv] [varchar](50) NULL,
 CONSTRAINT [PK_kategorije] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[login_user]    Script Date: 8/7/2020 9:40:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[login_user](
	[username] [varchar](20) NOT NULL,
	[pass] [varchar](20) NULL,
 CONSTRAINT [PK_login_user] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[prodavnice]    Script Date: 8/7/2020 9:40:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[prodavnice](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[brojproizvoda] [int] NULL,
 CONSTRAINT [PK_prodavnice] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[proizvodi_tab]    Script Date: 8/7/2020 9:40:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[proizvodi_tab](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ime] [varchar](50) NULL,
	[cena] [float] NULL,
	[roktrajanja] [date] NULL,
	[idkategorije] [int] NOT NULL,
 CONSTRAINT [PK_proizvodi_tab] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[proizvodiprodavnice]    Script Date: 8/7/2020 9:40:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[proizvodiprodavnice](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idprodavnice] [int] NOT NULL,
	[idproizvoda] [int] NOT NULL,
	[kolicina] [int] NULL,
 CONSTRAINT [PK_proizvodiprodavnice] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user_db]    Script Date: 8/7/2020 9:40:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_db](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](20) NULL,
	[name] [varchar](20) NULL,
	[email] [varchar](50) NULL,
	[birthdate] [date] NULL,
	[role] [varchar](50) NULL,
 CONSTRAINT [PK_user_db] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[proizvodi_tab]  WITH CHECK ADD  CONSTRAINT [FK_proizvodi_tab_kategorije] FOREIGN KEY([idkategorije])
REFERENCES [dbo].[kategorije] ([id])
GO
ALTER TABLE [dbo].[proizvodi_tab] CHECK CONSTRAINT [FK_proizvodi_tab_kategorije]
GO
ALTER TABLE [dbo].[proizvodiprodavnice]  WITH CHECK ADD  CONSTRAINT [FK_proizvodiprodavnice_prodavnice] FOREIGN KEY([idprodavnice])
REFERENCES [dbo].[prodavnice] ([id])
GO
ALTER TABLE [dbo].[proizvodiprodavnice] CHECK CONSTRAINT [FK_proizvodiprodavnice_prodavnice]
GO
ALTER TABLE [dbo].[proizvodiprodavnice]  WITH CHECK ADD  CONSTRAINT [FK_proizvodiprodavnice_proizvodi_tab] FOREIGN KEY([idproizvoda])
REFERENCES [dbo].[proizvodi_tab] ([id])
GO
ALTER TABLE [dbo].[proizvodiprodavnice] CHECK CONSTRAINT [FK_proizvodiprodavnice_proizvodi_tab]
GO
ALTER TABLE [dbo].[user_db]  WITH CHECK ADD  CONSTRAINT [FK_user_db_login_user] FOREIGN KEY([username])
REFERENCES [dbo].[login_user] ([username])
GO
ALTER TABLE [dbo].[user_db] CHECK CONSTRAINT [FK_user_db_login_user]
GO
/****** Object:  StoredProcedure [dbo].[dajKorisnika]    Script Date: 8/7/2020 9:40:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[dajKorisnika] @username char(100) , @password char(100)  
AS  
BEGIN  
SELECT * FROM dbo.user_db db1 WHERE db1.username = @username
AND exists(
	select *
	from dbo.login_user db
	WHERE db1.username = db.username AND db.pass = @password
)
END;
GO
/****** Object:  StoredProcedure [dbo].[dajNajskupljiProizvod]    Script Date: 8/7/2020 9:40:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[dajNajskupljiProizvod]  
AS  
BEGIN  
SELECT * FROM dbo.proizvodi_tab pt WHERE not exists (
	SELECT pt2.cena
	FROM dbo.proizvodi_tab pt2
	WHERE pt2.cena > pt.cena and pt2.id != pt.id
)  
END;
GO
/****** Object:  StoredProcedure [dbo].[dajProizvod]    Script Date: 8/7/2020 9:40:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[dajProizvod] @id int  
AS  
BEGIN  
SELECT * FROM dbo.proizvodi_tab WHERE id = @id  
END;
GO
/****** Object:  StoredProcedure [dbo].[dajSveProizvode]    Script Date: 8/7/2020 9:40:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[dajSveProizvode] @idprodavnice int  
AS  
BEGIN  
	select pp.idproizvoda,p.ime,p.cena,p.roktrajanja,p.idkategorije,pp.kolicina
	from proizvodiprodavnice pp left join proizvodi_tab p on pp.idproizvoda = p.id
	where pp.idprodavnice = @idprodavnice
END;
GO
