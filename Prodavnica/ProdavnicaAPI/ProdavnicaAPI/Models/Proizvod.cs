﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdavnicaAPI
{
    public class Proizvod
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public float Cena { get; set; }
        public DateTime RokTrajanja { get; set; }
        public int IdKategorije { get; set; }
        public int Kolicina { get; set; }
    }
}
