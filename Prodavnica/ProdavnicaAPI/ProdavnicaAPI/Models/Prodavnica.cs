﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProdavnicaAPI.Models
{
    public class Prodavnica
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public int BrojProizvoda { get; set; }
        public List<Proizvod> proizvodi { get; set; }

        public Prodavnica()
        {
            BrojProizvoda = 0;
        }
    }
}
