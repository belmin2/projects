﻿
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProdavnicaAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace ProdavnicaAPI
{
    class Program
    {
        static HttpClient httpClient = new HttpClient();
        static void Main(string[] args)
        {
            RynAsync().GetAwaiter().GetResult();
        }
        static async Task RynAsync()
        {
            httpClient.BaseAddress = new Uri("https://localhost:44349");
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            try
            {
                string token = await CreateToken("login/createtoken");
                Console.WriteLine(token);
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token); 
                /*string response = await DeleteFile("googledrive/deleteFile?id=1Uf8Ut3fRQ6ISDQSMc65_PYmRTkML3bsk");
                Console.WriteLine(response);
                response = await UploadFile("googledrive/uploadFile?path=C:\\Users\\Fujitsu\\Desktop\\pom\\proba.pdf&typeId=1");
                response = await ShareFile("googledrive/sharefile?id=1s6lBxTBP3BfNaIwAbAhPcTQ1IRtkTfNn&user=darkcet406@gmail.com");
                response = await UploadAllFiles("googledrive/uploadAllFiles?path=C:\\Users\\Fujitsu\\Desktop\\pomocni\\");
                response = await GetFileName("googledrive/getfilename?id=1s6lBxTBP3BfNaIwAbAhPcTQ1IRtkTfNn");
                string[] response2 = await DownloadFiles("googledrive/downloadfiles?savePath=C:\\Users\\Fujitsu\\Desktop\\pom\\");
                response = await Home("proizvod");
                response2 = await DajUkupnuCenu("prodavnica/ukupno");
                Console.WriteLine(response);
                response = await UnesiProdavnicu("prodavnica/insert");
                response = await UbaciProizvod("proizvod/insert");
                response = await UnesiKategoriju("kategorija/insert");
                response = await IzbrisiProdavnicu("prodavnica/delete?id=3");
                response = await IzbrisiKategoriju("kategorija/delete?id=1");
                response = await IzbrisiProizvod("proizvod/delete?id=2");
                Proizvod proizvod = await DajProizvod("proizvod/1");
                List<Proizvod> proizvodi = await DajProizvode("proizvod/select");
                Console.WriteLine(System.Text.Json.JsonSerializer.Serialize(proizvodi));
                proizvodi = await DajNajskupljiProizvod("proizvod/najskupli");
                Console.WriteLine(System.Text.Json.JsonSerializer.Serialize(proizvodi));
                proizvodi = await DajProizvodPoKategoriji("proizvod/kategorijaproizvoda?id=1");
                Console.WriteLine(System.Text.Json.JsonSerializer.Serialize(proizvodi));
                List<Prodavnica> prodavnice = await DajSveProdavnice("/prodavnica");
                Console.WriteLine(System.Text.Json.JsonSerializer.Serialize(prodavnice));
                List<FileModel> files = await GetFiles("/googledrive/listFiles");
                Console.WriteLine(System.Text.Json.JsonSerializer.Serialize(files));
                List<Kategorija> kategorije = await DajSveKategorije("kategoirja");*/
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        static async Task<string> Home(string path)
        {
            string result = null;
            HttpResponseMessage responseMessage = await httpClient.GetAsync(path);
            if (responseMessage.IsSuccessStatusCode)
            {
                result = await responseMessage.Content.ReadAsAsync<string>();
            }
            return result;
        }
        static async Task<List<Proizvod>> DajProizvodPoKategoriji(string path)
        {
            List<Proizvod> result = null;
            HttpResponseMessage responseMessage = await httpClient.GetAsync(path);
            if (responseMessage.IsSuccessStatusCode)
            {
                result = await responseMessage.Content.ReadAsAsync<List<Proizvod>>();
            }
            return result;
        }
        static async Task<string> GetFileName(string path)
        {
            string result = null;
            HttpResponseMessage responseMessage = await httpClient.GetAsync(path);
            if (responseMessage.IsSuccessStatusCode)
            {
                result = await responseMessage.Content.ReadAsAsync<string>();
            }
            return result;
        }
        static async Task<List<Kategorija>> DajSveKategorije(string path)
        {
            List<Kategorija> result = null;
            HttpResponseMessage responseMessage = await httpClient.GetAsync(path);
            if (responseMessage.IsSuccessStatusCode)
            {
                result = await responseMessage.Content.ReadAsAsync<List<Kategorija>>();
            }
            return result;
        }
        static async Task<List<Proizvod>> DajNajskupljiProizvod(string path)
        {
            List<Proizvod> result = null;
            HttpResponseMessage responseMessage = await httpClient.GetAsync(path);
            if (responseMessage.IsSuccessStatusCode)
            {
                result = await responseMessage.Content.ReadAsAsync<List<Proizvod>>();
            }
            return result;
        }
        static async Task<string[]> DownloadFiles(string path)
        {
            string[] result = null;
            HttpResponseMessage responseMessage = await httpClient.GetAsync(path);
            if (responseMessage.IsSuccessStatusCode)
            {
                result = await responseMessage.Content.ReadAsAsync<string[]>();
            }
            return result;
        }
        static async Task<string> DownloadFile(string path)
        {
            string result = null;
            HttpResponseMessage responseMessage = await httpClient.GetAsync(path);
            if (responseMessage.IsSuccessStatusCode)
            {
                result = await responseMessage.Content.ReadAsAsync<string>();
            }
            return result;
        }
        static async Task<Proizvod> DajProizvod(string path)
        {
            Proizvod result = null;
            HttpResponseMessage responseMessage = await httpClient.GetAsync(path);
            if (responseMessage.IsSuccessStatusCode)
            {
                result = await responseMessage.Content.ReadAsAsync<Proizvod>();
            }
            return result;
        }
        static async Task<List<FileModel>> GetFiles(string path)
        {
            List<FileModel> result = null;
            HttpResponseMessage responseMessage = await httpClient.GetAsync(path);
            if (responseMessage.IsSuccessStatusCode)
            {
                result = await responseMessage.Content.ReadAsAsync<List<FileModel>>();
            }
            return result;
        }
        static async Task<List<Proizvod>> DajProizvode(string path)
        {
            List<Proizvod> result = null;
            HttpResponseMessage responseMessage = await httpClient.GetAsync(path);
            if (responseMessage.IsSuccessStatusCode)
            {
                result = await responseMessage.Content.ReadAsAsync<List<Proizvod>>();
            }
            return result;
        }
        static async Task<List<Prodavnica>> DajSveProdavnice(string path)
        {
            List<Prodavnica> result = null;
            HttpResponseMessage responseMessage = await httpClient.GetAsync(path);
            if (responseMessage.IsSuccessStatusCode)
            {
                result = await responseMessage.Content.ReadAsAsync<List<Prodavnica>>();
            }
            return result;
        }
        static async Task<string[]> DajUkupnuCenu(string path)
        {
            string[] result = null;
            HttpResponseMessage responseMessage = await httpClient.GetAsync(path);
            if (responseMessage.IsSuccessStatusCode)
            {
                result = await responseMessage.Content.ReadAsAsync<string[]>();
            }
            return result;
        }
        static async Task<string> CreateToken(string path)
        {
            LoginModel login = new LoginModel { Username = "pera", Password = "pera" };
            var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(login));
            var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");
            //string token = "";
            HttpResponseMessage responseMessage = await httpClient.PostAsync(path, httpContent);
            string nes = await responseMessage.Content.ReadAsAsync<string>();   
            return nes;
        }
        static async Task<string> DeleteFile(string path)
        {
            HttpResponseMessage responseMessage = await httpClient.DeleteAsync(path);
            if (responseMessage.IsSuccessStatusCode)
            {
                return "Fajl obrisan.";
            }
            else
            {
                return "Neuspesno obrisan fajl.";
            }
        }
        static async Task<string> IzbrisiProizvod(string path)
        {
            HttpResponseMessage responseMessage = await httpClient.DeleteAsync(path);
            if (responseMessage.IsSuccessStatusCode)
            {
                return "Proizvod izbacen";
            }
            else
            {
                return "Neuspesno izbacen proizvod";
            }
        }
        static async Task<string> IzbrisiKategoriju(string path)
        {
            HttpResponseMessage responseMessage = await httpClient.DeleteAsync(path);
            if (responseMessage.IsSuccessStatusCode)
            {
                return "Kategorija izbrisana uspesno";
            }
            else
            {
                return "Kategorija izbrisana neuspesno";
            }
        }
        static async Task<string> IzbrisiProdavnicu(string path)
        {
            HttpResponseMessage responseMessage = await httpClient.DeleteAsync(path);
            if (responseMessage.IsSuccessStatusCode)
            {
                return "Prodavnica izbrisana uspesno.";
            }
            else
            {
                return "Prodavnica izbrisana neuspesno.";
            }
        }
        static async Task<string> UploadAllFiles(string path)
        {
            HttpResponseMessage responseMessage = await httpClient.PostAsync(path, null);
            if (responseMessage.IsSuccessStatusCode)
            {
                return "Uspesno upload fajlova.";
            }
            else
            {
                return "Neuspesna upload fajlova.";
            }
        }
        static async Task<string> ShareFile(string path)
        {
            HttpResponseMessage responseMessage = await httpClient.PostAsync(path, null);
            if (responseMessage.IsSuccessStatusCode)
            {
                return "Uspesno podela fajla.";
            }
            else
            {
                return "Neuspesna podela fajla.";
            }
        }
        static async Task<string> UnesiKategoriju(string path)
        {
            Kategorija kategorija = new Kategorija
            {
                Naziv = "Higijenski Proizvodi"
            };
            var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(kategorija));
            var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");
            HttpResponseMessage responseMessage = await httpClient.PostAsync(path, httpContent);
            if (responseMessage.IsSuccessStatusCode)
            {
                return "Uspesan unos kategorije.";
            }
            else
            {
                return "Neuspesan unos kategorije.";
            }
        }
        static async Task<string> UnesiProdavnicu(string path)
        {
            Prodavnica proizvod = new Prodavnica
            {
                Naziv = "Roda"
            };
            var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(proizvod));
            var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");
            HttpResponseMessage responseMessage = await httpClient.PostAsync(path, httpContent);
            if (responseMessage.IsSuccessStatusCode)
            {
                return "Uspesan unos prodavnice.";
            }
            else
            {
                return "Neuspesan unos prodavnice.";
            }
        }
        static async Task<string> UbaciProizvod(string path)
        {
            Proizvod proizvod = new Proizvod { 
                Ime = "Voda-Voda", 
                Cena = (float)34.4,
                RokTrajanja = DateTime.Now,
                IdKategorije = 1
            };
            var stringPayload = await Task.Run(() => JsonConvert.SerializeObject(proizvod));
            var httpContent = new StringContent(stringPayload, Encoding.UTF8, "application/json");
            HttpResponseMessage responseMessage = await httpClient.PostAsync(path, httpContent);
            if (responseMessage.IsSuccessStatusCode)
            {
                return "Uspesno ubacivanje proizvoda.";
            }
            else
            {
                return "Neuspesno ubacivanje proizvoda.";
            }
        }
        static async Task<string> UploadFile(string path)
        {
            HttpResponseMessage responseMessage = await httpClient.PostAsync(path, null);
            if (responseMessage.IsSuccessStatusCode)
            {
                return "Uspesan upload fajla.";
            }
            else
            {
                return "Neuspesan upload fajla.";
            }
        }
    }
}
