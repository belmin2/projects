using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Prodavnica1.BLL;
using Prodavnica1.BLL.Interfaces;
using Prodavnica1.DAL;
using Prodavnica1.DAL.Interfaces;
using Prodavnica1.Helper;
using Prodavnica1.Models;
using System.Text;

namespace Prodavnica1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IProizvodBLL, ProizvodBLL>();
            services.AddSingleton<IUserBLL, UserBLL>();
            services.AddSingleton<IProdavnicaBLL, ProdavnicaBLL>();
            services.AddSingleton<IKategorijaBLL, KategorijaBLL>();
            services.AddSingleton<IGoogleDriveApiHelper, GoogleDriveApiHelper>();
            services.AddSingleton<IGoogleDriveBLL, GoogleDriveBLL>();
            services.AddSingleton<IGoogleDriveDAL, GoogleDriveDAL>();
            //Prva baza
            services.AddSingleton<IProizvodDAL, ProizvodDAL>();  
            services.AddSingleton<IUserDAL, UserDAL>();
            services.AddSingleton<IProdavnicaDAL, ProdavnicaDAL>();
            services.AddSingleton<IKategorijaDAL, KategorijaDAL>();
            //Druga baza
            //services.AddSingleton<IProizvodDAL, DrugiProizvodDAL>();
            //services.AddSingleton<IUserDAL, DrugiUserDAL>();
            //services.AddSingleton<IKategorijaDAL, DrugiKategorijaDAL>();
            //services.AddSingleton<IProdavnicaDAL, DrugiProdavnicaDAL>();
            services.AddControllers();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["Jwt:Issuer"],
                    ValidAudience = Configuration["Jwt:Issuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))

                };
            });
            services.AddDataProtection();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDataProtectionProvider provider)
        {
            Init(provider);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseCustomLogger();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        private void Init(IDataProtectionProvider provider)
        {
            ProtectionHelper.Singleton.SetConfig(Configuration);
            ProtectionHelper.Singleton.SetProvider(provider, Configuration["DataProtection:Key:Value"]);
            ProtectionHelper.Singleton.GetSectionValue("DataProtection:Key");
        }
    }
}
