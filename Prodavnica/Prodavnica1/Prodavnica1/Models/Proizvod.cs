﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.Models
{
    public class Proizvod
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public float Cena { get; set; }
        public DateTime RokTrajanja { get; set; }
        public int IdKategorije { get; set; }
        public int Kolicina { get; set; }
    }
}
