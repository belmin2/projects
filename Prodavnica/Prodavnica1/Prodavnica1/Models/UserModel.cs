﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.Models
{
    public class UserModel
    {
        public String Username { get; set; }
        public String Name { get; set; }
        public String Email { get; set; }
        public DateTime Birthdate { get; set; }
        public String Role { get; set; }
    }
}
