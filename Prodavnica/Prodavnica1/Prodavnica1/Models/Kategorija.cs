﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.Models
{
    public class Kategorija
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
    }
}
