﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.Models
{
    public class ShareModel
    {
        public int Id { get; set; }
        public string DriveShareId { get; set; }
        public DriveUserModel User { get; set; }
        public DocumentModel Document { get; set; }
    }
}
