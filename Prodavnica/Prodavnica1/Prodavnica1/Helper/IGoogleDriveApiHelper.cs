﻿using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.Helper
{
    public interface IGoogleDriveApiHelper
    {
        public IList<FileModel> GetFiles();
        public string GetFileName(string id);
        public void DeleteFile(string id);

        public FileModel UploadFile(string path);
        public void DownloadFile(string id, string savePath);
        public string UploadAllFiles(string path);
        public string ShareFile(string id, string user);
        public void DownloadAllFiles(string id, string savePath, string Name, int ind,int i);
    }
}
