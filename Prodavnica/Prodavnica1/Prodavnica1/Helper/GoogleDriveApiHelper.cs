﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Prodavnica1.Helper
{
    public class GoogleDriveApiHelper : IGoogleDriveApiHelper
    {
        private static DriveService driveService;
        public DriveService GetDriveService()
        {
            if (driveService == null)
            {
                return GetDriveServiceSingleton();
            }

            return driveService;
        }
        public DriveService GetDriveServiceSingleton()
        {
            string[] scope = { DriveService.Scope.Drive };

            UserCredential credentials;

            using (FileStream stream = new FileStream(Path.Combine("credentials.json"), FileMode.Open, FileAccess.Read))
            {
                ClientSecrets secrets = GoogleClientSecrets.Load(stream).Secrets;

                credentials = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    secrets, scope, "beco.cu2@gmail.com", CancellationToken.None, null
                ).Result;
            }
            DriveService service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credentials,
                ApplicationName = "GoogleDriveASPNETCore"
            });

            return service;
        }

        public IList<FileModel> GetFiles()
        {
            FilesResource.ListRequest listRequest = GetDriveService().Files.List();
            listRequest.PageSize = 10;
            listRequest.Fields = "nextPageToken,files(id, parents, name, mimeType)";

            IList<Google.Apis.Drive.v3.Data.File> files = listRequest.Execute().Files.ToList();
            List<FileModel> newFiles = new List<FileModel>();

            foreach (Google.Apis.Drive.v3.Data.File file in files)
            {
                newFiles.Add(new FileModel()
                {
                    Id = file.Id,

                    DrivePath = GetFileName(file.Parents.ElementAt(0)),
                    MimeType = file.MimeType,
                    Name = file.Name
                });
            }

            return newFiles;

        }
        public string GetFileName(string id)
        {
            Google.Apis.Drive.v3.Data.File file = new Google.Apis.Drive.v3.Data.File();
            file.Id = id;

            FilesResource.GetRequest getRequest = GetDriveService().Files.Get(id);

            return getRequest.Execute().Name;
        }
        public void DeleteFile(string id)
        {
            FilesResource.DeleteRequest deleteRequest = GetDriveService().Files.Delete(id);

            deleteRequest.Execute();
        }
        public FileModel UploadFile(string path)
        {
            Google.Apis.Drive.v3.Data.File file = new Google.Apis.Drive.v3.Data.File();
            file.Name = Path.GetFileName(path);

            file.MimeType = MimeKit.MimeTypes.GetMimeType(file.Name);

            FilesResource.CreateMediaUpload createMediaUpload;

            using (FileStream stream = new FileStream(path, FileMode.Open))
            {
                createMediaUpload = GetDriveService().Files.Create(file, stream, file.MimeType);
                createMediaUpload.Fields = "id,name,mimeType,parents";
                createMediaUpload.Upload();
            }

            FileModel fileModel = new FileModel()
            {
                Id = createMediaUpload.ResponseBody.Id,
                Name = createMediaUpload.ResponseBody.Name,
                MimeType = createMediaUpload.ResponseBody.MimeType,
                DrivePath = createMediaUpload.ResponseBody.Parents != null ? GetFileName(createMediaUpload.ResponseBody.Parents.ElementAt(0)) : null
            };

            return fileModel;

        }
        public void DownloadAllFiles(string id, string savePath, string name, int ind, int i)
        {
            if (GetFileName(id) != "")
            {
                if (ind == 0)
                {
                    savePath += name;
                    DownloadFile(id, savePath);
                }
                else
                {
                    string[] str = name.Split(".");
                    savePath += str[0] + i +"." + str[1];
                    DownloadFile(id, savePath);
                }
            }
        }
        public string UploadAllFiles(string path)
        {
            var i = 0;
            string[] fajlovi = Directory.GetFiles(path);
            DirectoryInfo dir_info = new DirectoryInfo(path);
            string folder = dir_info.Name;
            Google.Apis.Drive.v3.Data.File foldernovi = new Google.Apis.Drive.v3.Data.File();
            foldernovi.Name = folder;
            foldernovi.Description = "";
            foldernovi.MimeType = "application/vnd.google-apps.folder";
            //drive.Files.Create(foldernovi,stream,foldernovi.MimeType);
            FilesResource.CreateRequest request;
            request = GetDriveService().Files.Create(foldernovi);
            request.Fields = "id";
            var fajlzaid = request.Execute();
            var folderid = fajlzaid.Id;
            for (i = 0; i < fajlovi.Length; i++)
            {
                string putanja = fajlovi[i];
                string[] ekstenzije = fajlovi[i].Split(".");
                string ekstenzija = ekstenzije[1];
                Google.Apis.Drive.v3.Data.File file = new Google.Apis.Drive.v3.Data.File();
                file.Name = Path.GetFileName(putanja);

                file.MimeType = "application/" + ekstenzija;

                file.Parents = new List<string>
                {
                    folderid
                };

                FilesResource.CreateMediaUpload createMediaUpload;

                using (FileStream stream = new FileStream(putanja, FileMode.Open))
                {
                    createMediaUpload = GetDriveService().Files.Create(file, stream, file.MimeType);
                    createMediaUpload.Fields = "id";
                    createMediaUpload.Upload();
                }
            }
            return folder;
        }
        public void DownloadFile(string id, string savePath)
        {
            FilesResource.GetRequest getRequest = GetDriveService().Files.Get(id);
            MemoryStream stream = new MemoryStream();
            getRequest.MediaDownloader.ProgressChanged += (Google.Apis.Download.IDownloadProgress downloadProgres) =>
            {
                switch (downloadProgres.Status)
                {
                    case Google.Apis.Download.DownloadStatus.Completed:
                        {
                            using (FileStream fileStream = new FileStream(savePath, FileMode.Create, FileAccess.Write))
                            {
                                stream.WriteTo(fileStream);
                                break;
                            }
                        }
                }
            };
            getRequest.Download(stream);
        }
        public string ShareFile(string id, string user)
        {

            Permission premission = new Permission()
            {
                EmailAddress = user,
                Role = "reader",
                Type = "user"
            };

            PermissionsResource.CreateRequest createRequest = GetDriveService().Permissions.Create(premission, id);
            return createRequest.Execute().Id;
        }
    }
}
