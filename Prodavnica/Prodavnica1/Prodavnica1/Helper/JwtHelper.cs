﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Prodavnica1.BLL;
using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Prodavnica1.Helper
{
    public class JwtHelper
    {
        private static readonly JwtHelper singleton;
        private IUserBLL _userBLL;
        private IConfiguration _configuration;
        static JwtHelper()
        {
            singleton = new JwtHelper();
        }

        public static JwtHelper Singleton
        {
            get { return singleton; }
        }

        public object JwtRegisteredClainNames { get; private set; }

        public void SetConfig(IConfiguration configuration)
        {
            singleton._configuration = configuration;  
        }

        public UserModel AuthenticateUser(LoginModel loginUser,IUserBLL userBLL)
        {
            _userBLL = userBLL;
            UserModel user = userBLL.DajKorisnika(loginUser.Username,loginUser.Password);
            /*UserModel user = new UserModel()
            {
                Username = "Ad",
                Name = "Pera",
                Role = "Korisnik",
            };*/
            return user;
        }

        public string BuildToken(UserModel user)
        {
            /*Claim[] claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub,user.Name),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Birthdate,user.Birthdate.ToString("yyyy-MM-dd")),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString())
            };*/

            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            SigningCredentials credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor()
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Name),
                    new Claim(ClaimTypes.Role,user.Role)
                }),
                Expires = DateTime.Now.AddMinutes(60),
                SigningCredentials = credentials,
                Audience = _configuration["Jwt:Issuer"],
                Issuer = _configuration["Jwt:Issuer"]
            };
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor); 

            return tokenHandler.WriteToken(token);
        }
    }
}
