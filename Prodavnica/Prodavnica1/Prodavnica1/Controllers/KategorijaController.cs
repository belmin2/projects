﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Prodavnica1.BLL.Interfaces;
using Prodavnica1.Models;

namespace Prodavnica1.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class KategorijaController : ControllerBase
    {
        private readonly IKategorijaBLL _kategorijaBLL;
        public KategorijaController(IKategorijaBLL kategorijaBLL)
        {
            _kategorijaBLL = kategorijaBLL;
        }
        [Authorize]
        [HttpGet]
        public List<Kategorija> DajSveKategorije()
        {
            return _kategorijaBLL.DajSveKategorije();
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("insert")]
        public void Insert([FromBody] Kategorija kategorija)
        {
            _kategorijaBLL.Insert(kategorija);
        }
        [Authorize(Roles = "Admin")]
        [HttpDelete]
        [Route("delete")]
        public void IzbrisiKategoriju(int id)
        {
            _kategorijaBLL.IzbrisiKategoriju(id);
        }
    }
}
