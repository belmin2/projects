﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Prodavnica1.BLL.Interfaces;
using Prodavnica1.Helper;
using Prodavnica1.Models;

namespace Prodavnica1.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class GoogleDriveController : ControllerBase
    {
        private readonly IGoogleDriveBLL _googleDriveBLL;
        private readonly IGoogleDriveApiHelper _googleDriveApiHelper;
        public GoogleDriveController(IGoogleDriveApiHelper googleDriveApiHelper, IGoogleDriveBLL googleDriveBLL)
        {
            _googleDriveBLL = googleDriveBLL;
            _googleDriveApiHelper = googleDriveApiHelper;
        }
        [Authorize]
        [HttpGet]
        [Route("listFiles")]
        public IList<FileModel> ListFiles()
        {
            return _googleDriveApiHelper.GetFiles();
        }
        [Authorize]
        [HttpGet]
        [Route("getFileName")]
        public string GetFileName(string id)
        {
            return _googleDriveApiHelper.GetFileName(id);
        }
        [Authorize]
        [HttpDelete]
        [Route("deleteFile")]
        public void DeleteFile(string id)
        {
            _googleDriveApiHelper.DeleteFile(id);
            _googleDriveBLL.DeleteFile(id);
        }

        [Authorize]
        [HttpGet]
        [Route("authorize")]
        public IActionResult Init()
        {
            return Ok();
        }
        [Authorize]
        [HttpPost]
        [Route("uploadFile")]
        public string UploadFile(string path, int typeId)
        {
            FileModel file = _googleDriveApiHelper.UploadFile(path);

            _googleDriveBLL.CreateDocument(new DocumentModel()
            {
                Name = file.Name,
                DriveDocumentId = file.Id,
                DrivePath = file.DrivePath,
                Type = new TypeModel()
                {
                    Id = typeId
                }
            });

            return file.Id;
        }
        [Authorize]
        [HttpGet]
        [Route("downloadFile")]
        public string DownloadFile(string id, string savePath)
        {
            _googleDriveApiHelper.DownloadFile(id, savePath);

            return _googleDriveBLL.GetDocumentData(id);
        }
        [Authorize]
        [HttpPost]
        [Route("uploadAllFiles")]
        public string UploadAllFiles(string path)
        {
            return _googleDriveApiHelper.UploadAllFiles(path);
        }
        [Authorize]
        [HttpPost]
        [Route("shareFile")]
        public string ShareFile(string id, string user)
        {
            string shareId = _googleDriveApiHelper.ShareFile(id, user);

            _googleDriveBLL.CreateShare(shareId, id, user);

            return shareId;
        }
        [Authorize]
        [HttpGet]
        [Route("downloadFiles")]
        public string[] DownloadFilesByType(string savePath, string typeId)
        {
            var i = 0;
            var j = 0;
            var ind = 0;
            List<DocumentModel> dokumenti = _googleDriveBLL.GetAllDocumentsByType(typeId);
            string[] fajlovi = new string[dokumenti.Count];
            for (i = 0; i < dokumenti.Count; i++)
            {
                ind = 0;
                for (j = 0; j < fajlovi.Length; j++)
                {
                    if (dokumenti[i].Name == fajlovi[j])
                    {
                        ind = 1;
                    }
                }
                _googleDriveApiHelper.DownloadAllFiles(dokumenti[i].DriveDocumentId, savePath, dokumenti[i].Name, ind,i);
                fajlovi[i] = dokumenti[i].Name;
            }
            return fajlovi;
        }
    }
}
