﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Prodavnica1.BLL;
using Prodavnica1.Helper;
using Prodavnica1.Models;

namespace Prodavnica1.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IUserBLL _userBLL;
        
        public LoginController(IConfiguration configuration,IUserBLL userBLL)
        {
            _configuration = configuration;
            _userBLL = userBLL;
            JwtHelper.Singleton.SetConfig(configuration);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("createToken")]
        public string CreateToken([FromBody] LoginModel loginModel)
        {
            //IActionResult response = Unauthorized();
            var token = "";
            UserModel user = JwtHelper.Singleton.AuthenticateUser(loginModel,_userBLL);

            if (user != null && user.Username != null)
            {
                token = JwtHelper.Singleton.BuildToken(user);
                //response = Ok(new { token = token });
            }

            return token;
        }
    }
}
