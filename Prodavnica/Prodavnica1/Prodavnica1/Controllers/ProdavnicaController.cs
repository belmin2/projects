﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Prodavnica1.BLL;
using Prodavnica1.Models;

namespace Prodavnica1.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProdavnicaController : ControllerBase
    {
        private readonly IProdavnicaBLL _prodavnicaBLL;
        public ProdavnicaController(IProdavnicaBLL prodavnicaBLL)
        {
            _prodavnicaBLL = prodavnicaBLL;
        }
        [Authorize]
        [HttpGet]
        public List<Prodavnica> DajSveProdavnice()
        {
            return _prodavnicaBLL.DajSveProdavnice();
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("insert")]
        public void Insert([FromBody] Prodavnica prodavnica)
        {
            _prodavnicaBLL.Insert(prodavnica);
        }
        [Authorize(Roles = "Admin")]
        [HttpDelete]
        [Route("delete")]
        public void IzbrisiProdavnicu(int id)
        {
            _prodavnicaBLL.IzbrisiProdavnicu(id);
        }
        [Authorize]
        [HttpGet]
        [Route("ukupno")]
        public string[] DajUkupnuCenu()
        {
            var i = 0;
            var j = 0;
            float suma = 0;
            List<Prodavnica> prodavnice = _prodavnicaBLL.DajSveProdavnice();
            string[] pomocni = new string[prodavnice.Count];
            for (i=0;i < prodavnice.Count();i++)
            {
                suma = 0;
                List<Proizvod> proizvod = prodavnice[i].proizvodi;
                for (j = 0; j < proizvod.Count(); j++)
                {
                    if (proizvod != null)
                    {
                        Proizvod proz = proizvod[j];
                        suma += proz.Cena * proz.Kolicina;
                    }
                }
                pomocni[i] = "Prodavnica: " + prodavnice[i].Naziv + " | Vrednost:" + suma;
            }
            return pomocni;
        }
    }
}
