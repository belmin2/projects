﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Prodavnica1.BLL;
using Prodavnica1.DAL;
using Prodavnica1.Models;

namespace Prodavnica1.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProizvodController : ControllerBase
    {
        private readonly IProizvodBLL _proizvodBLL;
        public ProizvodController(IProizvodBLL proizvodBLL)
        {
            _proizvodBLL = proizvodBLL;
        }
        [HttpGet]
        public string Home()
        {
            return "Welcome";
        }
        [Authorize]
        [HttpGet]
        [Route("select")]
        public List<Proizvod> Select()
        {
            return _proizvodBLL.Select();
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("insert")]
        public void Insert([FromBody] Proizvod proizvod)
        {
            _proizvodBLL.Insert(proizvod);
        }
        [Authorize]
        [HttpGet]
        [Route("najskuplji")]
        public List<Proizvod> DajNajskupljiProizvod()
        {
            return _proizvodBLL.DajNajskupljiProizvod();
        }
        [Authorize]
        [HttpGet("{id}")]
        public Proizvod DajProizvod(int id)
        {
            List<Proizvod> proizvod = _proizvodBLL.DajProizvod(id);
            if(proizvod == null)
            {
                return proizvod[0];
            }
            return proizvod[0];
        }
        [Authorize(Roles ="Admin")]
        [HttpDelete]
        [Route("delete")]
        public void IzbrisiProizvod(int id)
        {
            _proizvodBLL.IzbrisiProizvod(id);
        }
        [Authorize]
        [HttpGet]
        [Route("kategorijaproizvoda")]
        public List<Proizvod> DajProizvodePoKategoriji(int id)
        {
            return _proizvodBLL.DajProizvodePoKategoriji(id);
        }
    }
}
