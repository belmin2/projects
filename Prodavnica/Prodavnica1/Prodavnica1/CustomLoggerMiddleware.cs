﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Prodavnica1
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class CustomLoggerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public CustomLoggerMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<CustomLoggerMiddleware>();
        }

        public async Task Invoke(HttpContext context)
        {
            _logger.LogInformation("**Handling request: " + context.Request.Path + "**");
            await _next.Invoke(context);
            _logger.LogInformation("**Finished handling request.**");
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class CustomLoggerMiddlewareExtensions
    {
        public static IApplicationBuilder UseCustomLoggerMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CustomLoggerMiddleware>();
        }
    }
}
