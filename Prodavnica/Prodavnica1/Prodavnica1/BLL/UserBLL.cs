﻿using Prodavnica1.DAL;
using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.BLL
{
    public class UserBLL : IUserBLL
    {
        private readonly IUserDAL _userDAL;
        public UserBLL(IUserDAL userDAL)
        {
            _userDAL = userDAL;
        }
        public UserModel DajKorisnika(string Username,string Password)
        {
            return _userDAL.DajKorisnika(Username,Password);
        }
    }
}
