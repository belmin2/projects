﻿using Prodavnica1.DAL;
using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.BLL
{
    public class ProdavnicaBLL : IProdavnicaBLL
    {
        private readonly IProdavnicaDAL _prodavnicaDAL;
        public ProdavnicaBLL(IProdavnicaDAL prodavnicaDAL)
        {
            _prodavnicaDAL = prodavnicaDAL;
        }
        public List<Prodavnica> DajSveProdavnice()
        {
            return _prodavnicaDAL.DajSveProdavnice();
        }

        public void Insert(Prodavnica prodavnica)
        {
            _prodavnicaDAL.Insert(prodavnica);
        }
        public void IzbrisiProdavnicu(int Id)
        {
            _prodavnicaDAL.IzbrisiProdavnicu(Id);
        }
    }
}
