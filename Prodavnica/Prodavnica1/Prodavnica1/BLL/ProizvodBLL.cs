﻿using Prodavnica1.DAL;
using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.BLL
{
    public class ProizvodBLL : IProizvodBLL
    {
        private readonly IProizvodDAL _proizvodDAL;
        public ProizvodBLL(IProizvodDAL proizvodDAL)
        {
            _proizvodDAL = proizvodDAL;
        }
        public List<Proizvod> DajNajskupljiProizvod()
        {
            return _proizvodDAL.DajNajskupljiProizvod();
        }

        public void Insert(Proizvod proizvod)
        {
            _proizvodDAL.Insert(proizvod);
        }

        public List<Proizvod> Select()
        {
            return _proizvodDAL.Select();
        }
        public List<Proizvod> DajProizvod(int id)
        {
            return _proizvodDAL.DajProizvod(id);
        }
        public void IzbrisiProizvod(int Id)
        {
            _proizvodDAL.IzbrisiProizvod(Id);
        }

        public List<Proizvod> DajProizvodePoKategoriji(int id)
        {
            return _proizvodDAL.DajProizvodePoKategoriji(id);
        }
    }
}
