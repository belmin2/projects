﻿using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.BLL
{
    public interface IUserBLL
    {
        public UserModel DajKorisnika(string Username,string Password);
    }
}
