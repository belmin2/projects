﻿using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.BLL.Interfaces
{
    public interface IGoogleDriveBLL
    {
        public void CreateDocument(DocumentModel documentModel);
        public string GetDocumentData(string driveId);
        public void CreateShare(string shareId, string fileId, string user);
        public List<DocumentModel> GetAllDocumentsByType(string typeId);
        public void DeleteFile(string id);
    }
}
