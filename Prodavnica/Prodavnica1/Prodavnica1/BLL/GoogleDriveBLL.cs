﻿using Prodavnica1.BLL.Interfaces;
using Prodavnica1.DAL.Interfaces;
using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.BLL
{
    public class GoogleDriveBLL : IGoogleDriveBLL
    {
        private readonly IGoogleDriveDAL _googleDriveDAL;
        public GoogleDriveBLL(IGoogleDriveDAL googleDriveDAL)
        {
            _googleDriveDAL = googleDriveDAL;
        }

        public void CreateDocument(DocumentModel documentModel)
        {
            _googleDriveDAL.CreateDocument(documentModel);
        }

        public string GetDocumentData(string driveId)
        {
            return _googleDriveDAL.GetDocumentData(driveId);
        }

        public void CreateShare(string shareId, string fileId, string user)
        {
            _googleDriveDAL.CreateShare(shareId, fileId, user);
        }
        public List<DocumentModel> GetAllDocumentsByType(string typeId)
        {
            return _googleDriveDAL.GetAllDocumentsByType(typeId);
        }

        public void DeleteFile(string id)
        {
            _googleDriveDAL.DeleteFile(id);
        }
    }
}
