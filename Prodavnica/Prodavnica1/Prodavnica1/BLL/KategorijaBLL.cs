﻿using Prodavnica1.BLL.Interfaces;
using Prodavnica1.DAL.Interfaces;
using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.BLL
{
    public class KategorijaBLL : IKategorijaBLL
    {
        private readonly IKategorijaDAL _kategorijaDAL;
        public KategorijaBLL(IKategorijaDAL kategorijaDAL)
        {
            _kategorijaDAL = kategorijaDAL;
        }
        public List<Kategorija> DajSveKategorije()
        {
            return _kategorijaDAL.DajSveKategorije();
        }

        public void Insert(Kategorija kategorija)
        {
            _kategorijaDAL.Insert(kategorija);   
        }

        public void IzbrisiKategoriju(int id)
        {
            _kategorijaDAL.IzbrisiKategoriju(id);
        }
    }
}
