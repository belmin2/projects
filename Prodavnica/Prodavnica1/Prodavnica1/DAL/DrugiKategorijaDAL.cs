﻿using Microsoft.Extensions.Configuration;
using Prodavnica1.DAL.Interfaces;
using Prodavnica1.Helper;
using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.DAL
{
    public class DrugiKategorijaDAL : IKategorijaDAL
    {
        private string ConnectionString;
        private readonly IConfiguration _configuration;
        public DrugiKategorijaDAL(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = ProtectionHelper.Singleton.GetSectionValue("ConnectionStrings:Connection2");
        }
        public List<Kategorija> DajSveKategorije()
        {
            List<Kategorija> kategorije = new List<Kategorija>();
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {

                try
                {
                    connection.Open();
                    SQLiteCommand command = connection.CreateCommand();
                    command.CommandText = $"SELECT * FROM kategorije";

                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        kategorije.Add(new Kategorija()
                        {
                            Id = Int32.Parse(reader[0].ToString()),
                            Naziv = reader[1].ToString()
                        });
                    }
                    reader.Close();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return kategorije;
        }

        public void Insert(Kategorija kategorija)
        {
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    SQLiteCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO kategorije VALUES (NULL,@naziv)";

                    SQLiteParameter parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.String;
                    parameter.ParameterName = "@naziv";
                    parameter.Value = kategorija.Naziv;
                    command.Parameters.Add(parameter);

                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void IzbrisiKategoriju(int id)
        {
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    SQLiteCommand command = connection.CreateCommand();
                    command.CommandText = "DELETE FROM kategorije WHERE id=@id";

                    SQLiteParameter parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.String;
                    parameter.ParameterName = "@id";
                    parameter.Value = id;
                    command.Parameters.Add(parameter);

                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}
