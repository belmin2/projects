﻿using Microsoft.Extensions.Configuration;
using Prodavnica1.Helper;
using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.DAL
{
    public class DrugiProdavnicaDAL : IProdavnicaDAL
    {
        private string ConnectionString;
        private readonly IConfiguration _configuration;
        public DrugiProdavnicaDAL(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = ProtectionHelper.Singleton.GetSectionValue("ConnectionStrings:Connection2");
        }
        public void IzbrisiProdavnicu(int Id)
        {
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    SQLiteCommand command = connection.CreateCommand();
                    command.CommandText = "DELETE FROM prodavnice WHERE id=@id";

                    SQLiteParameter parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.String;
                    parameter.ParameterName = "@id";
                    parameter.Value = Id;
                    command.Parameters.Add(parameter);

                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public void Insert(Prodavnica prodavnica)
        {
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    SQLiteCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO prodavnice VALUES (NULL,@ime,0)";

                    SQLiteParameter parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.String;
                    parameter.ParameterName = "@ime";
                    parameter.Value = prodavnica.Naziv;
                    command.Parameters.Add(parameter);

                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        public List<Proizvod> DajSveProizvode(int idProdavnice)
        {
            List<Proizvod> proizvodi = new List<Proizvod>();
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {

                try
                {
                    connection.Open();
                    SQLiteCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT pp.idproizvoda,p.ime,p.cena,p.roktrajanja,p.idkategorije,pp.kolicina FROM proizvodiprodavnice pp LEFT JOIN proizvodi_tab p on pp.idproizvoda = p.id WHERE pp.idprodavnice = @idprodavnice";

                    SQLiteParameter parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.Int32;
                    parameter.Value = idProdavnice;
                    parameter.ParameterName = "@idprodavnice";
                    command.Parameters.Add(parameter);

                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        proizvodi.Add(new Proizvod()
                        {
                            Id = Int32.Parse(reader[0].ToString()),
                            Ime = reader[1].ToString(),
                            Cena = (float)Decimal.Parse(reader[2].ToString()),
                            RokTrajanja = DateTime.Parse(reader[3].ToString()),
                            IdKategorije = Int32.Parse(reader[4].ToString()),
                            Kolicina = Int32.Parse(reader[5].ToString())
                        });
                    }
                    reader.Close();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return proizvodi;
        }
        public int prebrojProizvode(int idProdavnice)
        {
            int broj = 0;
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {

                try
                {
                    connection.Open();
                    SQLiteCommand command = connection.CreateCommand();
                    command.CommandText = $"SELECT COUNT(*) FROM proizvodiprodavnice WHERE idprodavnice = @idprodavnice";

                    SQLiteParameter parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.Int32;
                    parameter.Value = idProdavnice;
                    parameter.ParameterName = "@idprodavnice";
                    command.Parameters.Add(parameter);

                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        broj = Int32.Parse(reader[0].ToString());
                    }
                    reader.Close();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return broj;
        }
        public List<Prodavnica> DajSveProdavnice()
        {
            List<Prodavnica> prodavnice = new List<Prodavnica>();
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {

                try
                {
                    connection.Open();
                    SQLiteCommand command = connection.CreateCommand();
                    command.CommandText = $"SELECT * FROM prodavnice";

                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        prodavnice.Add(new Prodavnica()
                        {
                            Id = Int32.Parse(reader[0].ToString()),
                            Naziv = reader[1].ToString(),
                            BrojProizvoda = prebrojProizvode(Int32.Parse(reader[0].ToString())),
                            proizvodi = DajSveProizvode(Int32.Parse(reader[0].ToString()))
                        });
                    }
                    reader.Close();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return prodavnice;
        }
    }
}
