﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Prodavnica1.Helper;
using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.DAL
{
    public class UserDAL : IUserDAL
    {
        private string ConnectionString;
        private readonly IConfiguration _configuration;
        public UserDAL(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = ProtectionHelper.Singleton.GetSectionValue("ConnectionStrings:Connection1");
        }
        public UserModel DajKorisnika(string Username,string Password)
        {
            UserModel user = new UserModel();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("dajKorisnika", connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.String;
                    parameter.Value = Username;
                    parameter.ParameterName = "@username";
                    command.Parameters.Add(parameter);

                    parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.String;
                    parameter.Value = Password;
                    parameter.ParameterName = "@password";
                    command.Parameters.Add(parameter);

                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        user = new UserModel()
                        {
                            Username = reader[1].ToString(),
                            Name = reader[2].ToString(),
                            Email = reader[3].ToString(),
                            Birthdate = DateTime.Parse(reader[4].ToString()),
                            Role = reader[5].ToString()
                        };
                    }

                    reader.Close();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return user;
        }
    }
}
