﻿using Microsoft.Extensions.Configuration;
using Prodavnica1.Helper;
using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.DAL
{
    public class DrugiUserDAL : IUserDAL
    {
        private string ConnectionString;
        private readonly IConfiguration _configuration;
        public DrugiUserDAL(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = ProtectionHelper.Singleton.GetSectionValue("ConnectionStrings:Connection2");
        }
        public UserModel DajKorisnika(string Username, string Password)
        {
            UserModel user = new UserModel();
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    SQLiteCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT * FROM user_db db1 WHERE db1.username = @username AND EXISTS ( SELECT * FROM login_user db WHERE db1.username=db.username AND db.pass = @password )";

                    SQLiteParameter parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.String;
                    parameter.Value = Username;
                    parameter.ParameterName = "@username";
                    command.Parameters.Add(parameter);

                    parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.String;
                    parameter.Value = Password;
                    parameter.ParameterName = "@password";
                    command.Parameters.Add(parameter);

                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        user = new UserModel()
                        {
                            Username = reader[1].ToString(),
                            Name = reader[2].ToString(),
                            Email = reader[3].ToString(),
                            Birthdate = DateTime.Parse(reader[4].ToString()),
                            Role = reader[5].ToString()
                        };
                    }

                    reader.Close();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return user;
        }
    }
}
