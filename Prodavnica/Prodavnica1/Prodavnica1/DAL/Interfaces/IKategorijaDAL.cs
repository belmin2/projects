﻿using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.DAL.Interfaces
{
    public interface IKategorijaDAL
    {
        public void Insert(Kategorija kategorija);
        public void IzbrisiKategoriju(int id);
        public List<Kategorija> DajSveKategorije();
    }
}
