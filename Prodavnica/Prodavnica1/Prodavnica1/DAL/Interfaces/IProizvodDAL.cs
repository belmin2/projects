﻿using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.DAL
{
    public interface IProizvodDAL
    {
        public List<Proizvod> DajProizvod(int id);
        public List<Proizvod> Select();
        public void Insert(Proizvod proizvod);
        public List<Proizvod> DajNajskupljiProizvod();
        public void IzbrisiProizvod(int Id);
        public List<Proizvod> DajProizvodePoKategoriji(int id);
    }
}
