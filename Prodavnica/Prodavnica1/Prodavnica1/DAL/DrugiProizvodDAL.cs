﻿using Microsoft.Extensions.Configuration;
using Prodavnica1.Helper;
using Prodavnica1.Models;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Threading.Tasks;

namespace Prodavnica1.DAL
{
    public class DrugiProizvodDAL : IProizvodDAL
    {
        private string ConnectionString;
        private readonly IConfiguration _configuration;
        public DrugiProizvodDAL(IConfiguration configuration)
        {
            _configuration = configuration;
            ConnectionString = ProtectionHelper.Singleton.GetSectionValue("ConnectionStrings:Connection2");
        }
        public List<Proizvod> DajProizvod(int id)
        {
            List<Proizvod> proizvodi = new List<Proizvod>();
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    SQLiteCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT * FROM proizvodi_tab WHERE id = @id";

                    SQLiteParameter parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.Int32;
                    parameter.Value = id;
                    parameter.ParameterName = "@id";
                    command.Parameters.Add(parameter);

                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        proizvodi.Add(new Proizvod()
                        {
                            Id = Int32.Parse(reader[0].ToString()),
                            Ime = reader[1].ToString(),
                            Cena = (float)Decimal.Parse(reader[2].ToString()),
                            RokTrajanja = DateTime.Parse(reader[3].ToString()),
                            IdKategorije = Int32.Parse(reader[4].ToString())
                        });
                    }

                    reader.Close();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return proizvodi;
        }
        public List<Proizvod> DajNajskupljiProizvod()
        {
            List<Proizvod> proizvodi = new List<Proizvod>();
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    SQLiteCommand command = connection.CreateCommand();
                    command.CommandText = "SELECT * FROM proizvodi_tab pt WHERE NOT EXISTS ( SELECT pt2.cena FROM proizvodi_tab pt2 WHERE pt2.cena > pt.cena and pt2.id != pt.id )";

                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        proizvodi.Add(new Proizvod()
                        {
                            Ime = reader[1].ToString(),
                            Cena = (float)Decimal.Parse(reader[2].ToString()),
                            RokTrajanja = DateTime.Parse(reader[3].ToString()),
                            IdKategorije = Int32.Parse(reader[4].ToString())
                        });
                    }

                    reader.Close();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return proizvodi;
        }
        public List<Proizvod> Select()
        {
            List<Proizvod> proizvodi = new List<Proizvod>();
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {

                try
                {
                    connection.Open();
                    SQLiteCommand command = connection.CreateCommand();
                    command.CommandText = $"SELECT * FROM proizvodi_tab";

                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        proizvodi.Add(new Proizvod()
                        {
                            Id = Int32.Parse(reader[0].ToString()),
                            Ime = reader[1].ToString(),
                            Cena = (float)Decimal.Parse(reader[2].ToString()),
                            RokTrajanja = DateTime.Parse(reader[3].ToString()),
                            IdKategorije = Int32.Parse(reader[4].ToString())
                        });
                    }
                    reader.Close();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                return proizvodi;
            }
        }
        public void Insert(Proizvod proizvod)
        {
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    SQLiteCommand command = connection.CreateCommand();
                    command.CommandText = "INSERT INTO proizvodi_tab VALUES (NULL,@ime,@cena,@roktrajanja,@idkategorije)";

                    SQLiteParameter parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.String;
                    parameter.ParameterName = "@ime";
                    parameter.Value = proizvod.Ime;
                    command.Parameters.Add(parameter);


                    parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.Decimal;
                    parameter.ParameterName = "@cena";
                    parameter.Value = proizvod.Cena;
                    command.Parameters.Add(parameter);

                    parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.String;
                    parameter.ParameterName = "@roktrajanja";
                    parameter.Value = proizvod.RokTrajanja.ToString();
                    command.Parameters.Add(parameter);

                    parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.Int32;
                    parameter.ParameterName = "@idkategorije";
                    parameter.Value = proizvod.IdKategorije;
                    command.Parameters.Add(parameter);

                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void IzbrisiProizvod(int Id)
        {
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                try
                {
                    connection.Open();
                    SQLiteCommand command = connection.CreateCommand();
                    command.CommandText = "DELETE FROM proizvodi_tab WHERE id=@id";

                    SQLiteParameter parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.String;
                    parameter.ParameterName = "@id";
                    parameter.Value = Id;
                    command.Parameters.Add(parameter);

                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public List<Proizvod> DajProizvodePoKategoriji(int id)
        {
            List<Proizvod> proizvodi = new List<Proizvod>();
            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {

                try
                {
                    connection.Open();
                    SQLiteCommand command = connection.CreateCommand();
                    command.CommandText = $"SELECT * FROM proizvodi_tab WHERE idkategorije = @id";

                    SQLiteParameter parameter = command.CreateParameter();
                    parameter.DbType = System.Data.DbType.String;
                    parameter.ParameterName = "@id";
                    parameter.Value = id;
                    command.Parameters.Add(parameter);

                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        proizvodi.Add(new Proizvod()
                        {
                            Id = Int32.Parse(reader[0].ToString()),
                            Ime = reader[1].ToString(),
                            Cena = (float)Decimal.Parse(reader[2].ToString()),
                            RokTrajanja = DateTime.Parse(reader[3].ToString()),
                            IdKategorije = Int32.Parse(reader[4].ToString())
                        });
                    }
                    reader.Close();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                return proizvodi;
            }
        }
    }
}
