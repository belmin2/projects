package tacos.data;

import org.springframework.data.repository.CrudRepository;
import tacos.Ingredient;

public interface IngredinetRepository extends CrudRepository<Ingredient, String> {
    //Iterable<Ingredient> findAll();
    //Ingredient findOne(String id);
    //Ingredient save(Ingredient ingredient);
}
