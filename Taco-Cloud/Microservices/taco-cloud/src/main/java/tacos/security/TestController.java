package tacos.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import tacos.Order;
import tacos.data.UserRepository;

@Slf4j
@Controller
@RequestMapping("/test")
@SessionAttributes("user")
public class TestController {
    private UserRepository userRepo;

    public TestController(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    @GetMapping
    public String getAll(Model model){
        model.addAttribute("users", userRepo.findAll());
        System.out.println(userRepo.findAll());
        return "test";
    }
}
