package tacos.data;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import tacos.Ingredient;

@CrossOrigin(origins="*")
public interface IngredinetRepository extends ReactiveCrudRepository<Ingredient, String> {
    //Iterable<Ingredient> findAll();
    //Ingredient findOne(String id);
    //Ingredient save(Ingredient ingredient);
}
