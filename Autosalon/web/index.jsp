
<%-- 
    Document   : index
    Created on : Apr 21, 2020, 1:32:10 PM
    Author     : UltraBook
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<title>Beogradski Sajam</title>
	<link href="style1.css" type="text/css" rel="stylesheet">
	<script src='show.js'></script>
</head>
<body onload='promeniSlike()'>
<div id='novi'>
	<div id='meni'>
		<ul>
		<li><a href="index.jsp">Pocetna</a></li>
		<li><a href='o_nama.jsp'>O Nama</a></li>
		<li><a href='kontakt.jsp'>Kontakt</a></li>
		<li><a href='kalendar.jsp'>Kalendar</a></li>
		<li><a href='kupi.jsp'>Naruci vozilo</a></li>
		</ul>
	</div>
	<div id="logo">
	</div>
	
	<div id='novosti'>
		<div id='novosti2'>
		<h1>Novosti i Najave</h1>
		
		<p>Svečano otvaranje 54. Međunarodnog Salona automobila i 13. Međunarodnog sajma motocikala MotoPassion biće održano u četvrtak, 21. marta 2019. godine, u 18.00 sati, u hali 1 Beogradskog sajma.

Sajmove će svečano otvoriti pozorišni, televizijski i filmski glumac Vlasta Velisavljević,  koji će se u prigodnom i neformalnom govoru šaljivo osvrnuti na automobile i vozačke dozvole.

Velisavljević je, sa svoje 92 godine, jedan od dvojice najstarijih vlasnika uredne vozačke dozvole, a na otvaranje će doći – svojim automobilom.

Posetiocima, gostima i izlagačima obratiće se i Igor Ševo, predsednik Skupštine Udruženja proizvođača drumskih vozila Srbije, i dr Đorđo Markeđani, predsednik Izvršnog odbora DDOR Novi Sad, generalnog sponzora Salona automobila.</p>
		</div>
	</div>
	<div id="galerija">
	<h1 >
            <br>Galerija
	</h1>
	<div id="prvi"></div>
	<div id="drugi"></div>
	</div>
	<div id="footer">
	<br>
	<br>
	 <br>
	 E-mail:salon-automobila14@gmail.com<br>
	Adresa:Bulevar Kralja Aleksandra 219<br>	
	Kontakt: +381 65/576-7694 <br>	
	Belmin Cuturic &#169 2019/20
	
	
	</div>
	</div>
</body>
</html>