<%-- 
    Document   : kalendar
    Created on : Apr 21, 2020, 1:38:46 PM
    Author     : UltraBook
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

<title>Beogradski Sajam</title>
<link href="style1.css" type="text/css" rel="stylesheet">

</head>
<body>

<div id="novi">
<div id='meni'>
		<ul>
		<li><a href="index.jsp">Pocetna</a></li>
		<li><a href='o_nama.jsp'>O Nama</a></li>
		<li><a href='kontakt.jsp'>Kontakt</a></li>
		<li><a href='kalendar.jsp'>Kalendar</a></li>
		<li><a href='kupi.jsp'>Naruci vozilo</a></li>
		</ul>
	</div>
	<div id="logo">
	</div>
	<hr id="hr1">
	<div id="kalendar">
	<p id="mesec"><b>April</b>
<br>
<i>02-04.04.2020.</i> – Međunarodni sajam automobila, Fiat i Ford <b>[ODLOŽENO]</b>
<br>
<i>03-05.04.2020.</i> – Sajam motocikala
<br>
<i>04-05.04.2020.</i> – Međunarodni kongres i sajam Porsche i Lamborghini
<br>
<i>09.04.2020.</i> – Sajam srednjih stručnih i umetničkih automobila
<br>
<i>14-15.04.2020.</i> – Sajam za treće doba Old Timeri(55+)
<br>
<i>22-25.04.2020.</i> – Međunarodni sajam automobila
<br><br><b>Maj</b>
<br>
<i>02-03.05.2020.</i> – Knights & Legends
<br>
<i>19-22.05.2020.</i> – Međunarodni sajam tehnike i tehničkih dostignuća u auto industriji
<br><br><b>Jun</b>
<br>
<i>02-03.06.2020.</i> – Belgrade Future Cars
<br>
<i>10-13.06.2020.</i> – Međunarodni sajam automobila Audi i BMW</p>
	<h1 id="naslov">Kalendar: 2020/21  MAJ</h1>
	<table  id="tabela" cellspacing="0" border="1">
                    <tr>
                      
                        <th>Pon</th>
                        <th>Uto</th>
                        <th>Sre</th>
                        <th>Cet</th>
                        <th>Pet</th>
                        <th>Sub</th>
                        <th>Ned</th>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>1</td>
                        <td style="background-color:#595959">2</td>
                        <td style="background-color:#595959">3</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>5</td>
                        <td>6</td>
                        <td>7</td>
                        <td>8</td>
                        <td>9</td>
                        <td>10</td>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td>12</td>
                        <td>13</td>
                        <td>14</td>
                        <td>15</td>
                        <td>16</td>
                        <td>17</td>
                    </tr>
                    <tr>
                        <td>18</td>
                        <td style="background-color:#595959">19</td>
                        <td style="background-color:#595959">20</td>
                        <td style="background-color:#595959">21</td>
                        <td style="background-color:#595959">22</td>
                        <td>23</td>
                        <td>24</td>
                    </tr>
                    <tr>
                        <td>25</td>
                        <td>26</td>
                        <td>27</td>
                        <td>28</td>
                        <td>29</td>
                        <td>30</td>
                        <td>31</td>
                    </tr>
                    </table>
					</div>
					<div id="footer4">
	<br>
	<br>
	 <br>
	 E-mail:salon-automobila14@gmail.com<br>
	Adresa:Bulevar Kralja Aleksandra 219<br>	
	Kontakt: +381 65/576-7694 <br>	
	Belmin Cuturic &#169 2019/20
	
	
	</div>
	</div>
</body>
</html>
