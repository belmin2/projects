<%-- 
    Document   : o_nama
    Created on : Apr 21, 2020, 1:35:52 PM
    Author     : UltraBook
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

<title>Beogradski Sajam</title>
<link href="style1.css" type="text/css" rel="stylesheet">

</head>
<body>

<div id="novi">
<div id='meni'>
		<ul>
		<li><a href="index.jsp">Pocetna</a></li>
		<li><a href='o_nama.jsp'>O Nama</a></li>
		<li><a href='kontakt.jsp'>Kontakt</a></li>
		<li><a href='kalendar.jsp'>Kalendar</a></li>
		<li><a href='kupi.jsp'>Naruci vozilo</a></li>
		</ul>
	</div>
	<div id="logo">
	</div>
	<br>
	<br>
	<br>
	<div id="slika"> <img src="slika.png" width="100%"></div>
	<div id="text">
	<div id="text1">
	<h1>O nama</h1>

Po svim kriterijumima – izgledu, atmosferi, atraktivnim dešavanjima i premijerama, broju izlagača i posetilaca… Sajam automobila svake godine predstavlja pravi spektakl. Sjaj i glamur ove manifestacije privlače potencijalne kupce, ali i one koji samo prate dešavanja i novitete u automobilskoj industriji. Naravno, ne sme se zaboraviti poslovni segment, kao najznačajniji, jer se na ovoj izložbeno-prodajnoj priredbi okupljaju uvoznici i distributeri svih svetskih automobilskih brendova, proizvođači auto delova i opreme, predstavnici pratećih industrija, strukovnih organizacija, udruženja, klubova…

Nebitno je da li ste zainteresovani za putničko ili teretno vozilo, pravilo glasi – kupujte ga na ovoj manifestaciji. Od malih gradskih automobila do luksuznih limuzina, preko terenaca, kombija i dostavnih vozila do najvećih kamiona, ponuda je najbolja na Sajmu. Treba iskoristiti promotivne i akcijske cene i uslove kupovine.

Sajam je mesto na kome se noviteti afirmišu kroz aktivno učešće posetilaca, test vožnje, nagradne igre, zabavna i takmičarska dešavanja.
</div></div><div id="text2"><div id="text3"><h1>KAKO JE POČELO</h1>

Međunarodni salon automobila suštinski je začet na Prvom beogradskom sajmu 1937. godine, na kojem je segment automobilske industrije imao zapaženo mesto. Već (1-15) marta 1938. organizovan je, kao samostalna manifestacija, Prvi međunarodni salon automobila, na 10.000 km2 izložbenog prostora, a 107 izlagača prikazalo je 375 modela putničkih i teretnih automobila, autobusa, motocikala, bicikala, traktora, pa čak i jedan motorni bager. Već sledeće godine Salon je bio proširen Izložbom puteva i turizma…
</div></div><div id="text4"><div id="text5"><h1>MISIJA</h1>

Cilj je organizatora, Beogradskog sajma, da u saradnji sa svojim strateškim partnerima – Udruženjem proizvođača drumskih vozila Srbije, Srpskom asocijacijom uvoznika vozila i delova i kompanijom DDOR Novi Sad, generalnim sponzorom ove manifestacije – predstavi najnovija dostignuća kompletne auto-moto industrije i najraznovrsniju moguću paletu performansi, dizajnerskih i tehničko-tehnoloških inovativnih rešenja u svim sektorima ove industrije. Ipak, programski akcenat je na novim konceptima u oblasti alternativnih te hibridnih i elektro pogona, ali i bezbednosnih rešenja novih modela, koji za potencijalne kupce postaju jedan od ključnih faktora prilikom izbora novog vozila.
</div></div><div id="text6"><div id="text7"><h1>DDOR BG CAR SHOW 06</h1>

DDOR BG CAR SHOW 06 održan je 2018. godine na više od 29.000 m2 izložbenog prostora, pred skoro 130.000 posetilaca. Učestvovalo je 306 izlagača iz 26 zemalja.
</div></div><div id="text8"><div id="text9"><h1>54. MEĐUNARODNI SALON AUTOMOBILA</h1>

54. Međunarodni salon automobila, održan 2019. godine, posetilo je više od 140 hiljada posetilaca.

U sedam sajamskih hala, na preko 41.000 m2 izložbenog prostora, predstavilo se 410 izlagača iz Austrije, Belgije, Bosne i Hercegovine, Bugarske, Češke, Danske, Francuske, Holandije, Hrvatske, Italije, Japana, Južne Koreje, Kanade, Kine, Litvanije, Mađarske, Nemačke, Poljske, Rumunije, Rusije, SAD-a, Turske, Švajcarske, Švedske, Španije, Velike Britanije, i Srbije.
	</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
<iframe width="1000" height="500" src="https://www.youtube.com/embed/h_6NQMPFF8Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>	<br>
    <div id="footer2">	
	<br>
	 E-mail:salon-automobila14@gmail.com<br>
	Adresa:Bulevar Kralja Aleksandra 219<br>	
	Kontakt: +381 65/576-7694 <br>	
	Belmin Cuturic &#169 2019/20
	
	
	</div>
	</div>

</body>
</html>
