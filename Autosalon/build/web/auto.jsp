<%-- 
    Document   : auto
    Created on : Apr 21, 2020, 12:58:27 AM
    Author     : UltraBook
--%>

<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="baza.Database" %>
<%@page import="automobil.Automobil" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Beogradski Sajam</title>
        <link href="style1.css" type="text/css" rel="stylesheet">
        <script>
            function Kupio()
            {
                alert('Uspesna kupovina');
            }
        </script>
    </head>
    <body>
        <div id="novi">
        <div id='meni'>
                    <ul>
                    <li><a href="index.jsp">Pocetna</a></li>
		<li><a href='o_nama.jsp'>O Nama</a></li>
		<li><a href='kontakt.jsp'>Kontakt</a></li>
		<li><a href='kalendar.jsp'>Kalendar</a></li>
		<li><a href='kupi.jsp'>Naruci vozilo</a></li>
                    </ul>
        </div>
        <div id="logo">

        </div>
        <% String broj = request.getParameter("Carlist");
        String Audi = "1";
        String BMW = "2";
        String Porshe= "3";
        String Mercedes="4";
        String Ostalo="5";
        Database db=new Database();
        if(broj.equals(Audi))
        {
            Automobil[] audiji;
            audiji = db.SviAudi();
            int i,ind=0;
            for(i=0;i<audiji.length;i++)
            {
                if(i==0 || i%3 == 0)
                {
                    out.println("<div id='nesto'>");
                    out.println("<div class='gt'>");
                }
                out.println("<div class='title'>");
                out.println("<img src='images/"+audiji[i].Slika+".jpg' width='100%' height='200px'>");    
                out.println("<p>");
                    out.println("Marka: "+audiji[i].Marka+" <br>");
                    out.println("Model: "+audiji[i].Naziv+"  <br>");
                    out.println("Boja: "+audiji[i].Boja+" <br>");
                    out.println("Cena:  "+audiji[i].Cena+"  &euro; <br>");					
		    out.println("Kubikaža: "+audiji[i].Kubikaza+" (cm<sup>3</sup>)<br>");
                    out.println("Snaga: "+audiji[i].KW+"/"+audiji[i].KS+" (KW/KS)<br>");
                    out.println(" Menjač: "+audiji[i].Menjac+"<br>");
                    out.println("Klima: "+audiji[i].Klima+"<br>");
                    out.println("Broj vrata: "+audiji[i].Vrata+" vrata<br>");					
                out.println("</p>");
                out.println("<br>");
                out.println("<input type='button' value='Kupi' onclick='Kupio()' id='dugme'>");         
                out.println("</div>");
                if(i%3 == 2)
                {
                    ind=1;
                }
                if(ind == 1 || i == audiji.length-1)
                {
                    ind=0;
                    out.println("</div>");
                    out.println("</div>");
                }
         }
         out.println("<div id='traze'>");
         out.println("<form action='auto.jsp' method='post'>");
         out.println(" <pre>");
                
        out.println("<select name='Carlist' id='trazi1' style='width:400px;margin-right: 60px;height:50px;font: bold 16px Tahoma,sans-serif;text-align: center'>");
               out.println("  <option value='1'>Audi</option>");
               out.println("   <option value='2'>BMW</option>");
               out.println("  <option value='3'>Porshe</option>");
               out.println("  <option value='4'>Mercedes</option>");
               out.println("  <option value='5'>Ostalo...</option>");
        out.println(" </select>");
        
        out.println(" <br>");
        out.println(" <input type='submit' value='Trazi' id='dugme2'>");
        out.println("</form>");
 
        out.println(" </div>");
        
        }
        else if(broj.equals(BMW))
        {
            Automobil[] bmw;
             bmw = db.SviBmw();
            int i,ind=0;
            for(i=0;i<bmw.length;i++)
            {
                if(i==0 || i%3 == 0)
                {
                    out.println("<div id='nesto'>");
                    out.println("<div class='gt'>");
                }
                out.println("<div class='title'>");
                out.println("<img src='images/"+bmw[i].Slika+".jpg' width='100%' height='200px'>");    
                out.println("<p>");
                    out.println("Marka: "+bmw[i].Marka+" <br>");
                    out.println("Model: "+bmw[i].Naziv+"  <br>");
                    out.println("Boja: "+bmw[i].Boja+" <br>");
                    out.println("Cena:  "+bmw[i].Cena+"  &euro; <br>");					
		    out.println("Kubikaža: "+bmw[i].Kubikaza+" (cm<sup>3</sup>)<br>");
                    out.println("Snaga: "+bmw[i].KW+"/"+bmw[i].KS+" (KW/KS)<br>");
                    out.println(" Menjač: "+bmw[i].Menjac+"<br>");
                    out.println("Klima: "+bmw[i].Klima+"<br>");
                    out.println("Broj vrata: "+bmw[i].Vrata+" vrata<br>");					
                out.println("</p>");
                out.println("<br>");
                out.println("<input type='button' value='Kupi' onclick='Kupio()' id='dugme'>");         
                out.println("</div>");
                if(i%3 == 2)
                {
                    ind=1;
                }
                if(ind == 1 || i == bmw.length-1)
                {
                    ind=0;
                    out.println("</div>");
                    out.println("</div>");
                }
         }
         out.println("<div id='traze'>");
         out.println("<form action='auto.jsp' method='post'>");
         out.println(" <pre>");
                
        out.println("<select name='Carlist' id='trazi1' style='width:400px;margin-right: 60px;height:50px;font: bold 16px Tahoma,sans-serif;text-align: center'>");
               out.println("  <option value='1'>Audi</option>");
               out.println("   <option value='2'>BMW</option>");
               out.println("  <option value='3'>Porshe</option>");
               out.println("  <option value='4'>Mercedes</option>");
               out.println("  <option value='5'>Ostalo...</option>");
        out.println(" </select>");
        
        out.println(" <br>");
        out.println(" <input type='submit' value='Trazi' id='dugme2'>");
        out.println("</form>");
 
        out.println(" </div>");
        
        }
        else if(broj.equals(Porshe))
        {
            Automobil[] porshe;
            porshe = db.SviPorseovi();
            int i,ind=0;
            for(i=0;i<porshe.length;i++)
            {
                if(i==0 || i%3 == 0)
                {
                    out.println("<div id='nesto'>");
                    out.println("<div class='gt'>");
                }
                out.println("<div class='title'>");
                out.println("<img src='images/"+porshe[i].Slika+".jpg' width='100%' height='200px'>");    
                out.println("<p>");
                    out.println("Marka: "+porshe[i].Marka+" <br>");
                    out.println("Model: "+porshe[i].Naziv+"  <br>");
                    out.println("Boja: "+porshe[i].Boja+" <br>");
                    out.println("Cena:  "+porshe[i].Cena+"  &euro; <br>");					
		    out.println("Kubikaža: "+porshe[i].Kubikaza+" (cm<sup>3</sup>)<br>");
                    out.println("Snaga: "+porshe[i].KW+"/"+porshe[i].KS+" (KW/KS)<br>");
                    out.println(" Menjač: "+porshe[i].Menjac+"<br>");
                    out.println("Klima: "+porshe[i].Klima+"<br>");
                    out.println("Broj vrata: "+porshe[i].Vrata+" vrata<br>");					
                out.println("</p>");
                out.println("<br>");
                out.println("<input type='button' value='Kupi' onclick='Kupio()' id='dugme'>");         
                out.println("</div>");
                if(i%3 == 2)
                {
                    ind=1;
                }
                if(ind == 1 || i == porshe.length-1)
                {
                    ind=0;
                    out.println("</div>");
                    out.println("</div>");
                }
         }
         out.println("<div id='traze'>");
         out.println("<form action='auto.jsp' method='post'>");
         out.println(" <pre>");
                
        out.println("<select name='Carlist' id='trazi1' style='width:400px;margin-right: 60px;height:50px;font: bold 16px Tahoma,sans-serif;text-align: center'>");
               out.println("  <option value='1'>Audi</option>");
               out.println("   <option value='2'>BMW</option>");
               out.println("  <option value='3'>Porshe</option>");
               out.println("  <option value='4'>Mercedes</option>");
               out.println("  <option value='5'>Ostalo...</option>");
        out.println(" </select>");
        
        out.println(" <br>");
        out.println(" <input type='submit' value='Trazi' id='dugme2'>");
        out.println("</form>");
 
        out.println(" </div>");
        }
        else if(broj.equals(Mercedes))
        {
           Automobil[] mercedes;
         mercedes = db.SviMercedesi();
            int i,ind=0;
            for(i=0;i<mercedes.length;i++)
            {
                if(i==0 || i%3 == 0)
                {
                    out.println("<div id='nesto'>");
                    out.println("<div class='gt'>");
                }
                out.println("<div class='title'>");
                out.println("<img src='images/"+mercedes[i].Slika+".jpg' width='100%' height='200px'>");    
                out.println("<p>");
                    out.println("Marka: "+mercedes[i].Marka+" <br>");
                    out.println("Model: "+mercedes[i].Naziv+"  <br>");
                    out.println("Boja: "+mercedes[i].Boja+" <br>");
                    out.println("Cena:  "+mercedes[i].Cena+"  &euro; <br>");					
		    out.println("Kubikaža: "+mercedes[i].Kubikaza+" (cm<sup>3</sup>)<br>");
                    out.println("Snaga: "+mercedes[i].KW+"/"+mercedes[i].KS+" (KW/KS)<br>");
                    out.println(" Menjač: "+mercedes[i].Menjac+"<br>");
                    out.println("Klima: "+mercedes[i].Klima+"<br>");
                    out.println("Broj vrata: "+mercedes[i].Vrata+" vrata<br>");					
                out.println("</p>");
                out.println("<br>");
                out.println("<input type='button' value='Kupi' onclick='Kupio()' id='dugme'>");         
                out.println("</div>");
                if(i%3 == 2)
                {
                    ind=1;
                }
                if(ind == 1 || i == mercedes.length-1)
                {
                    ind=0;
                    out.println("</div>");
                    out.println("</div>");
                }
         }   
            
            
         out.println("<div id='traze'>");
         out.println("<form action='auto.jsp' method='post'>");
         out.println(" <pre>");
                
        out.println("<select name='Carlist' id='trazi1' style='width:400px;margin-right: 60px;height:50px;font: bold 16px Tahoma,sans-serif;text-align: center'>");
               out.println("  <option value='1'>Audi</option>");
               out.println("   <option value='2'>BMW</option>");
               out.println("  <option value='3'>Porshe</option>");
               out.println("  <option value='4'>Mercedes</option>");
               out.println("  <option value='5'>Ostalo...</option>");
        out.println(" </select>");
        
        out.println(" <br>");
        out.println(" <input type='submit' value='Trazi'  id='dugme2'>");
        out.println("</form>");
 
        out.println(" </div>");   
        }
        else if(broj.equals(Ostalo))
        {
          Automobil[] ostali;
           ostali = db.SviOstali();
            int i,ind=0;
            for(i=0;i<ostali.length;i++)
            {
                if(i==0 || i%3 == 0)
                {
                    out.println("<div id='nesto'>");
                    out.println("<div class='gt'>");
                }
                out.println("<div class='title'>");
                out.println("<img src='images/"+ostali[i].Slika+".jpg' width='100%' height='200px'>");    
                out.println("<p>");
                    out.println("Marka: "+ostali[i].Marka+" <br>");
                    out.println("Model: "+ostali[i].Naziv+"  <br>");
                    out.println("Boja: "+ostali[i].Boja+" <br>");
                    out.println("Cena:  "+ostali[i].Cena+"  &euro; <br>");					
		    out.println("Kubikaža: "+ostali[i].Kubikaza+" (cm<sup>3</sup>)<br>");
                    out.println("Snaga: "+ostali[i].KW+"/"+ostali[i].KS+" (KW/KS)<br>");
                    out.println(" Menjač: "+ostali[i].Menjac+"<br>");
                    out.println("Klima: "+ostali[i].Klima+"<br>");
                    out.println("Broj vrata: "+ostali[i].Vrata+" vrata<br>");					
                out.println("</p>");
                out.println("<br>");
                out.println("<input type='button' value='Kupi' onclick='Kupio()' id='dugme'>");         
                out.println("</div>");
                if(i%3 == 2)
                {
                    ind=1;
                }
                if(ind == 1 || i == ostali.length-1)
                {
                    ind=0;
                    out.println("</div>");
                    out.println("</div>");
                }
         }
         out.println("<div id='traze'>");
         out.println("<form action='auto.jsp' method='post'>");
         out.println(" <pre>");
                
        out.println("<select name='Carlist' id='trazi1' style='width:400px;margin-right: 60px;height:50px;font: bold 16px Tahoma,sans-serif;text-align: center'>");
               out.println("  <option value='1'>Audi</option>");
               out.println("   <option value='2'>BMW</option>");
               out.println("  <option value='3'>Porshe</option>");
               out.println("  <option value='4'>Mercedes</option>");
               out.println("  <option value='5'>Ostalo...</option>");
        out.println(" </select>");
        
        out.println(" <br>");
        out.println(" <input type='submit' value='Trazi' id='dugme2'>");
        out.println("</form>");
 
        out.println(" </div>");   
        }
        %>
        <div id="footer4">
	<br>
	<br>
        <br>
	 E-mail:salon-automobila14@gmail.com<br>
	Adresa:Bulevar Kralja Aleksandra 219<br>	
	Kontakt: +381 65/576-7694 <br>	
	Belmin Cuturic &#169 2019/20
	
	
	</div>
        </div>
    </body>
</html>
