package baza;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import automobil.Automobil;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author UltraBook
 */
    public class Database {
        public int Velicina(String marka)
        {
            int i = 0;
            try{
                Class.forName("com.mysql.jdbc.Driver");  
                Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/auto","root","");  
                Statement stmt=con.createStatement();  
                ResultSet rs=stmt.executeQuery("select * from automobili where marka like '"+marka+"'");
                while(rs.next())  {
                    i=i+1;
                }
            }
            catch(Exception e){
                System.out.println(e);
            }  
            return i;
        }
        public int Velicina2(String marka,String marka2,String marka3,String marka4)
        {
            int i = 0;
            try{
                Class.forName("com.mysql.jdbc.Driver");  
                Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/auto","root","");  
                Statement stmt=con.createStatement();  
                ResultSet rs=stmt.executeQuery("select * from automobili where marka not like '"+marka+"' and marka not like '"+marka2+"' and marka not like '"+marka3+"' and marka not like '"+marka4+"'");
                while(rs.next())  {
                    i=i+1;
                }
            }
            catch(Exception e){
                System.out.println(e);
            }  
            return i;
        }
        public Automobil[] SviAudi()
        {
            Automobil[] audiji = new Automobil[2];
            int size;
            
            try{
                Class.forName("com.mysql.jdbc.Driver");  
                Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/auto","root","");  
                Statement stmt=con.createStatement();  
                ResultSet rs=stmt.executeQuery("select * from automobili where marka like 'Audi'");
                int i = 0;
                size = 0;
                String marka="Audi";
                size = Velicina(marka);
                audiji = new Automobil[size];
                while(rs.next())  {
                    audiji[i] = new Automobil(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),
                            rs.getInt(5),rs.getInt(6),rs.getInt(7),rs.getInt(8),rs.getString(9),rs.getString(10),
                            rs.getString(11),rs.getString(12));
                    i=i+1;
                }
            }
            catch(Exception e){
                System.out.println(e);
            }  
            return audiji;
            
        }
        public Automobil[] SviBmw()
        {
            Automobil[] bmw = new Automobil[2];
            int size;
            
            try{
                Class.forName("com.mysql.jdbc.Driver");  
                Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/auto","root","");  
                Statement stmt=con.createStatement();  
                ResultSet rs=stmt.executeQuery("select * from automobili where marka like 'BMW'");
                int i = 0;
                size = 0;
                String mar="BMW";
                size = Velicina(mar);
                bmw = new Automobil[size];
                while(rs.next())  {
                    bmw[i] = new Automobil(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),
                            rs.getInt(5),rs.getInt(6),rs.getInt(7),rs.getInt(8),rs.getString(9),rs.getString(10),
                            rs.getString(11),rs.getString(12));
                    i=i+1;
                }
            }
            catch(Exception e){
                System.out.println(e);
            }  
            return bmw;
            
        }
        public Automobil[] SviMercedesi()
        {
            Automobil[] mercedes = new Automobil[2];
            int size;
            
            try{
                Class.forName("com.mysql.jdbc.Driver");  
                Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/auto","root","");  
                Statement stmt=con.createStatement();  
                ResultSet rs=stmt.executeQuery("select * from automobili where marka like 'Mercedes'");
                int i = 0;
                size = 0;
                String mar="Mercedes";
                size = Velicina(mar);
                mercedes = new Automobil[size];
                while(rs.next())  {
                    mercedes[i] = new Automobil(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),
                            rs.getInt(5),rs.getInt(6),rs.getInt(7),rs.getInt(8),rs.getString(9),rs.getString(10),
                            rs.getString(11),rs.getString(12));
                    i=i+1;
                }
            }
            catch(Exception e){
                System.out.println(e);
            }  
            return mercedes;
            
        }
        public Automobil[] SviPorseovi()
        {
            Automobil[] porshe = new Automobil[2];
            int size;
            
            try{
                Class.forName("com.mysql.jdbc.Driver");  
                Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/auto","root","");  
                Statement stmt=con.createStatement();  
                ResultSet rs=stmt.executeQuery("select * from automobili where marka like 'Porsche'");
                int i = 0;
                size = 0;
                String mar="Porsche";
                size = Velicina(mar);
                porshe = new Automobil[size];
                while(rs.next())  {
                    porshe[i] = new Automobil(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),
                            rs.getInt(5),rs.getInt(6),rs.getInt(7),rs.getInt(8),rs.getString(9),rs.getString(10),
                            rs.getString(11),rs.getString(12));
                    i=i+1;
                }
            }
            catch(Exception e){
                System.out.println(e);
            }  
            return porshe;
            
        }
        public Automobil[] SviOstali()
        {
            Automobil[] ostali = new Automobil[2];
            int size;
            
            try{
                Class.forName("com.mysql.jdbc.Driver");  
                Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/auto","root","");  
                Statement stmt=con.createStatement();  
                ResultSet rs=stmt.executeQuery("select * from automobili where marka not like 'Mercedes' and marka not like 'Porsche' and marka not like 'BMW' and marka not like 'Audi'");
                int i = 0;
                size = 0;
                String mar="Mercedes";
                String mar1="Porsche";
                String mar2="BMW";
                String mar3="Audi";
                size = Velicina2(mar,mar1,mar2,mar3);
                ostali = new Automobil[size];
                while(rs.next())  {
                    ostali[i] = new Automobil(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),
                            rs.getInt(5),rs.getInt(6),rs.getInt(7),rs.getInt(8),rs.getString(9),rs.getString(10),
                            rs.getString(11),rs.getString(12));
                    i=i+1;
                }
            }
            catch(Exception e){
                System.out.println(e);
            }  
            return ostali;
            
        }
}