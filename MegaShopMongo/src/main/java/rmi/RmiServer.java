package rmi;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

import service.AdminService;
import service.AuthService;
import service.ContactService;
import service.ProductService;

public class RmiServer {

	public static void main(String[] args) {
		try {
			AuthService authService = new AuthService();
			ContactService contactService = new ContactService();
			ProductService productService = new ProductService();
			AdminService adminService = new AdminService();
			LocateRegistry.createRegistry(1103);
			Naming.rebind("rmi://localhost:1103/authServer", authService);
			System.out.println("Auth server started on :1103/authServer");
			Naming.rebind("rmi://localhost:1103/contactServer", contactService);
			System.out.println("Contact server started on :1103/contactServer");
			Naming.rebind("rmi://localhost:1103/productServer", productService);
			System.out.println("Prodcut server started on :1103/productServer");
			Naming.rebind("rmi://localhost:1103/adminServer", adminService);
			System.out.println("Prodcut server started on :1103/adminServer");
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}

}
