package servlet;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import model.Category;
import model.IAdmin;
import model.User;

@WebServlet("/CategoryServlet")
public class CategoryServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	IAdmin adminService;
	public CategoryServlet() {
		super();
		try {
			this.adminService = (IAdmin) Naming.lookup("//localhost:1103/adminServer");
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Category> categories = this.adminService.getAllCategories();
		JSONArray array = new JSONArray();
		
		for(Category c : categories)
		{
			JSONObject category = new JSONObject();
			category.put("id", c.getId());
			category.put("naziv", c.getNaziv());
			array.put(category);
		}
		response.setContentType("application/json");
        response.getWriter().write(array.toString());
	}
}
