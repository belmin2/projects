package servlet;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONObject;

import model.Category;
import model.IAdmin;
import model.Product;
import model.User;

@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	IAdmin adminService;
	public AdminServlet() {
		super();
		try {
			this.adminService = (IAdmin) Naming.lookup("//localhost:1103/adminServer");
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Category> categories = this.adminService.getAllCategories();
		request.setAttribute("categories", categories);
		request.setAttribute("page", "users");
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/pages/admin.jsp");
		rd.forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<User> users = this.adminService.getAllUsers();
		JSONArray array = new JSONArray();
		
		for(User u : users)
		{
			JSONObject user = new JSONObject();
			user.put("id", u.getId());
			user.put("korisnicko_ime", u.getKorisnicko_ime());
			user.put("email", u.getEmail());
			user.put("datum_registracije", u.getDatum_registracije());
			user.put("status", u.getStatus());
			array.put(user);
		}
		response.setContentType("application/json");
        response.getWriter().write(array.toString());
	}
}
