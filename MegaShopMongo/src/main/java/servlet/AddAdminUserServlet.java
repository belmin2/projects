package servlet;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Category;
import model.IAdmin;

@WebServlet("/AddAdminUserServlet")
public class AddAdminUserServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	IAdmin adminService;
	public AddAdminUserServlet() {
		super();
		try {
			this.adminService = (IAdmin) Naming.lookup("//localhost:1103/adminServer");
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("idkorisnikadmin"));
		this.adminService.addAdminStatus(id);
		List<Category> categories = this.adminService.getAllCategories();
		request.setAttribute("categories", categories);
		request.setAttribute("page", "users");
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/pages/admin.jsp");
		rd.forward(request, response);
	}
}
