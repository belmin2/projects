package servlet;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.IContact;
import model.Location;
import model.User;

import org.json.JSONArray;
import org.json.JSONObject;

@WebServlet("/ContactServlet")
public class ContactServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	IContact contactService;
	public ContactServlet() {
		super();
		// TODO Auto-generated constructor stub
		try {
			this.contactService = (IContact) Naming.lookup("//localhost:1103/contactServer");
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		List<Location> locations = this.contactService.getAllLocations();
		List<Location> locationsm = this.contactService.getLocations("maloprodaja");
		List<Location> locationsv = this.contactService.getLocations("veleprodaja");
		request.setAttribute("contacts", locations);
		request.setAttribute("contactsm", locationsm);
		request.setAttribute("contactsv", locationsv);
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/pages/contact.jsp");
		rd.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Location> locations = this.contactService.getAllLocations();
		JSONArray array = new JSONArray();
        
		for(Location l : locations)
		{
			JSONObject location = new JSONObject();
			location.put("id", l.getId());
			location.put("drzava", l.getDrzava());
			location.put("telefon", l.getTelefon());
			location.put("adresa", l.getAdresa());
			location.put("tip", l.getTip());
			location.put("grad",l.getGrad());
			array.put(location);
		}
		response.setContentType("application/json");
        response.getWriter().write(array.toString());
	}
}
