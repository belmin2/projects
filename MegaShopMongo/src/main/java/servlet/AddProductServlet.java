package servlet;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import model.Category;
import model.IAdmin;
import model.Product;


@WebServlet("/AddProductServlet")
@MultipartConfig
public class AddProductServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	IAdmin adminService;
	public AddProductServlet() {
		super();
		try {
			this.adminService = (IAdmin) Naming.lookup("//localhost:1103/adminServer");
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("idproduct");
		String naziv = request.getParameter("novinaziv");
		String boja = request.getParameter("novaboja");
		String kolicina = request.getParameter("novakolicina");
		String kategorija = request.getParameter("newprodcutselect");
		String cena = request.getParameter("novacena");
		String opis = request.getParameter("noviopis");
		String slika = request.getParameter("novaslika");
		Product product = new Product();
		product.setNaziv(naziv);
		product.setBoja(boja);
		product.setCena(Double.parseDouble(cena));
		product.setKolicina(Integer.parseInt(kolicina));
		product.setId_kategorije(Integer.parseInt(kategorija));
		product.setSlika(slika);
		product.setOpis(opis);
		if(id.equals(""))
		{
			if(slika != "")
			{
				Part part = request.getPart("file-name");
				String fileName = part.getSubmittedFileName();
//				String path = getServletContext().C:\Users\Fujitsu\workspace\Belmin-Cuturic-PDS-projekt\WebContent\items
				String path = "C:/Users/Fujitsu/eclipse-workspace/MegaShop/WebContent/items/" + fileName;
				try {
					FileOutputStream fos = new FileOutputStream(path);
					InputStream is = part.getInputStream();
					byte[] byt = new byte[is.available()];
					is.read(byt);
					fos.write(byt);
					fos.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			this.adminService.addProduct(product);
		}
		else
		{
			String slikaNow = this.adminService.getProductPicture(Integer.parseInt(id));
			if(slika != "" && !(slika.equals(slikaNow)))
			{
				Part part = request.getPart("file-name");
				String fileName = part.getSubmittedFileName();
//				String path = getServletContext().C:\Users\Fujitsu\workspace\Belmin-Cuturic-PDS-projekt\WebContent\items
				String path = "C:/Users/Fujitsu/eclipse-workspace/MegaShop/WebContent/items/" + fileName;
				try {
					FileOutputStream fos = new FileOutputStream(path);
					InputStream is = part.getInputStream();
					byte[] byt = new byte[is.available()];
					is.read(byt);
					fos.write(byt);
					fos.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			product.setId(Integer.parseInt(id));
			this.adminService.changeProduct(product);
		}
		List<Category> categories = this.adminService.getAllCategories();
		request.setAttribute("categories", categories);
		request.setAttribute("page", "product");
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/pages/admin.jsp");
		rd.forward(request, response);
	}
	
}
