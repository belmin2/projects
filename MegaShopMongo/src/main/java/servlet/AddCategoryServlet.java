package servlet;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Category;
import model.IAdmin;

@WebServlet("/AddCategoryServlet")
public class AddCategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	IAdmin adminService;
	public AddCategoryServlet() {
		super();
		try {
			this.adminService = (IAdmin) Naming.lookup("//localhost:1103/adminServer");
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("idcategory");
		String naziv = request.getParameter("novinazivkategorije");
		Category category = new Category();
		category.setNaziv(naziv);
		if(id.equals(""))
		{
			this.adminService.addCategory(category);
		}
		else
		{
			category.setId(Integer.parseInt(id));
			this.adminService.changeCategory(category);
		}
		List<Category> categories = this.adminService.getAllCategories();
		request.setAttribute("categories", categories);
		request.setAttribute("page", "category");
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/pages/admin.jsp");
		rd.forward(request, response);
	}
}
