package servlet;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import model.Category;
import model.IAdmin;
import model.Location;
import model.Product;

@WebServlet("/AddLocationServlet")
@MultipartConfig
public class AddLocationServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	IAdmin adminService;
	public AddLocationServlet() {
		super();
		try {
			this.adminService = (IAdmin) Naming.lookup("//localhost:1103/adminServer");
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("idlocation");
		String drzava = request.getParameter("novadrzava");
		String tip = request.getParameter("newlocationtypesselect");
		String grad = request.getParameter("novigrad");
		String adresa = request.getParameter("novaadresa");
		String telefon = request.getParameter("novitelefon");
		Location location = new Location();
		location.setDrzava(drzava);
		location.setTip(tip);
		location.setGrad(grad);
		location.setAdresa(adresa);
		location.setTelefon(telefon);
		if(id.equals(""))
		{
			this.adminService.addLocation(location);
		}
		else
		{
			location.setId(Integer.parseInt(id));
			this.adminService.changeLocation(location);
		}
		List<Category> categories = this.adminService.getAllCategories();
		request.setAttribute("categories", categories);
		request.setAttribute("page", "location");
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/pages/admin.jsp");
		rd.forward(request, response);
	}
}
