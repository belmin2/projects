package servlet;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.IAuth;
import model.User;


@WebServlet("/AuthServlet")
public class AuthServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	IAuth authService;
	public AuthServlet() {
		super();
		
		try {
			this.authService = (IAuth) Naming.lookup("//localhost:1103/authServer");
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String name = request.getParameter("ime");
		String email = request.getParameter("email");
		String password = request.getParameter("sifra");
		String address = request.getParameter("adresa");
		String city = request.getParameter("grad");
		String country = request.getParameter("drzava");
		String phone = request.getParameter("telefon");
		String years = request.getParameter("godine");
		String nationality = request.getParameter("nacionalnost");
		String polvrednost = request.getParameter("polvrednost");
		int pol,yrs=0;
		if(polvrednost.equals("muski"))
		{
			pol=0;
		}
		else
		{
			pol=1;
		}
		if(!(years.equals("")))
		{
			yrs = Integer.parseInt(years);
		}
		
		Date today = new java.util.Date();
		Date datum = new java.sql.Date(today.getTime());
		User u = new User(name,email,password,address,city,country,phone,nationality,pol,yrs,datum);
		if ( this.authService.register(u)) {
			HttpSession session = request.getSession();
			session.setAttribute("name", u.getKorisnicko_ime());
			session.setAttribute("UserId", u.getId());
			response.sendRedirect("index.jsp");
		} else
			response.sendRedirect("pages/register.jsp?errorRegister=true");
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String email = request.getParameter("email");
		String password = request.getParameter("sifra");

		User u = this.authService.login(email, password);

		if (u != null) {
			HttpSession session = request.getSession();
			session.setAttribute("UserId", u.getId());
			session.setAttribute("name", u.getKorisnicko_ime());
			if(u.getStatus() != 0)
			{
				session.setAttribute("Admin",1);
			}
			response.sendRedirect("pages/home.jsp");
		} else {
			response.sendRedirect("index.jsp?errorLogin=true");
		}
	}
}
