package servlet;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import db.BoughtProductsRepository;
import db.ProductRepository;
import model.BoughtProducts;
import model.Category;
import model.IProduct;
import model.Product;

@WebServlet("/BuyProductServlet")
public class BuyProductServlet extends HttpServlet{

	private static final long serialVersionUID = 2L;
	
	IProduct productService;
	
	public BuyProductServlet() {
		super();
		try {
			productService = (IProduct)Naming.lookup("//localhost:1103/productServer");
		} catch (MalformedURLException | RemoteException | NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String[] kolicine =request.getParameterValues("kolicina[]");
		String[] idproizvoda =request.getParameterValues("idproizvoda[]");
		String idKorisnika = request.getParameter("userid");
		
		double ukupna_cena=0;
		int i;
		long kupovina = this.productService.getIdOfBuy();
		int brojKupovine=1;
		List<Product> proizvodi = new ArrayList<Product>();
		Date today = new java.util.Date();
		Date datum = new java.sql.Date(today.getTime());
		Product p = null;
		Product pomocniP = null;
		int kolicina,indikator=0,trenutnaKolicina;
		if(idproizvoda.length == kolicine.length && idproizvoda.length>0)
		{
			BoughtProducts boughtProduct = new BoughtProducts();
			boughtProduct.setId_korisnika(Integer.parseInt(idKorisnika));
			boughtProduct.setDatum_kupovine(datum);
			for(i=0;i<idproizvoda.length;i++)
			{
				p = this.productService.getProductById(Integer.parseInt(idproizvoda[i]));
				kolicina= Integer.parseInt(kolicine[i]);
				trenutnaKolicina = p.getKolicina();
				if(trenutnaKolicina < kolicina)
				{
					indikator=1;
				}
			}
			if(indikator == 0)
			{
				for(i=0;i<idproizvoda.length;i++)
				{
					p = this.productService.getProductById(Integer.parseInt(idproizvoda[i]));
					kolicina= Integer.parseInt(kolicine[i]);
					trenutnaKolicina = p.getKolicina();
					p.setKolicina(kolicina);
					ukupna_cena+=p.getKolicina() * p.getCena();
					proizvodi.add(p);
					pomocniP = this.productService.getProductById(Integer.parseInt(idproizvoda[i]));
					pomocniP.setKolicina(trenutnaKolicina - kolicina);
					this.productService.changeProduct(pomocniP);
				}
				boughtProduct.setListaProizvoda(proizvodi);
				boughtProduct.setUkupna_cena(ukupna_cena);
				this.productService.saveBuy(boughtProduct);
			}
		}
		if(indikator == 1)
		{
			request.setAttribute("bought", "NotValid");
			response.sendRedirect("./ProductServlet?bought=NotValid");
		}
		else
		{
			request.setAttribute("bought", "Yes");
			response.sendRedirect("./ProductServlet?bought=Yes");
		}
	}
}
