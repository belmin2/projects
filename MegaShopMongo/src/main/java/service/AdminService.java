package service;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;

import db.CategoryRepository;
import db.LocationRepository;
import db.ProductRepository;
import db.UserRepository;
import model.Category;
import model.IAdmin;
import model.Location;
import model.Product;
import model.User;

public class AdminService extends UnicastRemoteObject implements IAdmin {


	private static final long serialVersionUID = 1L;
	LocationRepository locationRepo;
	ProductRepository productRepo;
	UserRepository userRepo;
	CategoryRepository categoryRepo;
	public AdminService() throws RemoteException {
		super();
		this.locationRepo = new LocationRepository();
		this.productRepo = new ProductRepository();
		this.userRepo = new UserRepository();
		this.categoryRepo = new CategoryRepository();
	}

	@Override
	public void deleteProduct(int id) throws RemoteException {
		this.productRepo.deleteProduct(id);
	}

	@Override
	public void changeProduct(Product p) throws RemoteException {
		this.productRepo.updateProduct(p);
	}

	@Override
	public void addProduct(Product p) throws RemoteException {
		productRepo.addProduct(p);
	}

	@Override
	public void deleteLocation(int id) throws RemoteException {
		this.locationRepo.deleteLocation(id);
		
	}

	@Override
	public void changeLocation(Location l) throws RemoteException {
		this.locationRepo.updateLocation(l);
		
	}

	@Override
	public void addLocation(Location l) throws RemoteException {
		this.locationRepo.addLocation(l);
		
	}

	@Override
	public void deleteUser(int id) throws RemoteException {
		this.userRepo.deleteUser(id);
		
	}

	@Override
	public void addAdminStatus(int id) throws RemoteException {
		this.userRepo.addAdminUser(id);
		
	}

	@Override
	public List<User> getAllUsers() throws RemoteException {
		List<User> users = new LinkedList<User>();
		users = this.userRepo.getAllUsers();
		return users;
	}

	@Override
	public List<Category> getAllCategories() throws RemoteException {
		List<Category> categories = new LinkedList<Category>();
		categories = this.categoryRepo.getAllCategories();
		return categories;
	}

	@Override
	public String getProductPicture(int id) throws RemoteException {
		return this.productRepo.getProductPicture(id);
	}

	@Override
	public void deleteCategory(int id) throws RemoteException {
		this.categoryRepo.deleteCategory(id);
		
	}

	@Override
	public void changeCategory(Category c) throws RemoteException {
		this.categoryRepo.updateCategory(c);
		
	}

	@Override
	public void addCategory(Category c) throws RemoteException {
		this.categoryRepo.addCategory(c);
		
	}

	@Override
	public boolean findProductWithCategoryId(int id) throws RemoteException {
		return this.productRepo.findProductByCategoryId(id);

	}

}
