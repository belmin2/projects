package service;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;

import db.BoughtProductsRepository;
import db.CategoryRepository;
import db.ProductRepository;
import model.BoughtProducts;
import model.Category;
import model.IProduct;
import model.Product;

public class ProductService extends UnicastRemoteObject implements IProduct{

	private static final long serialVersionUID = 1L;
	ProductRepository productRepo;
	CategoryRepository categoryRepo;
	BoughtProductsRepository boughtproductsRepo;
	public ProductService() throws RemoteException {
		super();
		productRepo = new ProductRepository();
		categoryRepo = new CategoryRepository();
		boughtproductsRepo = new BoughtProductsRepository();
	}

	@Override
	public List<Product> getAllProducts() throws RemoteException {
		List<Product> products = new LinkedList<Product>();
		products = this.productRepo.getAllProducts();
		return products;
	}

	@Override
	public List<Category> getAllCategories() throws RemoteException {
		List<Category> categories = new LinkedList<Category>();
		categories = this.categoryRepo.getAllCategories();
		return categories;
	}

	@Override
	public long getIdOfBuy() throws RemoteException {
		long idBuy = this.boughtproductsRepo.getBuyId();
		return idBuy;
	}

	@Override
	public boolean saveBuy(BoughtProducts boughtProduct) throws RemoteException {
		if(this.boughtproductsRepo.saveBuy(boughtProduct) != null)
		{
			return true;
		}
		return false;
	}
	@Override
	public Product getProductById(int id) throws RemoteException {
		Product p = this.productRepo.getProductById(id);
		return p;
	}

	@Override
	public void changeProduct(Product p) throws RemoteException {
		this.productRepo.updateProduct(p);
		
	}

}
