package service;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

import db.BoughtProductsRepository;
import db.ProductRepository;
import db.UserRepository;
import model.BoughtProducts;
import model.IAuth;
import model.Product;
import model.User;

public class AuthService extends UnicastRemoteObject implements IAuth {
	
	private static final long serialVersionUID = 1L;
	UserRepository userRepo;
	ProductRepository productRepo;
	BoughtProductsRepository boughtProductsRepo;
	
	public AuthService() throws RemoteException {
		super();
		this.userRepo = new UserRepository();
		this.productRepo = new ProductRepository();
		this.boughtProductsRepo = new BoughtProductsRepository();
	}

	/**
	 * 
	 */

	@Override
	public boolean register(User u) throws RemoteException {
		
		if(this.userRepo.findUserByEmail(u.getEmail()) != null) {
			return false;
		}
		User user = this.userRepo.addUser(u);
		if(user != null)
		{
			return true;
		}
		return false;
	}
	
	@Override
	public User login(String email, String password) throws RemoteException {
		User u = this.userRepo.findUserByEmail(email);
		if(u != null)
		{
			if(u.getSifra().equals(password))
			{
				return u;
			}
		}
		return null;
	}

	@Override
	public User getUserById(int id) throws RemoteException {
		return this.userRepo.findUserById(id);
	}

	@Override
	public List<Product> getAllProducts() throws RemoteException {
		return this.productRepo.getAllProducts();
	}

	@Override
	public List<BoughtProducts> getAllBoughtProductsById(int idUser) throws RemoteException {
		return this.boughtProductsRepo.getAllBoughtProductsByIdUser(idUser);
	}
	public int getMaxIdOfBuy() throws RemoteException {
		//int maxBuyId = this.boughtProductsRepo.getMaxBuyId();
		return 0;
	}
	
}
