package service;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;

import db.LocationRepository;
import model.IContact;
import model.Location;
import model.User;

public class ContactService extends UnicastRemoteObject implements IContact{


	private static final long serialVersionUID = 1L;
	LocationRepository locationRepo;
	
	public ContactService() throws RemoteException {
		super();
		this.locationRepo = new LocationRepository();
	}
	
	@Override
	public List<Location> getAllLocations() throws RemoteException {
		List<Location> listLocation = new LinkedList<Location>();
		listLocation = locationRepo.getAllLocations();
		return listLocation;
	}

	@Override
	public List<Location> getLocations(String type) throws RemoteException {
		List<Location> listLocation = new LinkedList<Location>();
		listLocation = locationRepo.getLocationByTip(type);
		return listLocation;
	
	}

}
