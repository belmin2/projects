package db;

import static com.mongodb.MongoClientSettings.getDefaultCodecRegistry;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import java.util.ArrayList;
import java.util.List;


import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

import model.Product;
import model.User;

public class ProductRepository {
	
	private static MongoClient getConnection() {
		int port_no = 27017;
		String url="localhost";
		
		MongoClient mongoClntObj = new MongoClient(url,port_no);
		return mongoClntObj;
	}
	CodecProvider pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build();
	CodecRegistry pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(), fromProviders(pojoCodecProvider));
	private String db_name = "MegaShop",db_collection_name = "products";
	private MongoDatabase db = getConnection().getDatabase(db_name);
    private MongoCollection productCollection = db.getCollection(db_collection_name, Product.class).withCodecRegistry(pojoCodecRegistry);
    
	public List<Product> getAllProducts()
	{
		List<Product> products = new ArrayList<>();
		productCollection.find().into(products);
		return products;
	}
	public Product getProductById(int id)
	{
		Product p = (Product) productCollection.find(Filters.eq("_id",id)).first();
		return p;
	}
	public boolean findProductByCategoryId(int id)
	{
		List<Product> products = new ArrayList<>();
		productCollection.find(Filters.eq("id_kategorije", id)).into(products);
		if(products.isEmpty())
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	public Product addProduct(Product product)
	{
		int productId;
		if(productCollection.find().sort(Sorts.descending("_id")).first() == null)
		{
			productId = 0;
		}
		else
		{
			productId = ((Product)productCollection.find().sort(Sorts.descending("_id")).first()).getId();
			productId++;
		}
		product.setId(productId);
        productCollection.insertOne(product);
		return product;
	}
	public Product updateProduct(Product product) {
		Product productToUpdate = this.getProductById(product.getId());
		productToUpdate.setBoja(product.getBoja());
		productToUpdate.setCena(product.getCena());
		productToUpdate.setId_kategorije(product.getId_kategorije());
		productToUpdate.setKolicina(product.getKolicina());
		productToUpdate.setNaziv(product.getNaziv());
		productToUpdate.setOpis(product.getOpis());
		productToUpdate.setSlika(product.getSlika());
		productCollection.replaceOne(Filters.eq("_id",productToUpdate.getId()), productToUpdate);
		return productToUpdate;
	}
	public Product deleteProduct(int id) {
		Product productToDelete = this.getProductById(id);
		productCollection.deleteOne(Filters.eq("_id",productToDelete.getId()));
		return productToDelete;
	}
	public String getProductPicture(int id)
	{
		return this.getProductById(id).getSlika();
	}
}
