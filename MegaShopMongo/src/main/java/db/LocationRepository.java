package db;

import static com.mongodb.MongoClientSettings.getDefaultCodecRegistry;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import java.util.ArrayList;
import java.util.List;

import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

import model.Location;
import model.User;

public class LocationRepository {
	
	private static MongoClient getConnection() {
		int port_no = 27017;
		String url="localhost";
		
		MongoClient mongoClntObj = new MongoClient(url,port_no);
		return mongoClntObj;
	}
	CodecProvider pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build();
	CodecRegistry pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(), fromProviders(pojoCodecProvider));
	private String db_name = "MegaShop",db_collection_name = "locations";
	private MongoDatabase db = getConnection().getDatabase(db_name);
    private MongoCollection locationCollection = db.getCollection(db_collection_name, Location.class).withCodecRegistry(pojoCodecRegistry);
    
    public Location addLocation(Location location)
	{
    	int locationId;
		if(locationCollection.find().sort(Sorts.descending("_id")).first() == null)
		{
			locationId = 0;
		}
		else
		{
			locationId = ((Location)locationCollection.find().sort(Sorts.descending("_id")).first()).getId();
			locationId++;
		}
		location.setId(locationId);
        locationCollection.insertOne(location);
		return location;
	}
	public Location getLocationById(int id)
	{
		return (Location) locationCollection.find(Filters.eq("_id",id)).first();
	}
	public List<Location> getLocationByTip(String tip)
	{
		List<Location> locationsByType = new ArrayList<>();
		locationCollection.find(Filters.eq("tip",tip)).into(locationsByType);
		return locationsByType;
	}
	public List<Location> getAllLocations()
	{
		List<Location> locations = new ArrayList<>();
		locationCollection.find().into(locations);
		return locations;
	}
	public Location updateLocation(Location location) {
		Location locationToUpdate = this.getLocationById(location.getId());
		locationToUpdate.setAdresa(location.getAdresa());
		locationToUpdate.setDrzava(location.getDrzava());
		locationToUpdate.setGrad(location.getGrad());
		locationToUpdate.setTelefon(location.getTelefon());
		locationToUpdate.setTip(location.getTip());
		locationCollection.replaceOne(Filters.eq("_id",locationToUpdate.getId()), locationToUpdate);
		return locationToUpdate;
	}
	public Location deleteLocation(int id) {
		Location locationToDelete = this.getLocationById(id);
		locationCollection.deleteOne(Filters.eq("_id",locationToDelete.getId()));
		return locationToDelete;
	}
}
