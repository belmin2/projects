package db;



import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

import static com.mongodb.MongoClientSettings.getDefaultCodecRegistry;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;


import model.User;

public class UserRepository {
	
	private static MongoClient getConnection() {
		int port_no = 27017;
		String url="localhost";
		
		MongoClient mongoClntObj = new MongoClient(url,port_no);
		return mongoClntObj;
	}
	CodecProvider pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build();
	CodecRegistry pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(), fromProviders(pojoCodecProvider));
	private String db_name = "MegaShop",db_collection_name = "users";
	private MongoDatabase db = getConnection().getDatabase(db_name);
    private MongoCollection userCollection = db.getCollection(db_collection_name, User.class).withCodecRegistry(pojoCodecRegistry);
    
    public User addUser(User u)
	{
    	int userId;
		if(userCollection.find().sort(Sorts.descending("_id")).first() == null)
		{
			userId = 0;
		}
		else
		{
			userId = ((User)userCollection.find().sort(Sorts.descending("_id")).first()).getId();
			userId++;
		}
		u.setId(userId);
        userCollection.insertOne(u);
		return u;
	}
	public User findUserById(int id)
	{
		User findUser = (User) userCollection.find(Filters.eq("_id",id)).first();
		return findUser;
	}
	public User findUserByEmail(String email) {
		User findUser = (User) userCollection.find(Filters.eq("email",email)).first();
		return findUser;
	}
	public List<User> getAllUsers()
	{
		List<User> users = new ArrayList<>();
		userCollection.find().into(users);
		return users;
	}
	public void addAdminUser(int id)
	{
		User newUser = this.findUserById(id);
		newUser.setStatus(1);
		this.updateUser(newUser);
	}
	public User updateUser(User user) {
		User userToUpdate = this.findUserById(user.getId());
		userToUpdate.setKorisnicko_ime(user.getKorisnicko_ime());
		userToUpdate.setAdresa(user.getAdresa());
		userToUpdate.setEmail(user.getEmail());
		userToUpdate.setSifra(user.getSifra());
		userToUpdate.setGrad(user.getGrad());
		userToUpdate.setDrzava(user.getDrzava());
		userToUpdate.setBroj_telefona(user.getBroj_telefona());
		userToUpdate.setDatum_registracije(user.getDatum_registracije());
		userToUpdate.setStatus(user.getStatus());
		userToUpdate.setPol(user.getPol());
		userToUpdate.setNacionalnost(user.getNacionalnost());
		userToUpdate.setGodine(user.getGodine());
		userCollection.replaceOne(Filters.eq("_id",userToUpdate.getId()), userToUpdate);
		return userToUpdate;
	}
	public User deleteUser(int id) {
		User userToDelete = this.findUserById(id);
		userCollection.deleteOne(Filters.eq("_id",userToDelete.getId()));
		return userToDelete;
	}
}
