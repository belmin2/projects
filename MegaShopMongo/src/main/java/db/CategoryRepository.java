package db;

import static com.mongodb.MongoClientSettings.getDefaultCodecRegistry;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import java.util.ArrayList;
import java.util.List;


import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

import model.Category;
import model.User;

public class CategoryRepository {
	
	private static MongoClient getConnection() {
		int port_no = 27017;
		String url="localhost";
		
		MongoClient mongoClntObj = new MongoClient(url,port_no);
		return mongoClntObj;
	}
	CodecProvider pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build();
	CodecRegistry pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(), fromProviders(pojoCodecProvider));
	private String db_name = "MegaShop",db_collection_name = "categories";
	private MongoDatabase db = getConnection().getDatabase(db_name);
    private MongoCollection categoryCollection = db.getCollection(db_collection_name, Category.class).withCodecRegistry(pojoCodecRegistry);
    
    public Category findCategoryById(int id)
	{
		Category findCategory = (Category) categoryCollection.find(Filters.eq("_id",id)).first();
		return findCategory;
	}
	public List<Category> getAllCategories()
	{
		List<Category> categories = new ArrayList<>();
		categoryCollection.find().into(categories);
		return categories;
	}
	public Category addCategory(Category category)
	{
    	int categoryId;
		if(categoryCollection.find().sort(Sorts.descending("_id")).first() == null)
		{
			categoryId = 0;
		}
		else
		{
			categoryId = ((Category)categoryCollection.find().sort(Sorts.descending("_id")).first()).getId();
			categoryId++;
		}
		category.setId(categoryId);
        categoryCollection.insertOne(category);
		return category;
	}
	public Category updateCategory(Category category) {
		Category categoryToUpdate = this.findCategoryById(category.getId());
		categoryToUpdate.setNaziv(category.getNaziv());
		categoryCollection.replaceOne(Filters.eq("_id",categoryToUpdate.getId()), categoryToUpdate);
		return categoryToUpdate;
	}
	public Category deleteCategory(int id) {
		Category categoryToDelete = this.findCategoryById(id);
		categoryCollection.deleteOne(Filters.eq("_id",categoryToDelete.getId()));
		return categoryToDelete;
	}
}
