package db;

import static com.mongodb.MongoClientSettings.getDefaultCodecRegistry;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import java.util.ArrayList;
import java.util.List;

import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

import model.BoughtProducts;
import model.Category;
import model.User;

public class BoughtProductsRepository {
	
	private static MongoClient getConnection() {
		int port_no = 27017;
		String url="localhost";
		
		MongoClient mongoClntObj = new MongoClient(url,port_no);
		return mongoClntObj;
	}
	CodecProvider pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build();
	CodecRegistry pojoCodecRegistry = fromRegistries(getDefaultCodecRegistry(), fromProviders(pojoCodecProvider));
	private String db_name = "MegaShop",db_collection_name = "bought_products";
	private MongoDatabase db = getConnection().getDatabase(db_name);
    private MongoCollection boughtProductsCollection = db.getCollection(db_collection_name, BoughtProducts.class).withCodecRegistry(pojoCodecRegistry);
    

	
	public long getBuyId() {
		
		return boughtProductsCollection.count();
	}
	public List<BoughtProducts> getAllBoughtProductsByIdUser(int id_korisnika)
	{
		List<BoughtProducts> boughtProducts = new ArrayList<>();
		boughtProductsCollection.find(Filters.eq("id_korisnika",id_korisnika)).into(boughtProducts);
		return boughtProducts;
		
	}
	public BoughtProducts saveBuy(BoughtProducts boughtProduct)
	{
		int boughtProductId;
		if(boughtProductsCollection.find().sort(Sorts.descending("_id")).first() == null)
		{
			boughtProductId = 0;
		}
		else
		{
			boughtProductId = ((BoughtProducts)boughtProductsCollection.find().sort(Sorts.descending("_id")).first()).getId();
			boughtProductId++;
		}
		boughtProduct.setId(boughtProductId);
        boughtProductsCollection.insertOne(boughtProduct);
		return boughtProduct;
	}
}
