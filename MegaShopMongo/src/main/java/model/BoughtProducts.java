package model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class BoughtProducts implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int id;
	private int id_korisnika;
	private List<Product> listaProizvoda;
	private Date datum_kupovine;
	private double ukupna_cena;
	
	

	



	public BoughtProducts(int id_korisnika, List<Product> listaProizvoda, Date datum_kupovine, double ukupna_cena) {
		super();
		this.id_korisnika = id_korisnika;
		this.listaProizvoda = listaProizvoda;
		this.datum_kupovine = datum_kupovine;
		this.ukupna_cena = ukupna_cena;
	}



	public BoughtProducts() {
		super();
		// TODO Auto-generated constructor stub
	}


	
	public double getUkupna_cena() {
		return ukupna_cena;
	}



	public void setUkupna_cena(double ukupna_cena) {
		this.ukupna_cena = ukupna_cena;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public int getId_korisnika() {
		return id_korisnika;
	}



	public void setId_korisnika(int id_korisnika) {
		this.id_korisnika = id_korisnika;
	}



	public List<Product> getListaProizvoda() {
		return listaProizvoda;
	}



	public void setListaProizvoda(List<Product> listaProizvoda) {
		this.listaProizvoda = listaProizvoda;
	}



	public Date getDatum_kupovine() {
		return datum_kupovine;
	}



	public void setDatum_kupovine(Date datum_kupovine) {
		this.datum_kupovine = datum_kupovine;
	}



	@Override
	public String toString() {
		return "BoughtProducts [id=" + id + ", id_korisnika=" + id_korisnika + ", listaProizvoda=" + listaProizvoda
				+ ", datum_kupovine=" + datum_kupovine + "]";
	}
	
	
	
	
	
	
}
