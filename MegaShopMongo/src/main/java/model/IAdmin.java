package model;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface IAdmin extends Remote{
	void deleteProduct(int id) throws RemoteException;
	void changeProduct(Product p) throws RemoteException;
	void addProduct(Product p) throws RemoteException;
	void deleteLocation(int id) throws RemoteException;
	void changeLocation(Location l) throws RemoteException;
	void addLocation(Location l) throws RemoteException;
	void deleteUser(int id) throws RemoteException;
	void addAdminStatus(int id) throws RemoteException;
	void deleteCategory(int id) throws RemoteException;
	void changeCategory(Category c) throws RemoteException;
	void addCategory(Category c) throws RemoteException;
	List<User> getAllUsers() throws RemoteException;
	List<Category> getAllCategories() throws RemoteException;
	String getProductPicture(int id) throws RemoteException;
	boolean findProductWithCategoryId(int id) throws RemoteException;
}
