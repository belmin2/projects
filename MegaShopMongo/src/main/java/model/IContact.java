package model;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface IContact extends Remote {
	List<Location> getAllLocations() throws RemoteException;
	List<Location> getLocations(String type) throws RemoteException;
}
