package model;

import java.io.Serializable;

public class Location implements Serializable{

	private static final long serialVersionUID = 1L;

	private int id;
	
	private String drzava;
	private String telefon;
	private String adresa;
	private String tip;
	private String grad;
	
	
	
	public Location() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Location(String drzava, String telefon, String adresa, String tip, String grad) {
		super();
		this.drzava = drzava;
		this.telefon = telefon;
		this.adresa = adresa;
		this.tip = tip;
		this.grad = grad;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDrzava() {
		return drzava;
	}
	public void setDrzava(String drzava) {
		this.drzava = drzava;
	}
	public String getTelefon() {
		return telefon;
	}
	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	
	public String getTip() {
		return tip;
	}
	public void setTip(String tip) {
		this.tip = tip;
	}
	public String getGrad() {
		return grad;
	}
	public void setGrad(String grad) {
		this.grad = grad;
	}
	@Override
	public String toString() {
		return "Location [id=" + id + ", drzava=" + drzava + ", telefon=" + telefon + ", adresa=" + adresa + ", tip="
				+ tip + ", grad=" + grad + "]";
	}
	
	
}
