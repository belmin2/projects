package model;

import java.io.Serializable;


public class Product implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private int id;
	
	private double cena;
	private String naziv;
	private String opis;
	private int kolicina;
	private String boja;
	private int id_kategorije;
	private String slika;
	
	
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Product(double cena, String naziv, String opis, int kolicina, String boja, int id_kategorije, String slika) {
		super();
		this.cena = cena;
		this.naziv = naziv;
		this.opis = opis;
		this.kolicina = kolicina;
		this.boja = boja;
		this.id_kategorije = id_kategorije;
		this.slika = slika;
	}

	public int getId_kategorije() {
		return id_kategorije;
	}
	public void setId_kategorije(int id_kategorije) {
		this.id_kategorije = id_kategorije;
	}
	public String getBoja() {
		return boja;
	}
	public void setBoja(String boja) {
		this.boja = boja;
	}
	public int getKolicina() {
		return kolicina;
	}
	public void setKolicina(int kolicina) {
		this.kolicina = kolicina;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	
	public String getSlika() {
		return slika;
	}
	public void setSlika(String slika) {
		this.slika = slika;
	}
	@Override
	public String toString() {
		return "Product [id=" + id + ", cena=" + cena + ", naziv=" + naziv + ", opis=" + opis + "]";
	}
	
	
	
}
