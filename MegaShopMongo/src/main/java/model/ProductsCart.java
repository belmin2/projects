package model;

import java.io.Serializable;

public class ProductsCart{
	//idKupovine,nazivProizvoda,kolicina(izBoughtProducts),Cena
	private int id;
	private int id_kupovine;
	private String nazivProizvoda;
	private int kolicina;
	private double cena;
	
	
	public ProductsCart() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ProductsCart(int id_kupovine,String naziv,int kolicina,double cena)
	{
		this.id_kupovine=id_kupovine;
		this.nazivProizvoda=naziv;
		this.kolicina=kolicina;
		this.cena=cena;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getId_kupovine() {
		return id_kupovine;
	}
	public void setId_kupovine(int id_kupovine) {
		this.id_kupovine = id_kupovine;
	}
	public String getNazivProizvoda() {
		return nazivProizvoda;
	}
	public void setNazivProizvoda(String nazivProizvoda) {
		this.nazivProizvoda = nazivProizvoda;
	}
	public int getKolicina() {
		return kolicina;
	}
	public void setKolicina(int kolicina) {
		this.kolicina = kolicina;
	}
	
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	@Override
	public String toString() {
		return "ProductsCart [id=" + id + ", id_kupovine=" + id_kupovine + ", nazivProizvoda=" + nazivProizvoda
				+ ", kolicina=" + kolicina + ", Cena=" + cena + "]";
	}
	
	
}
