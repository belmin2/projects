package model;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface IProduct extends Remote{
	List<Product> getAllProducts() throws RemoteException;
	List<Category> getAllCategories() throws RemoteException;
	long getIdOfBuy() throws RemoteException;
	Product getProductById(int id) throws RemoteException;
	boolean saveBuy(BoughtProducts boughtProduct) throws RemoteException;
	void changeProduct(Product p) throws RemoteException;
}
