package model;

import java.io.Serializable;
import java.util.Date;


public class User implements Serializable{

	private static final long serialVersionUID = 1L;

	private int id;
	
	private String korisnicko_ime;
	private String email;
	private String sifra;
	private String adresa;
	private String grad;
	private String drzava;
	private String broj_telefona;
	private Date datum_registracije;
	private int status;
	private int pol;
	private int godine;
	private String nacionalnost;
	
	

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public User(String name,String email,String password,String adress,String city,String country,String phone,String nationalty,int pol,int godine,Date datum)
	{
		super();
		korisnicko_ime = name;
		this.email=email;
		sifra=password;
		adresa=adress;
		grad=city;
		drzava=country;
		broj_telefona=phone;
		nacionalnost=nationalty;
		this.pol=pol;
		this.godine = godine;
		status=0;
		datum_registracije = datum;
	}
	
	public String getNacionalnost() {
		return nacionalnost;
	}

	public void setNacionalnost(String nacionalnost) {
		this.nacionalnost = nacionalnost;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getKorisnicko_ime() {
		return korisnicko_ime;
	}
	public void setKorisnicko_ime(String korisnicko_ime) {
		this.korisnicko_ime = korisnicko_ime;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSifra() {
		return sifra;
	}
	public void setSifra(String sifra) {
		this.sifra = sifra;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	public String getGrad() {
		return grad;
	}
	public void setGrad(String grad) {
		this.grad = grad;
	}
	public String getDrzava() {
		return drzava;
	}
	public void setDrzava(String drzava) {
		this.drzava = drzava;
	}
	public String getBroj_telefona() {
		return broj_telefona;
	}
	public void setBroj_telefona(String broj_telefona) {
		this.broj_telefona = broj_telefona;
	}
	public Date getDatum_registracije() {
		return datum_registracije;
	}
	public void setDatum_registracije(Date datum_registracije) {
		this.datum_registracije = datum_registracije;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getPol() {
		return pol;
	}
	public void setPol(int pol) {
		this.pol = pol;
	}
	public int getGodine() {
		return godine;
	}
	public void setGodine(int godine) {
		this.godine = godine;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", korisnicko_ime=" + korisnicko_ime + ", email=" + email + ", sifra=" + sifra
				+ ", adresa=" + adresa + ", grad=" + grad + ", drzava=" + drzava + ", broj_telefona=" + broj_telefona
				+ ", datum_registracije=" + datum_registracije + ", status=" + status + ", pol=" + pol + ", godine="
				+ godine + ", nacionalnost=" + nacionalnost + "]";
	}
	
}
