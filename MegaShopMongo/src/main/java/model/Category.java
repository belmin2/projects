package model;

import java.io.Serializable;

public class Category implements Serializable{

	private static final long serialVersionUID = 1L;
	private int id;
	private String naziv;
	
	
	public Category(String naziv) {
		super();
		this.naziv = naziv;
	}
	public Category() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	@Override
	public String toString() {
		return "Category [id=" + id + ", naziv=" + naziv + "]";
	}
	
	
}
