<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
 <head>
        <title>Mega Shop</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../css/style.css" rel="stylesheet" type="text/css"/>
        <link href="../css/registerstyle.css" rel="stylesheet" type="text/css"/>
        <link rel="apple-touch-icon" sizes="180x180" href="../icon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../icon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../icon/favicon-16x16.png">
        <link rel="manifest" href="../icon/site.webmanifest">
        <script>
            function proveri(checkboxpr)
            {
                chm = document.getElementById("polm");
                chz = document.getElementById("polz");
                checkv = document.getElementById("polvrednost");
                if(checkboxpr == 1)
                {
                    chm.checked = true;
                    chz.checked = false;
                    checkv.value="muski";
                }
                else
                {
                    chz.checked = true;
                    chm.checked = false;
                    checkv.value="zenski";
                }
            }
        </script>
    </head>
    <body onLoad="resetErrors();">
        <div id="header">
            <img src="../images/megashoplogonew.png" alt="logo" style="width:250px;height: 100px">
            <div id="flag">
                <ul>
                    <li>
                        <img src="../images/srb4.png" style="width: 50px;height:50px" alt="srb">
                    </li>
                    <li style="padding-top:32px;">
                       064/12356789<br>
                       034/7882390
                    </li>
                    <li id="phonenumber" style="padding-right: 5px;">
                       <img src="../images/phone.png" style="width: 50px;height:50px;" alt="phone">
                    </li>
                </ul>     
            </div>
        </div>
        <div id="menu">
            <ul>
                <li><a href="../index.jsp">Prijava</a></li>
                <li><a href="register.jsp">Registracija</a></li>
            </ul>
        </div>
        <div id="registracija"><br>
            <div id="registracijabox">
                <form id="registerForm" action="${pageContext.request.contextPath}/AuthServlet" class="register" method="post"
                onSubmit="return validateRegister();">
                <h1>Registracija</h1>
                <fieldset class="row1">
                <legend>Detalji naloga
                </legend>
                    <ul>
                        <li>
                        Email *
                        <input type="text" name="email" id="email"/>
                        <p id="errorEmailRegister" style="color: red"></p>
                        </li>
                        <li>
                            <label>Sifra*
                            </label>
                            <input type="text" name="sifra" id="sifra">
                             <p id="errorPasswordRegister" style="color: red"></p>
                        </li>
                        <li>
                            Ponovi email *
                            
                            <input type="text" name="ponovljeniemail" id="ponovljenimajil"/>
                        	<p id="errorEmailPRegister" style="color: red"></p>
                        </li>
                        <li>
                            <label>Ponovi sifru*
                            </label>
                            <input type="text" name="passponovo" id="passponovo"/>
                            <p id="errorPasswordPRegister" style="color: red"></p>
                        </li>
                    </ul>
            </fieldset>
                <fieldset class="row2">
                    <legend>Personalni detalji
                    </legend>
                    <ul>
                        <li>
                            Ime* <input type="text" name="ime" id="ime"/>
                             <p id="errorNameRegister" style="color: red"></p>
                        </li>
                        <li>
                            Telefon <input type="text" name="telefon" id="telefon"/>
                        </li>
                        <li>
                            Adresa <input type="text" name="adresa" id="adresa"/>
                        </li>
                        <li>
                            Grad <input type="text" name="grad" id="grad"/>
                        </li>
                    </ul>
                </fieldset>
                <fieldset class="row3">
                    <legend>Detlajnije informacije
                    </legend>
                    <ul>
                        <li>
                            Pol 
                            <input type="checkbox" id="polm" name="polmuski" onclick="proveri(1)" checked> Muski 
                            <input type="checkbox" name="polzenski" id="polz" onclick="proveri(2)"> Zenski
                        	<input type="hidden" name="polvrednost" id="polvrednost" value="muski">
                        </li>
                        <li>
                            Godine <input type="text" name="godine" id="godine"/>
                        </li>
                        <li>
                            Nacionalnost <input type="text" name="nacionalnost" id="nacionalnost"/>
                        </li>
   						<li>
                            Drzava <input type="text" name="drzava" id="drzava"/>
                        </li>
                    </ul>
                </fieldset>
                <fieldset class="row4">
                   <legend>Uslovi koriscenja
                    </legend>
                    <ul>
                        <li>
                            <input type="checkbox" id="prihvatiuslove" name="prihvatauslove"> *Ja prihvatam uslove koriscenja. 
                        	<p id="errorConditionsRegister" style="color: red"></p>
                        </li>
                        <li>
                            <input type="checkbox" name="prihvataper"> Zelim da dobijam personalne ponude za proizvode od strane sajta. 
                        </li>
                        <li>
                            <span style="color:red">Sva polja oznacena sa * su obavezna polja!</span>
                        </li>
                    </ul>
                    <div id="dugmereg">
                        <br>
                        <button id="dugmezareg">
                            Registruj se
                        </button>
                        <br>
                    </div>
                </fieldset>
            </form>
            </div>   
        </div>
        <div id="footer">
            <div id="mreze">
                <ul>
                    <li>
                        <img src="../images/twiter.png" alt="twiter" style="height: 50px;width:50px">
                    </li>
                    <li>
                        <img src="../images/instagram.jpg" alt="instagram" style="height: 50px;width:50px">
                    </li>
                    <li>
                        <img src="../images/facebook.png" alt="facebook" style="height: 50px;width:50px">
                    </li>
                </ul>
            </div>
            <br><br>
            MegaShop &#169 2022<br>
            megashop@gmail.com<br>
        </div>
        <script>
        function resetErrors() {
			document.getElementById("errorNameRegister").innerHTML = "";
			document.getElementById("errorEmailRegister").innerHTML = "";
			document.getElementById("errorPasswordRegister").innerHTML = "";
			document.getElementById("errorEmailPRegister").innerHTML = "";
			document.getElementById("errorPasswordPRegister").innerHTML = "";
			document.getElementById("errorConditionsRegister").innerHTML = "";
		}
        function validateRegister()
        {
        	resetErrors();
        	var err=false;
        	var email=document.forms["registerForm"]["email"].value;
        	var pass=document.forms["registerForm"]["sifra"].value;
        	var emailp=document.forms["registerForm"]["ponovljeniemail"].value;
        	var passp=document.forms["registerForm"]["passponovo"].value;
        	var name=document.forms["registerForm"]["ime"].value;
        	var prihvatiUslove = document.getElementById("prihvatiuslove");
        	if(!email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/))
        	{
        		if(email == "" || email == null)
        		{
        			document.getElementById("errorEmailRegister").innerHTML = "Email ne moze biti prazan";	
        		}
        		else
        		{
        			document.getElementById("errorEmailRegister").innerHTML = "Los format Email-a!";		
        		}
        		err=true;
        	}
        	if(!(email == emailp))
        	{
        		document.getElementById("errorEmailRegister").innerHTML = "Emailovi moraju biti isti";
        		err=true;
        	}
        	if(!(pass == passp))
        	{
        		document.getElementById("errorPasswordRegister").innerHTML = "Lozinke moraju biti iste";
            	err=true;	
        	}
        	if(!pass.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/))
        	{
        		if(pass == "" || pass == null)
            	{
            		document.getElementById("errorPasswordRegister").innerHTML = "Lozinka ne moze biti prazna";
            	}
        		else {
        			if(pass.length < 6)
        			{
        				document.getElementById("errorPasswordRegister").innerHTML = "Lozinka ne moze biti kraca od 6 karaktera";
        			}
        			if(pass.search(/[0-9]/) < 0)
        			{
        				document.getElementById("errorPasswordRegister").innerHTML = "Lozinka mora sadrzati barem jedan broj";
        			}
        			if(pass.search(/[!@#$%^&*]/) < 0)
        			{
        				document.getElementById("errorPasswordRegister").innerHTML = "Lozinka mora sadrzati barem jedan specijalni karakter($@!#%&*)";
        			}
        		}
        		err=true
        	}
        	if(!name.match(/^[A-Za-z]+$/))
        	{
        		if(name == null || name == "")
        		{
        			document.getElementById("errorNameRegister").innerHTML = "Ime ne moze biti prazno";
        		}
        		else {
        			document.getElementById("errorNameRegister").innerHTML = "Ime mora sadrzati samo velika i mala slova";
        		}
        		err=true;
        	}
        	if(prihvatiUslove.checked == false)
        	{
        		document.getElementById("errorConditionsRegister").innerHTML = "Niste prihvatili uslove koriscenja";
        		err=true;			
        	}
        	if(err)
        		return false;
       }
        </script>
    </body>
</html>