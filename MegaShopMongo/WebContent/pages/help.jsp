<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="model.*" %>
<%@ page import="java.util.*" %>
<%
	Object id = session.getAttribute("UserId");
	Object name = session.getAttribute("name");
	Object admin = session.getAttribute("Admin");
	String nameString="";
	if(id == null || name == null)
	{
		response.sendRedirect("../index.jsp");
	}
	else
	{
		nameString = name.toString();
	}
%>
<!DOCTYPE html>
<html>
<head>
<title>Mega Shop</title>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <link href="css/style.css" rel="stylesheet" type="text/css"/>
	    <link href="css/helpstyle.css" rel="stylesheet" type="text/css"/>
	    <link rel="apple-touch-icon" sizes="180x180" href="icon/apple-touch-icon.png">
	    <link rel="icon" type="image/png" sizes="32x32" href="icon/favicon-32x32.png">
	    <link rel="icon" type="image/png" sizes="16x16" href="icon/favicon-16x16.png">
	    <link rel="manifest" href="icon/site.webmanifest">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div id="header">
		<img src="images/megashoplogonew.png" alt="logo"
			style="width: 250px; height: 100px">
		<div id="flag">
			<ul>
				<li><img src="images/user.png"
					style="width: 50px; height: 50px" alt="user">
					<p style="float: right; padding-left: 10px">
						<%out.print(nameString);%>
					</p></li>
				<li><img src="images/srb4.png"
					style="width: 50px; height: 50px" alt="srb"></li>
				<li style="padding-top: 32px;">064/12356789<br>
					034/7882390
				</li>
				<li id="phonenumber" style="padding-right: 5px;"><img
					src="images/phone.png" style="width: 50px; height: 50px;"
					alt="phone"></li>
			</ul>
		</div>
	</div>
	<div id="menu">
		<ul>
			<li><a href="pages/home.jsp">Pocetna</a></li>
			<li><a href="${pageContext.request.contextPath}/ProductServlet">Proizvodi</a></li>
			<li><a href="${pageContext.request.contextPath}/UserServlet">Nalog</a></li>
			<li><a href="${pageContext.request.contextPath}/ContactServlet">Kontakt</a></li>
			<li><a href="pages/help.jsp">Podrska</a></li>
			<%
            	if(admin != null)
            	{
            		%><li><a
				href="${pageContext.request.contextPath}/AdminServlet">Admin</a></li>
			<%
            	}
            	%>
            <li><a href="pages/logout.jsp">Odjavi se</a></li>
		</ul>
	</div>
	<div id="footer">
		<div id="mreze">
			<ul>
				<li><img src="images/twiter.png" alt="twiter"
					style="height: 50px; width: 50px"></li>
				<li><img src="images/instagram.jpg" alt="instagram"
					style="height: 50px; width: 50px"></li>
				<li><img src="images/facebook.png" alt="facebook"
					style="height: 50px; width: 50px"></li>
			</ul>
		</div>
		<br>
		<br> MegaShop &#169; 2022<br> megashop@gmail.com<br>
	</div>
</body>
</html>