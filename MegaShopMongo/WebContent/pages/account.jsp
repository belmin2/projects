<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*" %>
<%@ page import="java.util.*" %>
<%
	Object id = session.getAttribute("UserId");
	Object name = session.getAttribute("name");
	Object admin = session.getAttribute("Admin");
	String nameString="";
	int i=0;
	if(id == null || name == null)
	{
		response.sendRedirect("../index.jsp");
	}
	else
	{
		nameString = name.toString();
	}
	List<BoughtProducts> listOfBoughtProducts = (List<BoughtProducts>)request.getAttribute("products");
	User user = (User)request.getAttribute("user");
%>
<!DOCTYPE html>
<html>
	<head>
		<title>Mega Shop</title>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <link href="css/style.css" rel="stylesheet" type="text/css"/>
	    <link href="css/accountstyle.css" rel="stylesheet" type="text/css"/>
	    <link rel="apple-touch-icon" sizes="180x180" href="icon/apple-touch-icon.png">
	    <link rel="icon" type="image/png" sizes="32x32" href="icon/favicon-32x32.png">
	    <link rel="icon" type="image/png" sizes="16x16" href="icon/favicon-16x16.png">
	    <link rel="manifest" href="icon/site.webmanifest">
	    <script src="javascript/account.js" type="text/javascript"></script>
	</head>
	<body>
		<div id="header">
            <img src="images/megashoplogonew.png" alt="logo" style="width:250px;height: 100px">
            <div id="flag">
                <ul>
                	<li>
                        <img src="images/user.png" style="width: 50px;height:50px" alt="user">
                        <p style="float:right;padding-left:10px">
                        <%out.print(nameString);%>
                        </p>
                    </li>
                    <li>
                        <img src="images/srb4.png" style="width: 50px;height:50px" alt="srb">
                    </li>
                    <li style="padding-top:32px;">
                       064/12356789<br>
                       034/7882390
                    </li>
                    <li id="phonenumber" style="padding-right: 5px;">
                       <img src="images/phone.png" style="width: 50px;height:50px;" alt="phone">
                    </li>
                </ul>     
            </div>
    	</div>
    	<div id="menu">
            <ul>
            	<li><a href="pages/home.jsp">Pocetna</a></li>
                <li><a href="${pageContext.request.contextPath}/ProductServlet">Proizvodi</a></li>
                <li><a href="${pageContext.request.contextPath}/UserServlet">Nalog</a></li>
                <li><a href="${pageContext.request.contextPath}/ContactServlet">Kontakt</a></li>
                <!--<li><a href="pages/help.jsp">Podrska</a></li>-->
            	<%
            	if(admin != null)
            	{
            		%><li><a href="${pageContext.request.contextPath}/AdminServlet">Admin</a></li><%
            	}
            	%>
            	
                <li><a href="pages/logout.jsp">Odjavi se</a></li>
            </ul>
        </div>
        <div id="person"><br>
            <div id="personprofile">
                <div id="profile1">
                    <br>
                    <img src="images/128x128.png" alt="profile picture" id="profilepicture">
                    <br>
                    <%out.print(nameString);
                    if(admin != null)
	            	{
	            		out.print("(Admin)");	
	            	}%>
                </div><br>
                <div id="profile2">
                    <div id="menip">
                    <ul>
                        <li>
                            <input type="button" class="dugmeprofil" onclick="openDiv('podacip')" value="Podaci">
                        </li>
                        <li>
                            <input type="button" class="dugmeprofil" onclick="openDiv('kupovine')" value="Kupovine">
                        </li>
                    </ul>
                    </div>
                    <div id="podacip">
                        <ul>
                            <li>
                                Ime: <%out.print(user.getKorisnicko_ime()); %>
                            </li>
                            <li>
                                Email: <%out.print(user.getEmail()); %>
                            </li>
                            <li>
                               	Adresa: <%out.print(user.getAdresa()); %>
                            </li>
                            <li>
                               	Grad: <%out.print(user.getGrad()); %>
                            </li>
                            <li>
                               	Drzava: <%out.print(user.getDrzava()); %>
                            </li>
                            <li>
                               	Broj Telefona: <%out.print(user.getBroj_telefona()); %>
                            </li>
                            <li>
                               	Datum registracije: <%out.print(user.getDatum_registracije()); %>
                            </li>
                            <li>
                               	Pol: 
                               	<%
                               		if(user.getPol() == 0)
                               		{
                               			out.print("Muski");
                               		}
                               		else
                               		{
                               			out.print("Zenski");
                               		}
                               	%>
                            </li>
                            <li>
                               	Godine: <%out.print(user.getGodine()); %>
                            </li>
                            <li>
                               	Nacionalnost: <%out.print(user.getNacionalnost()); %>
                            </li>
                        </ul>
                    </div>
                    <div id="kupovine">
                        <ul>
                        	<%
                        	for(BoughtProducts bp : listOfBoughtProducts)
                        	{
                        		i++;
                        		%>
                        		<li onclick="tabelaKupovine(<%out.print(i);%>)">
                        			<table style="width:100%;padding-left:3px">
                                    <tr>
                                        <th>Broj kupovine:<%out.print(i);%></th>
                                        <th>Ukupna cena:<%out.print(bp.getUkupna_cena()); %></th>
                                        <th><p class="arrow" id="arrowTabela<%out.print(i);%>">&#9654</p></th>
                                    </tr>
                                	</table>
                        		</li>
                        		<%
                        		%><li>
                                <table class="tabelakupljenih" id="kupovina<%out.print(i);%>" style="display:none">
                                    <tr>
                                        <th>Rbr</th>
                                        <th>Naziv proizvoda</th>
                                        <th>Kolicina</th>
                                        <th>Cena</th>
                                    </tr>
                                    <%
                                    int counter=0;
                                    for(Product pc : bp.getListaProizvoda())
                        			{
                        				counter++;
                        				%>
                        				<tr>
                                        <td><%out.print(counter); %></td>
                                        <td><%out.print(pc.getNaziv()); %></td>
                                        <td><%out.print(pc.getKolicina()); %></td>
                                        <td><%out.print(pc.getCena()); %></td>
                                        </tr>
                                        <% 
                        			}%>
                                </table>
                            </li>
                        	<%
                        	}
                        	%>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="footer">
            <div id="mreze">
                <ul>
                    <li>
                        <img src="images/twiter.png" alt="twiter" style="height: 50px;width:50px">
                    </li>
                    <li>
                        <img src="images/instagram.jpg" alt="instagram" style="height: 50px;width:50px">
                    </li>
                    <li>
                        <img src="images/facebook.png" alt="facebook" style="height: 50px;width:50px">
                    </li>
                </ul>
            </div>
            <br><br>
            MegaShop &#169 2022<br>
            megashop@gmail.com<br>
        </div>
	</body>
</html>