<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="model.*" %>
<%@ page import="java.util.*" %>
<%
	Object id = session.getAttribute("UserId");
	Object name = session.getAttribute("name");
	Object admin = session.getAttribute("Admin");
	String nameString="";
	if(id == null || name == null)
	{
		response.sendRedirect("../index.jsp");
	}
	else
	{
		nameString = name.toString();
	}
	List<Category> categories = (List<Category>)request.getAttribute("categories");
	String pageString = (String)request.getAttribute("page");
%>
<!DOCTYPE html>
<html>
	<head>
		<title>Mega Shop</title>
	    <meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <link href="css/style.css" rel="stylesheet" type="text/css"/>
	    <link href="css/adminstyle.css" rel="stylesheet" type="text/css"/>
	    <link rel="apple-touch-icon" sizes="180x180" href="icon/apple-touch-icon.png">
	    <link rel="icon" type="image/png" sizes="32x32" href="icon/favicon-32x32.png">
	    <link rel="icon" type="image/png" sizes="16x16" href="icon/favicon-16x16.png">
	    <link rel="manifest" href="icon/site.webmanifest">
	    <script src="javascript/admin.js" type="text/javascript"></script>
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	</head>
	<body onload="CallAjaxForCategories()">
		<div id="header">
            <img src="images/megashoplogonew.png" alt="logo" style="width:250px;height: 100px">
            <div id="flag">
                <ul>
                	<li>
                        <img src="images/user.png" style="width: 50px;height:50px" alt="user">
                        <p style="float:right;padding-left:10px">
                        <%out.print(nameString);%>
                        </p>
                    </li>
                    <li>
                        <img src="images/srb4.png" style="width: 50px;height:50px" alt="srb">
                    </li>
                    <li style="padding-top:32px;">
                       064/12356789<br>
                       034/7882390
                    </li>
                    <li id="phonenumber" style="padding-right: 5px;">
                       <img src="images/phone.png" style="width: 50px;height:50px;" alt="phone">
                    </li>
                </ul>     
            </div>
    	</div>
    	<div id="menu">
            <ul>
            	<li><a href="pages/home.jsp">Pocetna</a></li>
                <li><a href="${pageContext.request.contextPath}/ProductServlet">Proizvodi</a></li>
                <li><a href="${pageContext.request.contextPath}/UserServlet">Nalog</a></li>
                <li><a href="${pageContext.request.contextPath}/ContactServlet">Kontakt</a></li>
                <!--<li><a href="pages/help.jsp">Podrska</a></li>-->
            	<%
            	if(admin != null)
            	{
            		%><li><a href="${pageContext.request.contextPath}/AdminServlet">Admin</a></li><%
            	}
            	%>
            	<li><a href="pages/logout.jsp">Odjavi se</a></li>
            </ul>
        </div>
        <main>
            <aside class="sticky2">
                <div id="sticky2header">
                    <h1>Admin panel</h1>
                </div>
                    <ul>
                        <li onclick="ChangePage(1)">
                            <img src="icon/userpanel.png" style="width:17%;height:50px;float:left" alt="userpanel"><p>Korisnici</p>
                        </li>
                        <li onclick="ChangePage(2)">
                            <img src="icon/product.png" style="width:17%;height:50px;float:left" alt="product"><p>Proizvodi</p>
                        </li>
                        <li onclick="ChangePage(3)">
                            <img src="icon/location.png" style="width:17%;height:50px;float:left" alt="location"><p>Lokacije</p>
                        </li>
                        <li onclick="ChangePage(4)">
                            <img src="icon/category.png" style="width:17%;height:50px;float:left" alt="category"><p>Kategorije</p>
                        </li>
                    </ul>
            </aside>
            <section class="admin" id="selectionadmin">
                <div id="useradmin">
                    <h1>Korisnici</h1>
                    <div id="pretragakorisnika">
                        <ul>
                            <li>
                                Ime: <input class="pretragainput" type="text" name="username" placeholder="Ime">
                            </li>
                            <li>
                                Email: <input class="pretragainput" type="text" name="email" placeholder="Emai">
                            </li>
                        </ul>
                    </div>
                    <br>
                    <table id="tabelakorisnika">
                    </table>
                    <br>
                    <h1>Admin korisnici</h1>
                    <div id="pretragaadminkorisnika">
                        <ul>
                            <li>
                                Ime: <input class="pretragainput" type="text" name="username" placeholder="Ime">
                            </li>
                            <li>
                                Email: <input class="pretragainput" type="text" name="email" placeholder="Emai">
                            </li>
                        </ul>
                    </div>
                    <br>
                    <table id="tabelaadminkorisnika">
                    </table>
                </div>
                <div id="productadmin">
                    <h1>Proizvodi</h1>
                    <div id="pretragaproizvoda">
                        <ul>
                            <li>
                                Naziv: <input class="pretragainput" oninput="nameFiltering()" type="text" id="nazivproizvoda" placeholder="Naziv">
                            </li>
                            <li>
                                Boja: <input class="pretragainput"  oninput="colorFiltering()" type="text" id="boja" placeholder="Boja">
                            </li>
                            <li>
                                 Cena: od&nbsp;<input type="text" id="cenaod"  oninput="checkPriceFrom()" style="background-color:#121314;color:lightblue;border:1px solid lightblue;width:30%;height: 25px">&nbsp;do&nbsp;
                                <input type="text" id="cenado" oninput="checkPriceTo()" style="background-color:#121314;color:lightblue;border:1px solid lightblue;width:30%;height: 25px;">
                            </li>
                            <li style="width: 66%">
                                <div id="selectadmintext">Kategorija:&nbsp;</div> 
                                <div class="selectadmin" onchange="categoryFilter()">
                                <select id="standard-select-product" >
									<%
									int counter = 0;
									for (Category c : categories) {
										counter++;
										%><option value="<%out.print(counter);%>"><%out.print(c.getNaziv());%></option>
									<%
									}
									%>
								</select>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <br>
                    <button id="dodajproizvod" onclick="newProduct()">Novi proizvod</button>
                    <br><br>
                    <div id="proizvodnovi">
                        <form method="post" action="${pageContext.request.contextPath}/AddProductServlet" enctype="multipart/form-data">
                            <ul>
                                <li>
                                    <input type="hidden" name="idproduct" id="idproduct">
                                    Naziv:<br style="line-height: 150%">
                                     <input class="newinput" type="text" placeholder="Naziv" name="novinaziv" id="novinaziv">
                                </li>
                                <li>
                                    Cena:<br style="line-height: 150%">
                                    <input class="newinputnumber" type="number" placeholder="Cena" name="novacena" id="novacena">
                                </li>
                                <li>
                                    Opis:<br style="line-height: 150%">
                                    <input class="newinput" type="text" placeholder="Opis" name="noviopis" id="noviopis">
                                </li>
                                <li>
                                    Boja:<br style="line-height: 150%">
                                    <input class="newinput" type="text" placeholder="Boja" name="novaboja" id="novaboja">
                                </li>
                                <li>
                                    Kolicina:<br style="line-height: 150%">
                                    <input class="newinputnumber" type="number" placeholder="Kolicina" name="novakolicina" id="novakolicina">
                                </li>
                                <li>
                                    Kategorija:<br style="line-height: 150%">
                                    <div class="selectadmin2">
                                    <select id="standard-select-newproduct" name="newprodcutselect">
                                      <option disabled>Sve</option>
	                               		<%
	                                    counter=0;
	                                    for(Category c : categories)
	                                    {
	                               	    	counter++;
	                                    	%>
	                                    	<option value="<%out.print(counter);%>"><%out.print(c.getNaziv());%></option>
	                                    	<%  
	                                    }
	                          	     	%>
                                    </select>
                                    </div>
                                </li>
                                <li>
                                    Slika:<br style="line-height: 150%">
                                    <input class="newinput" type="text" placeholder="Slika" name="novaslika" id="novaslika">
                                </li>
                                <li>Nova slika:<br style="line-height: 150%">
                                		<input type="file" id="myFile" name="file-name" onchange="updatePictureName(this)">
                                		<input type="button" onClick="openFile()" id="buttonfile" value="Dodaj sliku">
                                </li>
                                <li style="text-align: right;"><br><button class="proizvodnovibuttons" type="submit">Sacuvaj</button></li>
                                <li style="text-align: left;"><br><button class="proizvodnovibuttono" onclick="newProduct();return false;" >Odustani</button></li>
                            </ul>
                        </form>   
                    </div>
                    <table id="tabelaproizvoda">
                        
                    </table>
                    <br>
                </div>
                <div id="locationadmin">
                    <h1>Lokacije</h1>
                    <div id="locationsearch">
                        <ul>
                            <li>
                                Grad: <input class="pretragainput" oninput="cityFiltering()" type="text" name="grad" id="grad" placeholder="Grad">
                            </li>
                            <li>
                                Adresa: <input class="pretragainput" oninput="addressFiltering()" type="text" name="adresa" id="adresa" placeholder="Adresa">
                            </li>
                            <li>
                            	<div id="selectadmintext">Tip:&nbsp;</div> 
                                <div class="selectadmin" onchange="typesFilter()">
                                <select id="standard-select-location" >
                                	<option value="Sve">Sve</option>
									<option value="Maloprodaja">Maloprodaja</option>
	                               	<option value="Veleprodaja">Veleprodaja</option>
								</select>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <br>
                    <button id="addlocation" onclick="newLocation()">Nova lokacija</button>
                    <br><br>
                    <div id="lokacijanovi">
                        <form method="post" action="${pageContext.request.contextPath}/AddLocationServlet">
                            <ul>
                                <li>
                                    <input type="hidden" name="idlocation" id="idlocation">
                                    Drzava:<br style="line-height: 150%">
                                     <input class="newinput" type="text" placeholder="Drzava" name="novadrzava" id="novadrzava">
                                </li>
                                <li>
                                    Tip:<br style="line-height: 150%">
                                    <div class="selectadmin2">
                                    <select id="standard-select-newlocationstype" name="newlocationtypesselect">
	                               		<option value="Maloprodaja">Maloprodaja</option>
	                               		<option value="Veleprodaja">Veleprodaja</option>
                                    </select>
                                    </div>
                                </li>
                                <li>
                                    Telefon<br style="line-height: 150%">
                                    <input class="newinput" type="text" placeholder="Telefon" name="novitelefon" id="novitelefon">
                                </li>
                                <li>
                                    Grad:<br style="line-height: 150%">
                                    <input class="newinput" type="text" placeholder="Grad" name="novigrad" id="novigrad">
                                </li>
                                <li>
                                    Adresa:<br style="line-height: 150%">
                                    <input class="newinput" type="text" placeholder="Adresa" name="novaadresa" id="novaadresa">
                                </li><li></li>
                                <li style="text-align: right;"><br><button class="lokacijanovibuttons" type="submit">Sacuvaj</button></li>
                                <li style="text-align: left;"><br><button class="lokacijanovibuttono" onclick="newLocation();return false;" >Odustani</button></li>
                            </ul>
                        </form>   
                    </div>
                    <table id="locationtable">
                    </table>
                    <br> 
                </div>
                <div id="categoryadmin">
                    <h1>Kategorije</h1>
                    <div id="categorysearch">
                        <ul>
                            <li>
                                Naziv: <input class="pretragainput" oninput="nameCategoryFiltering()" type="text" name="naziv" id="naziv" placeholder="Naziv">
                            </li>
                        </ul>
                    </div>
                    <br>
                    <button id="addcategory" onclick="newCategory()">Nova kategorija</button>
                    <br><br>
                    <div id="kategorijanovi">
                        <form method="post" action="${pageContext.request.contextPath}/AddCategoryServlet">
                            <ul>
                                <li>
                                    <input type="hidden" name="idcategory" id="idcategory">
                                    Naziv:<br style="line-height: 150%">
                                    <input class="newinput" type="text" placeholder="Naziv" name="novinazivkategorije" id="novinazivkategorije">
                                </li>
                                <li></li>
                                <li style="text-align: right;"><br><button class="lokacijanovibuttons" type="submit">Sacuvaj</button></li>
                                <li style="text-align: left;"><br><button class="lokacijanovibuttono" onclick="newCategory();return false;" >Odustani</button></li>
                            </ul>
                        </form>   
                    </div>
                    <table id="categorytable">
                    </table>
                    <br> 
                </div>
            </section>
        </main>
        <div id="footer">
            <div id="mreze">
                <ul>
                    <li>
                        <img src="images/twiter.png" alt="twiter" style="height: 50px;width:50px">
                    </li>
                    <li>
                        <img src="images/instagram.jpg" alt="instagram" style="height: 50px;width:50px">
                    </li>
                    <li>
                        <img src="images/facebook.png" alt="facebook" style="height: 50px;width:50px">
                    </li>
                </ul>
            </div>
            <br><br>
            MegaShop &#169; 2022<br>
            megashop@gmail.com<br>
        </div>
        <script>
        <%
        	if(pageString.equals("product"))
        	{
        		%>ChangePage(2);<%
        	}
        	else if(pageString.equals("users"))
        	{
        		%>ChangePage(1);<%
        	}
        	else if(pageString.equals("location"))
        	{
        		%>ChangePage(3);<%
        	}
        	else
        	{
        		%>ChangePage(4);<%
        	}
        %>
        </script>
	</body>
</html>