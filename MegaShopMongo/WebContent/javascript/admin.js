var newProductActive = false;
var newLocationActive = false;
var newCategoryActive = false;
var selectedCategoryId = 0;
var nameFilter = "";
var priceFrom = "";
var priceTo = "";
var colorFilter = "";
var selectedType = "Sve";
var cityFilter="";
var nameCategoryFilter="";
var addressFilter="";
var productArray = [];
var locationArray = [];
var userArray = [];
var adminUserArray = [];
var categoryArray = [];
function updatePictureName(target)
{
	document.getElementById("novaslika").value = target.files[0].name;
}
function openFile()
{
	var fileUpload = document.getElementById("myFile");
	fileUpload.click();
}
function ChangePage(id) {
	var productPanel = document.getElementById("productadmin");
	var userPanel = document.getElementById("useradmin");
	var locationPanel = document.getElementById("locationadmin");
	var categoryPanel = document.getElementById("categoryadmin");
	if (id == 1) {
		userPanel.style.display = "block";
		productPanel.style.display = "none";
		locationPanel.style.display = "none";
		categoryPanel.style.display = "none";
	}
	else if (id == 2) {
		userPanel.style.display = "none";
		productPanel.style.display = "block";
		locationPanel.style.display = "none";
		categoryPanel.style.display = "none";
	}
	else if(id == 3){
		userPanel.style.display = "none";
		productPanel.style.display = "none";
		locationPanel.style.display = "block";
		categoryPanel.style.display = "none";
	}
	else{
		userPanel.style.display = "none";
		productPanel.style.display = "none";
		locationPanel.style.display = "none";
		categoryPanel.style.display = "block";
	}
}
function newProduct() {
	var newProdcutRow = document.getElementById("proizvodnovi");
	if (newProductActive == false) {
		newProdcutRow.style.display = "revert";
		newProductActive = true;
	}
	else {
		newProdcutRow.style.display = "none";
		newProductActive = false;
		clearProduct();
	}
}
function clearProduct() {
	var select = document.getElementById("standard-select-newproduct");
	document.getElementById("novinaziv").value = "";
	document.getElementById("novaboja").value = "";
	document.getElementById("noviopis").value = "";
	document.getElementById("novacena").value = "";
	document.getElementById("novakolicina").value = "";
	document.getElementById("novaslika").value = "";
	document.getElementById("idproduct").value = "";
	select.selectedIndex = 0;
}
function changeProduct(id) {
	var select = document.getElementById("standard-select-newproduct");
	for (i = 0; i < productArray.length; i++) {
		if (productArray[i].id == id) {
			document.getElementById("novinaziv").value = productArray[i].naziv;
			document.getElementById("novaboja").value = productArray[i].boja;
			document.getElementById("noviopis").value = productArray[i].opis;
			document.getElementById("novacena").value = productArray[i].cena;
			document.getElementById("novakolicina").value = productArray[i].kolicina;
			document.getElementById("novaslika").value = productArray[i].slika;
			document.getElementById("idproduct").value = id;
			select.selectedIndex = productArray[i].idKategorije;
			break;
		}
	}
	newProduct();
}
function newCategory() {
	var newCategoryRow = document.getElementById("kategorijanovi");
	if (newCategoryActive == false) {
		newCategoryRow.style.display = "revert";
		newCategoryActive = true;
	}
	else {
		newCategoryRow.style.display = "none";
		newCategoryActive = false;
		clearCategory();
	}
}
function clearCategory() {
	document.getElementById("novinazivkategorije").value = "";
	document.getElementById("idcategory").value = "";
}
function changeCategory(id) {
	for (i = 0; i < categoryArray.length; i++) {
		if (categoryArray[i].id == id) {
			document.getElementById("novinazivkategorije").value = categoryArray[i].naziv;
			document.getElementById("idcategory").value = id;
			break;
		}
	}
	newCategory();
}
function newLocation() {
	var newLocationRow = document.getElementById("lokacijanovi");
	if (newLocationActive == false) {
		newLocationRow.style.display = "revert";
		newLocationActive = true;
	}
	else {
		newLocationRow.style.display = "none";
		newLocationActive = false;
		clearLocation();
	}
}
function clearLocation() {
	var select = document.getElementById("standard-select-newlocationstype");
	document.getElementById("novadrzava").value = "";
	document.getElementById("novaadresa").value = "";
	document.getElementById("novigrad").value = "";
	document.getElementById("novitelefon").value = "";
	document.getElementById("idlocation").value = "";
	select.selectedIndex = 0;
}
function changeLocation(id) {
	var select = document.getElementById("standard-select-newlocationstype");
	for (i = 0; i < locationArray.length; i++) {
		if (locationArray[i].id == id) {
			document.getElementById("novigrad").value = locationArray[i].grad;
			document.getElementById("novadrzava").value = locationArray[i].drzava;
			document.getElementById("novitelefon").value = locationArray[i].telefon;
			document.getElementById("novaadresa").value = locationArray[i].adresa;
			document.getElementById("idlocation").value = id;
			if(locationArray[i].tip == "Maloprodaja")
			{		
				select.selectedIndex = 0;
			}
			else
			{
				select.selectedIndex = 1;
			}
			break;
		}
	}
	newLocation();
}
function locationShow(array) {
	var sadrzaj = "<tr><th>Drzava</th><th>Tip</th><th>Telefon</th>" + "<th>Grad</th>"
		+ "<th>Adresa</th></tr>";
	for (var i = 0; i < array.length; i++) {
		sadrzaj += "<tr>";
		sadrzaj += "<td>" + array[i].drzava + "</td>";
		sadrzaj += "<td>" + array[i].tip + "</td>";
		sadrzaj += "<td>" + array[i].telefon + "</td>";
		sadrzaj += "<td>" + array[i].grad + "</td>";
		sadrzaj += "<td>" + array[i].adresa + "</td>";
		sadrzaj += "<td style='width:50px'><button class='tableicon' onclick='changeLocation(" + array[i].id + ")'>" +
			"<i class='fa fa-pencil'></i></button></td>";
		sadrzaj += "<td style='width:50px'>";
		sadrzaj += "<form method='post' action='/MegaShop/DeleteLocationServlet'>";
		sadrzaj += "<input type='hidden' name='idlokacije' value='"+array[i].id+"'>";
		sadrzaj += "<button class='tableicon'><i class='fa fa-trash'></i></button></form></td></tr>";
		sadrzaj += "</tr>";

	}
	document.getElementById('locationtable').innerHTML = sadrzaj;
}
function categoryShow(array) {
	var sadrzaj = "<tr><th>Naziv</th></tr>";
	for (var i = 0; i < array.length; i++) {
		sadrzaj += "<tr>";
		sadrzaj += "<td>" + array[i].naziv + "</td>";
		sadrzaj += "<td style='width:50px'><button class='tableicon' onclick='changeCategory(" + array[i].id + ")'>" +
			"<i class='fa fa-pencil'></i></button></td>";
		sadrzaj += "<td style='width:50px'>";
		sadrzaj += "<form method='post' action='/MegaShop/DeleteCategoryServlet'>";
		sadrzaj += "<input type='hidden' name='idkategorije' value='"+array[i].id+"'>";
		sadrzaj += "<button class='tableicon'><i class='fa fa-trash'></i></button></form></td></tr>";
		sadrzaj += "</tr>";
		console.log("BIO");
	}
	document.getElementById('categorytable').innerHTML = sadrzaj;
}
function productShow(array) {
	var sadrzaj = "<tr><th>Naziv</th><th>Cena</th><th>Opis</th><th style='min-width:50px'>Boja</th>" +
		"<th>Kolicina</th><th>Kategorija</th><th>Slika</th><th></th><th></th></tr>";
	for (var i = 0; i < array.length; i++) {
		sadrzaj += "<tr id='product" + i + "'>";
		sadrzaj += "<td>" + array[i].naziv + "</td>";
		sadrzaj += "<td>" + array[i].cena + "</td>";
		sadrzaj += "<td>" + array[i].opis + "</td>";
		sadrzaj += "<td>" + array[i].boja + "</td>";
		sadrzaj += "<td>" + array[i].kolicina + "</td>";
		for (var j = 0; j < categoryArray.length; j++) {
			if (array[i].idKategorije == j + 1) {
				sadrzaj += "<td>" + categoryArray[j].naziv + "</td>";
			}
		}
		sadrzaj += "<td>" + array[i].slika + "</td>";
		sadrzaj += "<td style='width:50px'><button class='tableicon' onclick='changeProduct(" + array[i].id + ")'>" +
			"<i class='fa fa-pencil'></i></button></td>";
		sadrzaj += "<td style='width:50px'>";
		sadrzaj += "<form method='post' action='/MegaShop/DeleteProductServlet'>";
		sadrzaj += "<input type='hidden' name='idproizvod' value='"+array[i].id+"'>";
		sadrzaj += "<button class='tableicon'><i class='fa fa-trash'></i></button></form></td></tr>";
	}
	document.getElementById('tabelaproizvoda').innerHTML = sadrzaj;
}
function CallAjaxForCategories() {
	httpRequest = new XMLHttpRequest();
	if (!httpRequest) {
		console.log('Unable to create XMLHTTP instance');
		return false;
	}
	httpRequest.open('POST', 'CategoryServlet');
	httpRequest.responseType = 'json';
	httpRequest.send();
	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === XMLHttpRequest.DONE) {
			if (httpRequest.status === 200) {
				array = httpRequest.response;
				categoryArray = [...array];
				categoryShow(categoryArray);
				CallAjaxForProducts();
			}
		}
	}
}
function CallAjaxForProducts() {
	httpRequest = new XMLHttpRequest();
	if (!httpRequest) {
		console.log('Unable to create XMLHTTP instance');
		return false;
	}
	httpRequest.open('POST', 'ProductServlet');
	httpRequest.responseType = 'json';
	httpRequest.send();
	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === XMLHttpRequest.DONE) {
			if (httpRequest.status === 200) {
				array = httpRequest.response;
				productArray = [...array];
				productShow(productArray);
				CallAjaxForUsers();
			}
		}
	}
}
function CallAjaxForUsers() {
	httpRequest = new XMLHttpRequest();
	if (!httpRequest) {
		console.log('Unable to create XMLHTTP instance');
		return false;
	}
	httpRequest.open('POST', 'AdminServlet');
	httpRequest.responseType = 'json';
	httpRequest.send();
	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === XMLHttpRequest.DONE) {
			if (httpRequest.status === 200) {
				var array = httpRequest.response;
				var sadrzajkorisnik = "<tr><th>Ime</th><th>Email</th><th>Datum registracije</th><th></th><th></th></tr>";
				var sadrzajadmin = "<tr><th>Ime</th><th>Email</th><th>Datum registracije</th></tr>";
				for (var i = 0; i < array.length; i++) {
					if (array[i].status == 1) {
						sadrzajadmin += "<tr>";
						sadrzajadmin += "<td>" + array[i].korisnicko_ime + "</td>";
						sadrzajadmin += "<td>" + array[i].email + "</td>";
						sadrzajadmin += "<td>" + array[i].datum_registracije + "</td>";
						sadrzajadmin += "</tr>";
						adminUserArray.push(array[i]);
					}
					else {
						sadrzajkorisnik += "<tr>";
						sadrzajkorisnik += "<td>" + array[i].korisnicko_ime + "</td>";
						sadrzajkorisnik += "<td>" + array[i].email + "</td>";
						sadrzajkorisnik += "<td>" + array[i].datum_registracije + "</td>";
						sadrzajkorisnik += "<td title='Dodeli admin status' style='width:50px'>";
						sadrzajkorisnik += "<form method='post' action='/MegaShop/AddAdminUserServlet'>";
						sadrzajkorisnik += "<input type='hidden' name='idkorisnikadmin' value='"+array[i].id+"'>";
						sadrzajkorisnik += "<button class='tableicon'><i class='fa fa-user-secret'></i></button></form></td>";
						sadrzajkorisnik += "<td style='width:50px;'>";
						sadrzajkorisnik += "<form method='post' action='/MegaShop/DeleteUserServlet'>";
						sadrzajkorisnik += "<input type='hidden' name='idkorisnik' value='"+array[i].id+"'>";
						sadrzajkorisnik += "<button class='tableicon'><i class='fa fa-trash'></i></button></form></td>";
						sadrzajkorisnik += "</tr>";
						userArray.push(array[i]);
					}
				}
				document.getElementById('tabelakorisnika').innerHTML = sadrzajkorisnik;
				document.getElementById('tabelaadminkorisnika').innerHTML = sadrzajadmin;
				CallAjaxForLocations();
			}
		}
	}
}
function CallAjaxForLocations() {
	httpRequest = new XMLHttpRequest();
	if (!httpRequest) {
		console.log('Unable to create XMLHTTP instance');
		return false;
	}
	httpRequest.open('POST', 'ContactServlet');
	httpRequest.responseType = 'json';
	httpRequest.send();
	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === XMLHttpRequest.DONE) {
			if (httpRequest.status === 200) {
				var array = httpRequest.response;
				locationArray=[...array];
				locationShow(locationArray);
			}
		}
	}
}
function checkAllCategories() {
	let array2 = [...categoryArray];
	if (cityFilter != "") {
		array2 = array2.filter(data => data.naziv.toUpperCase().includes(nameCategoryFilter.toUpperCase()));

	}
	categoryShow(array2);
}
function checkAllLocations() {
	let array2 = [...locationArray];
	if (cityFilter != "") {
		array2 = array2.filter(data => data.grad.toUpperCase().includes(cityFilter.toUpperCase()));

	}
	if (addressFilter != "") {
		array2 = array2.filter(data => data.adresa.toUpperCase().includes(addressFilter.toUpperCase()));
	}
	if (selectedType != "Sve") {
		array2 = array2.filter(data => data.tip == selectedType);
	}
	locationShow(array2);
}
function checkAllProducts() {
	let array2 = [...productArray];
	if (nameFilter != "") {
		array2 = array2.filter(data => data.naziv.toUpperCase().includes(nameFilter.toUpperCase()));

	}
	if (colorFilter != "") {
		array2 = array2.filter(data => data.boja.toUpperCase().includes(colorFilter.toUpperCase()));
	}
	if (selectedCategoryId != 0) {
		array2 = array2.filter(data => data.idKategorije == selectedCategoryId);
	}

	if (priceFrom == "") {
		priceFrom = 0;
	}
	if (priceTo == "") {
		array2 = array2.filter(data => data.cena >= priceFrom);
	}
	else {
		array2 = array2.filter(data => data.cena >= priceFrom && data.cena <= priceTo);
	}
	productShow(array2);
}
function categoryFilter() {
	selectedCategoryId = document.getElementById("standard-select-product").value;
	checkAllProducts();
}
function nameFiltering() {
	nameFilter = document.getElementById("nazivproizvoda").value;
	checkAllProducts();
}
function colorFiltering() {
	colorFilter = document.getElementById("boja").value;
	checkAllProducts();
}
function checkPriceTo() {
	priceTo = document.getElementById("cenado").value;
	checkAllProducts();
}
function checkPriceFrom() {
	priceFrom = document.getElementById("cenaod").value;
	checkAllProducts();
}
function typesFilter() {
	selectedType = document.getElementById("standard-select-location").value;
	checkAllLocations();
}
function cityFiltering() {
	cityFilter = document.getElementById("grad").value;
	checkAllLocations();
}
function addressFiltering()
{
	addressFilter = document.getElementById("adresa").value;
	checkAllLocations();
}
function nameCategoryFiltering() {
	nameCategoryFilter = document.getElementById("naziv").value;
	checkAllCategories();
}