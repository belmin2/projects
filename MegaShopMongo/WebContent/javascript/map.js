function initMap() {
    const postion1 = { lat: 44.018084, lng: 20.913491 };
    const postion2 = { lat: 43.889001, lng: 20.350163 };
    const postion3 = { lat: 44.638368, lng: 21.300890 };
    const postion4 = { lat: 43.613211, lng: 21.894539 };
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 8,
        center: postion1
    });
    var myIcon = new google.maps.MarkerImage('icon/msgoogle.png');
        var marker = new google.maps.Marker({
        position: postion1,
        map: map,
        icon: myIcon,
        optimization: true,
    });
    var marker2 = new google.maps.Marker({
        position: postion2,
        map: map,
        icon: myIcon,
        optimization: true,
    });
    var marker3 = new google.maps.Marker({
        position: postion3,
        map: map,
        icon: myIcon,
        optimization: true,
    });
    var marker4 = new google.maps.Marker({
        position: postion4,
        map: map,
        icon: myIcon,
        optimization: true,
    });
}
