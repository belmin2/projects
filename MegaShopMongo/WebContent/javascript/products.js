var selectedCategoryId = 0;
var nameFilter="";
var priceFrom = "";
var priceTo = "";
var colorFilter="";
var array;
var cart=[];
function callAjax() {
	httpRequest = new XMLHttpRequest();
	if (!httpRequest) {
		console.log('Unable to create XMLHTTP instance');
		return false;
	}
	httpRequest.open('POST', 'ProductServlet');
	httpRequest.responseType = 'json';
	httpRequest.send();
	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === XMLHttpRequest.DONE) {
			if (httpRequest.status === 200) {
				array = httpRequest.response;
				var sadrzaj = "";
				for (var i = 0; i < array.length; i++) {
					sadrzaj += "<div class='box'>";
					sadrzaj += "<div class='image'><img src='items/" + array[i].slika + "' alt='slika'></div>";
					sadrzaj += "<h2>" + array[i].naziv + "</h2>";
					sadrzaj += "<p>" + array[i].opis + "<br>";
					sadrzaj += "Cena:" + array[i].cena + "din<br>";
					sadrzaj += "Boja:" + array[i].boja + "<br>";
					sadrzaj += "Kolicina:" + array[i].kolicina + "</p><br>";
					sadrzaj += "<button class='buttonkorpa' onclick='cartAdd("+array[i].id+")'>Dodaj u korpu</button><br><br></div>";
				}
				document.getElementById('gridproduct').innerHTML = sadrzaj;
			}
		}
	}
}
function cartAdd(id)
{
	var arrayForCart = [...array];
	let indikator=0,pozicija;
	for(var i=0;i<arrayForCart.length;i++)
	{
		if(arrayForCart[i].id==id)
		{
			for(var j=0;j<cart.length;j++)
			{
				if(cart[j].id == id) //postoji u korpi vec
				{
					indikator=1;
					cart[j].kolicina++;
					break;
				}
			}
			if(indikator == 0) //nije pronadjen
			{
				arrayForCart[i].kolicina=1;
				cart.push(arrayForCart[i]);
			}
		}
	}
	updateCart();
}
function updateCart()
{
	var cartsadrzaj="<tr><th>Naziv</th><th>Kolicina</th><th>Cena</th></tr>";
	let suma=0;
	for(var i=0;i<cart.length;i++)
	{
		cartsadrzaj+="<tr>";
		cartsadrzaj+="<td>"+cart[i].naziv+"</td>";
		cartsadrzaj+="<input type='hidden' name='idproizvoda[]' value='"+cart[i].id+"'>";
		cartsadrzaj+="<td style='text-align: right;vertical-align: bottom;'>"+
		"<input type='number' name='kolicina[]' style='width:50%;min-width: 40px;' id='kolicina"+i+"' value='"+cart[i].kolicina+"' oninput='checkValue("+i+")'></td>";
        cartsadrzaj+="<td style='text-align: right;vertical-align: bottom;'>"+cart[i].cena+"din</td>";
        cartsadrzaj+="<td><button class='korpaobrisi' onclick='cartRemove("+cart[i].id+")'>&times;</button></td>";
		cartsadrzaj+="</tr>";
		suma+=cart[i].cena*cart[i].kolicina;
	}
	document.getElementById("tabelakorpa").innerHTML = cartsadrzaj;
	document.getElementById("ukupno").innerHTML = "Ukupno: "+suma+"din";
}
function checkValue(id)
{
	var number=document.getElementById("kolicina"+id).value;
	console.log("SAD"+number);
	if(number < 1)
	{
		number=1;
	}
	cart[id].kolicina=number;
	updateCart();
}
function cartRemove(id)
{
	cart=cart.filter(data => data.id != id);
	updateCart();
}
var cartactive = false;
function cartOpen() {
	var cartdiv = document.getElementById("cartdiv");
	if (cartactive == false) {
		cartactive = true;
		cartdiv.style.display = "block";
	}
	else {
		cartactive = false;
		cartdiv.style.display = "none";
	}
}
var x, i, j, l, ll, selElmnt, a, b, c;
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
	selElmnt = x[i].getElementsByTagName("select")[0];
	ll = selElmnt.length;
	/* For each element, create a new DIV that will act as the selected item: */
	a = document.createElement("DIV");
	a.setAttribute("class", "select-selected");
	a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
	x[i].appendChild(a);
	/* For each element, create a new DIV that will contain the option list: */
	b = document.createElement("DIV");
	b.setAttribute("class", "select-items select-hide");
	for (j = 1; j < ll; j++) {
		/* For each option in the original select element,
		create a new DIV that will act as an option item: */
		c = document.createElement("DIV");
		c.innerHTML = selElmnt.options[j].innerHTML;
		c.addEventListener("click", function(e) {
			/* When an item is clicked, update the original select box,
			and the selected item: */
			var y, i, k, s, h, sl, yl;
			s = this.parentNode.parentNode.getElementsByTagName("select")[0];
			sl = s.length;
			h = this.parentNode.previousSibling;
			for (i = 0; i < sl; i++) {
				if (s.options[i].innerHTML == this.innerHTML) {
					s.selectedIndex = i;
					h.innerHTML = this.innerHTML;
					y = this.parentNode.getElementsByClassName("same-as-selected");
					yl = y.length;
					for (k = 0; k < yl; k++) {
						y[k].removeAttribute("class");
					}
					this.setAttribute("class", "same-as-selected");
					break;
				}
			}
			h.click();
		});
		b.appendChild(c);
	}
	x[i].appendChild(b);
	a.addEventListener("click", function(e) {
		/* When the select box is clicked, close any other select boxes,
		and open/close the current select box: */
		e.stopPropagation();
		closeAllSelect(this);
		this.nextSibling.classList.toggle("select-hide");
		this.classList.toggle("select-arrow-active");
	});
}

function closeAllSelect(elmnt) {
	/* A function that will close all select boxes in the document,
	except the current select box: */
	var x, y, i, xl, yl, arrNo = [];
	x = document.getElementsByClassName("select-items");
	y = document.getElementsByClassName("select-selected");
	xl = x.length;
	yl = y.length;
	for (i = 0; i < yl; i++) {
		if (elmnt == y[i]) {
			arrNo.push(i)
		} else {
			y[i].classList.remove("select-arrow-active");
		}
	}
	for (i = 0; i < xl; i++) {
		if (arrNo.indexOf(i)) {
			x[i].classList.add("select-hide");
		}
	}
}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);
function checkAll() {
	let array2 = [...array];
	var sadrzaj = "";
	if(nameFilter != "")
	{
		array2=array2.filter(data => data.naziv.toUpperCase().includes(nameFilter.toUpperCase()));
		
	}
	if(colorFilter != "")
	{
		array2=array2.filter(data => data.boja.toUpperCase().includes(colorFilter.toUpperCase()));
	}
	if(selectedCategoryId != 0)
	{
		array2=array2.filter(data => data.idKategorije == selectedCategoryId);	
	}
	
	if(priceFrom == "")
	{
		priceFrom=0;
	}
	if (priceTo == "") {
		array2=array2.filter(data => data.cena >= priceFrom);	
	}
	else {
		array2=array2.filter(data => data.cena >= priceFrom && data.cena <= priceTo);
	}
	for ( i = 0; i < array2.length; i++) {
		sadrzaj += "<div class='box'>";
		sadrzaj += "<div class='image'><img src='items/" + array2[i].slika + "' alt='slika'></div>";
		sadrzaj += "<h2>" + array2[i].naziv + "</h2>";
		sadrzaj += "<p>" + array2[i].opis + "<br>";
		sadrzaj += "Cena:" + array2[i].cena + "din<br>";
		sadrzaj += "Boja:" + array2[i].boja + "<br>";
		sadrzaj += "Kolicina:" + array2[i].kolicina + "</p><br>";
		sadrzaj += "<button class='buttonkorpa' onclick='cartAdd("+array2[i].id+")'>Dodaj u korpu</button><br><br></div>";
	}
	document.getElementById('gridproduct').innerHTML = sadrzaj;
}
function categoryFilter() {
	selectedCategoryId = document.getElementById("standard-select").value;
	checkAll();
}
function nameFiltering() {
	nameFilter = document.getElementById("naziv").value;
	checkAll();
}
function colorFiltering() {
	colorFilter = document.getElementById("boja").value;
	checkAll();
}
function checkPriceTo() {
	priceTo = document.getElementById("cenado").value;
	checkAll();
}
function checkPriceFrom() {
	priceFrom = document.getElementById("cenaod").value;
	checkAll();
}