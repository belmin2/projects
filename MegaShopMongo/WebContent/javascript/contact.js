var maloprodaja = false;
var veleprodaja = false;
function callAjax() {
	httpRequest = new XMLHttpRequest();
	if (!httpRequest) {
		console.log('Unable to create XMLHTTP instance');
		return false;
	}
	httpRequest.open('POST', 'ContactServlet');
	httpRequest.responseType = 'json';
	httpRequest.send();
	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === XMLHttpRequest.DONE) {
			if (httpRequest.status === 200) {
				var array = httpRequest.response;
				var sadrzaj = "<tr><th>Drzava</th><th>Tip</th><th>Telefon</th>" + "<th>Grad</th>"
					+ "<th style='text-align:right;color: lightblue;'>Adresa</th></tr>";
				for (var i = 0; i < array.length; i++) {
					if (veleprodaja == true) {
						if (array[i].tip == "Veleprodaja") {
							sadrzaj += "<tr>";
							sadrzaj += "<td>" + array[i].drzava + "</td>";
							sadrzaj += "<td>" + array[i].tip + "</td>";
							sadrzaj += "<td>" + array[i].telefon + "</td>";
							sadrzaj += "<td>" + array[i].grad + "</td>";
							sadrzaj += "<td style='text-align:right;color: lightblue;'>" + array[i].adresa + "</td>";
							sadrzaj += "</tr>";
						}
					}
					if (maloprodaja == true) {
						if (array[i].tip == "Maloprodaja") {
							sadrzaj += "<tr>";
							sadrzaj += "<td>" + array[i].drzava + "</td>";
							sadrzaj += "<td>" + array[i].tip + "</td>";
							sadrzaj += "<td>" + array[i].telefon + "</td>";
							sadrzaj += "<td>" + array[i].grad + "</td>";
							sadrzaj += "<td style='text-align:right;color: lightblue;'>" + array[i].adresa + "</td>";
							sadrzaj += "</tr>";
						}
					}
				}
				document.getElementById('tabelalokacija').innerHTML = sadrzaj;
			}
			else {
				console.log('Something went wrong..!!');
			}
		}
	}
}
function filterLocations(tip) {
	if (tip == "maloprodaja") {
		if (maloprodaja == true) {
			maloprodaja = false;
		}
		else {
			maloprodaja = true;
		}
	}
	if (tip == "veleprodaja") {
		if (veleprodaja == true) {
			veleprodaja = false;
		}
		else {
			veleprodaja = true;
		}
	}
	callAjax();
}
